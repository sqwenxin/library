package test;

import java.lang.reflect.Constructor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sq.book.management.biz.IPurchaseBookBiz;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-mvc.xml","classpath*:spring-mybatis.xml","classpath*:spring-activiti.xml","classpath*:spring-config.xml","classpath*:applicationContext.xml"})
public class FileTest {
	
	@Autowired
	private IPurchaseBookBiz baseBiz;
	
	@Test
	public void AnnTest() throws ClassNotFoundException{
//		Class clazz = Class.forName("com.sq.book.management.entity.PurchaseBookEntity");
//		WorkFlow workFolw = (WorkFlow) clazz.getAnnotation(WorkFlow.class);
//		
//		System.out.println("表名：" + workFolw.table());
//		System.out.println("主键：" + workFolw.tableId());
//		System.out.println("流程定义列" + workFolw.procInstColumn());
//		System.out.println("流程定义标识" + workFolw.procDefKey());
//		System.out.println("流程状态字段" + workFolw.statusColumn());
//		baseBiz.excuteSql("update purchase_book set pb_price=100");
		Class clazz = Class.forName("java.lang.String");
		Constructor[] con = clazz.getConstructors();
		for(Constructor _con : con){
			System.out.println(_con);
		}
	}
	
}
