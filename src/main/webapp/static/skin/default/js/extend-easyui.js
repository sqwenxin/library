/**
 * 扩展修改easyui补充js
 */

/* 属性默认值修改 */
$.fn.tree.defaults.lines = true;// 树节点默认有线条
$.fn.datagrid.defaults.pageSize = 20;
$.fn.datagrid.defaults.pageList = [ 10, 20, 50, 100 ];
$.fn.filebox.defaults.prompt = "请选择文件...";
$.fn.filebox.defaults.buttonText = "选择文件";
$.fn.textbox.defaults.height = 24;
$.fn.combobox.defaults.multiline = true;//复选框多行显示
$.fn.combobox.defaults.height = 24;

//textbox被赋值时,根据长度自行修改高度
$.fn.textbox.defaults.onChange = function(newValue, oldValue){
	//当前文本框宽度
	var width = $(this).textbox('options').width;
	//当前高度
	var oldHeight = $(this).textbox('options').height;
	//判断是否为数字
	var isNUmber = width + "";
	//如果宽度是%表示的，那么以固定值300计算
	if(isNUmber.indexOf('%') != -1){		
		width = 300;
	}	
	//计算出字符串宽度
    var sensor = $('<pre>'+ newValue +'</pre>').css({display: 'none','font-size':'13px'}); 
    $('body').append(sensor); 
    var length = sensor.width();
    sensor.remove(); 	
    //计算需要的高度
    var rows = parseInt(( length + 4 ) / width) ;
    if(rows == 0 || length + 4 > width){
    	rows ++;
    }
    var height = 10 + 14 * rows;
    //若字符高度小于当前文本高度，则不处理
    if(oldHeight >= height){
    	return;
    }
	$(this).textbox({    
	    height:height,
	})
}

/**
 * easy UI树形扩展
 */
$.fn.tree.defaults.loadFilter = function(data, parent) {
	var opt = $(this).data().tree.options;
	var idFiled, textFiled, parentField;
	if (opt.parentField) {
		idFiled = opt.idFiled || 'id';
		textFiled = opt.textFiled || 'text';
		parentField = opt.parentField;
		var i, l, treeData = [], tmpMap = [];
		for (i = 0, l = data.length; i < l; i++) {
			tmpMap[data[i][idFiled]] = data[i];
		}
		for (i = 0, l = data.length; i < l; i++) {
			if (tmpMap[data[i][parentField]]
					&& data[i][idFiled] != data[i][parentField]) {
				if (!tmpMap[data[i][parentField]]['children'])
					tmpMap[data[i][parentField]]['children'] = [];
				data[i]['text'] = data[i][textFiled];
				tmpMap[data[i][parentField]]['children'].push(data[i]);
			} else {
				data[i]['text'] = data[i][textFiled];
				treeData.push(data[i]);
			}
		}
		return treeData;
	}
	return data;
};

//下拉树扩展
$.fn.combotree.defaults.loadFilter = function(data, parent) {
	var opt = $(this).data().tree.options;
	var idFiled, textFiled, parentField;
	if (opt.parentField) {
		idFiled = opt.idFiled || 'id';
		textFiled = opt.textFiled || 'text';
		parentField = opt.parentField;
		var i, l, treeData = [], tmpMap = [];
		for (i = 0, l = data.length; i < l; i++) {
			tmpMap[data[i][idFiled]] = data[i];
		}
		for (i = 0, l = data.length; i < l; i++) {
			if (tmpMap[data[i][parentField]]
					&& data[i][idFiled] != data[i][parentField]) {
				if (!tmpMap[data[i][parentField]]['children'])
					tmpMap[data[i][parentField]]['children'] = [];
				data[i]['text'] = data[i][textFiled];
				tmpMap[data[i][parentField]]['children'].push(data[i]);
			} else {
				data[i]['text'] = data[i][textFiled];
				treeData.push(data[i]);
			}
		}
		return treeData;
	}
	return data;
};
/*
 * 扩展时间解析
 */
$.fn.datebox.defaults.parser = function(s) {
	if (s instanceof Date)
		return s;
	if ((typeof s == 'number') && s.constructor == Number)
		return new Date(s);
	if (!s)
		return new Date();
	var ss = s.split('-');
	var y = parseInt(ss[0], 10);
	var m = parseInt(ss[1], 10);
	var d = parseInt(ss[2], 10);
	if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
		return new Date(y, m - 1, d);
	} else {
		return new Date();
	}
}

/** 
 * 处理linkbutton禁用后依然点击有效
 * linkbutton方法扩展 
 * @param {Object} jq 
 */  
$.extend($.fn.linkbutton.methods, {  
    /** 
     * 激活选项（覆盖重写） 
     * @param {Object} jq 
     */  
    enable: function(jq){  
        return jq.each(function(){  
            var state = $.data(this, 'linkbutton');  
            if ($(this).hasClass('l-btn-disabled')) {  
                var itemData = state._eventsStore;  
                //恢复超链接  
                if (itemData.href) {  
                    $(this).attr("href", itemData.href);  
                }  
                //回复点击事件  
                if (itemData.onclicks) {  
                    for (var j = 0; j < itemData.onclicks.length; j++) {  
                        $(this).bind('click', itemData.onclicks[j]);  
                    }  
                }  
                //设置target为null，清空存储的事件处理程序  
                itemData.target = null;  
                itemData.onclicks = [];  
                $(this).removeClass('l-btn-disabled');  
            }  
        });  
    },  
    /** 
     * 禁用选项（覆盖重写） 
     * @param {Object} jq 
     */  
    disable: function(jq){  
        return jq.each(function(){  
            var state = $.data(this, 'linkbutton');  
            if (!state._eventsStore)  
                state._eventsStore = {};  
            if (!$(this).hasClass('l-btn-disabled')) {  
                var eventsStore = {};  
                eventsStore.target = this;  
                eventsStore.onclicks = [];  
                //处理超链接  
                var strHref = $(this).attr("href");  
                if (strHref) {  
                    eventsStore.href = strHref;  
                    $(this).attr("href", "javascript:void(0)");  
                }  
                //处理直接耦合绑定到onclick属性上的事件  
                var onclickStr = $(this).attr("onclick");  
                if (onclickStr && onclickStr != "") {  
                    eventsStore.onclicks[eventsStore.onclicks.length] = new Function(onclickStr);  
                    $(this).attr("onclick", "");  
                }  
                //处理使用jquery绑定的事件  
                var eventDatas = $(this).data("events") || $._data(this, 'events');  
                if (eventDatas["click"]) {  
                    var eventData = eventDatas["click"];  
                    for (var i = 0; i < eventData.length; i++) {  
                        if (eventData[i].namespace != "menu") {  
                            eventsStore.onclicks[eventsStore.onclicks.length] = eventData[i]["handler"];  
                            $(this).unbind('click', eventData[i]["handler"]);  
                            i--;  
                        }  
                    }  
                }  
                state._eventsStore = eventsStore;  
                $(this).addClass('l-btn-disabled');  
            }  
        });  
    }  
});