/**
 * 基于jquery进行方法扩展
 */
(function($) {

	$.fn.serializeObject = function(options) {
		var defaults = {
			trimEmpty : true,// 去除数据中的空值
		}

		var options = $.extend(defaults, options);

		var resultObject = {};
		var rowData = this.serializeArray();

		$.each(rowData, function() {
			// 验证值是否为空值，为空值的话，跳出当前循环
			if (options.trimEmpty) {
				if (this.value == '') {
					// 跳过循环
					return true;
				}
			}
			if (resultObject[this.name]) {
				if (!resultObject[this.name].push) {
					resultObject[this.name] = [ resultObject[this.name] ];
				}
				resultObject[this.name].push(this.value || '');
			} else {
				resultObject[this.name] = this.value || '';
			}
		});
		return resultObject;
	};
})(jQuery);

(function() {

	/**
	 * 获取数组中某个字段的数据集合 典型用法是获取ids字符串
	 * 
	 * @param field
	 * @returns {String} 字段数据集合字符串
	 */
	Array.prototype.getFieldData = function(field) {
		if (field == undefined) {
			return;
		}

		var list = this;

		var ids = "";
		$.each(list, function(index, element) {
			ids += "" + eval("element." + field) + ","
		});
		if (ids != "") {
			ids = ids.substring(0, ids.length - 1);
		}
		return ids;
	}

	/**
	 * js默认时间解析 以及时间相加
	 */
	Date.prototype.format = function(format) {
		var o = {
			"M+" : this.getMonth() + 1, // month
			"d+" : this.getDate(), // day
			"h+" : this.getHours(), // hour
			"m+" : this.getMinutes(), // minute
			"s+" : this.getSeconds(), // second
			"q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
			"S" : this.getMilliseconds()
		// millisecond
		}
		var week = [ "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" ];
		if (/(y+)/.test(format)) {
			format = format.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		}
		if (/(w+)/.test(format)) {
			fmt = fmt.replace(RegExp.$1, week[this.getDay()]);
		}
		for ( var k in o) {
			if (new RegExp("(" + k + ")").test(format)) {
				format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
						: ("00" + o[k]).substr(("" + o[k]).length));
			}
		}
		return format;
	}

	/**
	 * js中更改日期 y年， m月， d日， h小时， n分钟，s秒
	 */
	Date.prototype.add = function(part, value) {
		value *= 1;
		if (isNaN(value)) {
			value = 0;
		}
		switch (part) {
		case "y":
			this.setFullYear(this.getFullYear() + value);
			break;
		case "m":
			this.setMonth(this.getMonth() + value);
			break;
		case "d":
			this.setDate(this.getDate() + value);
			break;
		case "h":
			this.setHours(this.getHours() + value);
			break;
		case "n":
			this.setMinutes(this.getMinutes() + value);
			break;
		case "s":
			this.setSeconds(this.getSeconds() + value);
			break;
		default:

		}
	}
})();

(function ($) {

    $.fn.filePreview = function (options) {

        var settings = $.extend({
        	
            "fileElementId": 'top',
        }, options);

        return this.each(function () {

            // Tooltip插件代码

        });

    };
})(jQuery);


/**
 * 附件预览前端组件
 */
var fileComponent = {
	base : getProjectName(),
	ATTACHMENTS_PATH: "C:\\Attachments",
	
	upload : function(options) {
		var settings = $.extend({
			"fileElementId" : "",
			"uploadURL" : fileComponent.base + "/file/upload.action",
			"queryParams": {
				"uploadPath" : "/tmpl",
			},
			"preview" : false,
			"success" : null,
		}, options);

		if (settings.fileElementId == "") {
			notify("提示信息", "未设置文件上传框id");
			return false;
		}

		var fileElement = $("#" + settings.fileElementId);
		if (fileElement.attr("class").indexOf("easyui-filebox") != -1) {
			settings.fileElementId = fileElement.next().find("input:file")[0].id;
		}
		
		if ($("#"+settings.fileElementId).val() == "") {
			notify("提示信息", "未上传文件");
			return false;
		}

		$.ajaxFileUpload({
			url : settings.uploadURL,
			fileElementId : settings.fileElementId,
			dataType : 'json',
			data : settings.queryParams,
			success : function(data, status) {
				if (settings.success != null) {
					settings.success(data);
				}
				if (settings.preview) {
					fileComponent.preview({
						"fileName" : data.name,
						"filePath" : data.path,
					});
				}
				return data;
			},
			error : function(data, status, e) {
				notify("提示信息", "文件上传失败");
			}
		})
	},

	preview : function(options) {
		var settings = $.extend({
			"url" : fileComponent.base + "/file/preview.action",
			"fileName" : "",
			"filePath" : "",
		}, options);
		
		if (settings.filePath == "" || settings.fileName == "") {
			notify("提示信息", "文件路径未设置，预览失败");
			return false;
		}
		
		if (settings.filePath.indexOf(fileComponent.ATTACHMENTS_PATH) != -1) {
			var filePath = settings.filePath.split(fileComponent.ATTACHMENTS_PATH);
			settings.filePath = filePath[1];
		}
		
		var url = settings.url + "?filePath=" + settings.filePath;
		
		//文件名中含有空格
		if (settings.fileName.indexOf(" ") != -1) {
			window.open(url);
		} else {
			addTab('main', settings.fileName, url);
		}
		
	},
	download : function(options) {
		var settings = $.extend({
			"url" : fileComponent.base + "/file/download.action",
			"fileName" : "",
			"filePath" : "",
		}, options);
		
		if (settings.filePath == "" || settings.fileName == "") {
			notify("提示信息", "文件路径未设置，下载失败");
			return false;
		}
		
		if (settings.filePath.indexOf(fileComponent.ATTACHMENTS_PATH) != -1) {
			var filePath = settings.filePath.split(fileComponent.ATTACHMENTS_PATH);
			settings.filePath = filePath[1];
		}
		
		var url = settings.url + "?filePath=" + settings.filePath + "&fileName=" + settings.fileName;
		
		window.open(url);
	}
};