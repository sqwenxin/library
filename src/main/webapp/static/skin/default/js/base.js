/**
 * 基础函数库
 */

/**
 * 工作流相关方法
 */
var workFlowClass = {
    /**
	 *
     * @param cls 工作流关联类
     * @param parms 工作流需要的主要参数 cls 所属类
										 bussinessKey 关联Id
										 state 状态字段
     * @param maps 工作流需要的其他参数，以json格式传送过来
     */
	start : function(cls,parms,maps){
        parms.map = JSON.stringify(map);
	}
}

/**
 * 随机数方法
 */
function randomNumBoth(Min, Max){
	var Range = Max - Min;
	var Rand = Math.random();
	var num = Min + Math.round(Rand * Range); //四舍五入
	return num;
}

/**
 * post方式提交数据
 */
function post(postUrl, postData) {
	var _data;
	$.ajax({
		type : "post",
		url : postUrl,
		data : postData,
		dataType : "json",
		async : false,
		success : function(data) {
			_data = data;
		}
	});
	return (_data);
}

function array2Object(nameArray, valueArray) {
	if (nameArray == undefined) {
		return;
	}
	var dataObj = {};
	$.each(valueArray, function(index, element){
		dataObj[nameArray[index]] = element;
	});
	return dataObj;
}

/**
 * 获取当前项目名
 * @returns
 */
function getProjectName() {
	var pathName = window.document.location.pathname;
	var projectName = pathName
			.substring(0, pathName.substr(1).indexOf('/') + 1);

	return projectName;
}

function appendParam(url, param) {
	if (url.indexOf('?') != -1) {
		url += "&" + param;
	} else {
		url += "?" + param;
	}
	return url;
}

/************************基于easyui扩展函数****************************/

/**
 * 通知消息框
 */
function notify(title, msg) {
	$.messager.show({
		title: title,
		msg: msg,
		timeout:2000,
		style:{
			left:'',
			right:'0',
			top:document.body.scrollTop+document.documentElement.scrollTop,
		}
	});
}

/**
 * easyui动态添加tab页，要求：
 * 页面需引用easyui资源文件
 * @param id tabs组件id
 * @param title tab页面的标题
 * @param url 加载tab页的地址
 * @returns
 */
function addTab(id, title, url){
	var tabs = "";
	
	if(window.self != window.top) {
		window.top.addTab("main", title, url);
		return;
	} else {
		tabs = $('#'+id);
	}
	
	var content = '<iframe frameborder="0" src='+url+' style="width:100%;min-height:' + ($(window).height()-90) + 'px;"></iframe>';
	
	//验证名为title的选项卡是否已经被打开
	if (tabs.tabs('exists', title)) {
		tabs.tabs('select', title);
		/*var tab = tabs.tabs('getTab', title);
		tabs.tabs('update', {
			tab: tab,
			options: {
			    content : content,
			}
		});*/
	} else {
		tabs.tabs('add', {
		    title : title,
		    content : content,
		    closable : true,
		    tools: [{
		        iconCls:'icon-mini-refresh',
		        handler:function(){
		        	var tab = tabs.tabs('getTab', title);
					var url = tab.children("iframe").get(0).src;
					tab.children("iframe").attr("src", url);
		        }
		    }],
			bodyCls: 'overflow-hidden',
		});
	}
}

/**
 * 关闭当前选项卡
 * @param id tabs组件id
 */
function closeTab(id) {
	id = id || "main";
	if(window.self != window.top) {
		window.top.closeTab(id);
		return;
	} else {
		var tabs = $('#'+id);
		var tab = tabs.tabs('getSelected');
		var index = tabs.tabs('getTabIndex', tab);
		tabs.tabs('close', index);
	}
}

/**
 * 加载菜单方法
 */
function addMune(id, title, href) {
	$("#"+id).panel({
		href: href,
		title: title,
		onLoad: function(){
			if ($("#loginWeb").length >= 1) {
				var origin = window.location.origin;
				var projectName = getProjectName();
				var href = origin + projectName + "/login.action";
				window.location = href;
			}
		}
	}).panel('resize');
}

/**
 * 树形菜单方法
 * 控制点击树节点跳转链接
 */
function initTreeMune() {
	//点击左侧菜单栏，页面跳转
	$('#menu .easyui-tree').tree({
		onClick : function(node) {
			//菜单没有子节点，加载页面
        	if (node.children == undefined) {
        		var url = node.attributes.url;
        		var title = node.text;
        		var modelCode = node.attributes.modelCode;
        		if (node.attributes.title != "") {
        			title = node.attributes.title;
        		}
        		url = appendParam(url, "modelCode=" + modelCode);
        		addTab("main", title, url);
        	} else {
        		if (node.state == "closed") {
	            	$(this).tree('expand', node.target);
	            }
	            else {
	            	$(this).tree('collapse', node.target);
	            }
        	}
		},
	});
}

/**
 * 将数据转成线性数据（适于树结构）
 * @param resultData
 * @param idField
 * @param textField
 * @param parentIdField
 * @param state
 * @returns
 */
function changeTreeJson(resultData, idField, textField, parentIdField, state) {
	// 新的数组
	var topDataList = new Array();
	for (var i = 0; i < resultData.length; i++) {
		// 取出所有的顶级节点
		if (resultData[i][parentIdField] == 0 || resultData[i][parentIdField] == undefined) {
			if (resultData[i]["categoryDel"] != undefined) {
				if (resultData[i]["categoryDel"] == 0) {
					topDataList.push(resultData[i]);
				}

			} else {
				topDataList.push(resultData[i]);
			}

		}
	}
	return funcJson(topDataList, resultData, idField, textField, parentIdField,
			state);
}

function funcJson(data, resultData, idField, textField, parentIdField, state) {
	if (!data) {
		return;
	}
	for (var j = 0; j < data.length; j++) {
		data[j].id = data[j][idField]; // 添加id属性
		data[j].text = data[j][textField]; // 添加text属性
		var newDataList = new Array();
		// alert(resultData.length)
		for (var i = 0; i < resultData.length; i++) {
			if (resultData[i][parentIdField] == data[j][idField]) {
				if (resultData[i]["categoryDel"] != undefined) {
					if (resultData[i]["categoryDel"] == 0) {
						newDataList.push(resultData[i]);
					}
				} else {
					newDataList.push(resultData[i]);
				}
			}
		}
		if (newDataList.length > 0) {
			if (state == true) {
				data[j].state = "open";
			} else {
				data[j].state = "closed";
			}
			data[j].children = funcJson(newDataList, resultData, idField,
					textField, parentIdField, state); // 添加children属性
		}
	}
	return data;
}

function trimDataEmpty(formData) {
	var queryData = [];
	for (var i = 0; i < formData.length; i++) {
		var data = formData[i];
		if (data.value != "" ) {
			queryData.push(data);
		}
	}
	return queryData;
}

/**
 * 确认删除提示框
 * @param title
 * @param content
 * @param yes
 */
function confirm(title,content,yes) {
	if (isBlank(title)) {
		title="提示";
	}
	if(isBlank(content)) {
		content="确认此操作？";
	}
	
	$.messager.confirm(title, content, function(r){
		if (r){
			yes.call(this);
		}
	});		
}

/**
 * 判断是否为空
 */
function isBlank(str){
	if(str!= undefined && str !=null && str!="" ){
		if(jQuery.isArray(str)){
			if(str.length>0){
				return false;
			}else{
				return true; 
			}
		}
		return false;
	}
	return true; 
}

/**
 *新增json方法
 */
var jsonArrayFunction = window.jsonArrayFunction || {};
jsonArrayFunction.isArray = function(o) { //判断对象类型是否为数组
	 return typeof o == 'object' && Object.prototype.toString.call(o).slice(8,-1).toLowerCase()=='array';
},
jsonArrayFunction.soft = function(array,type,asc) { //排序
	if(!jsonArrayFunction.isArray(array)){
		throw new Error('第一个参数必须是数组类型');
	}				
	var asc = asc||false;
	array.sort(function(a,b) {
		if(!asc) {
			return parseFloat(b[type]) - parseFloat(a[type]);
		}else{
			return parseFloat(a[type]) - parseFloat(b[type]);
		}
	})
	return array;
},
jsonArrayFunction.search = function(array,type,value) {//搜索
	if(!jsonArrayFunction.isArray(array)){
		throw new Error('第一个参数必须是数组类型');
	} 
	var arr = [];
	arr = array.filter(function(a) { 
		return a[type].toString().indexOf(value)!=-1;
	});
	return arr;
}

/**
 * 数据分配组件
 */
var assigneeComponent = {
	"deleteAction": getProjectName() + "/roleData/deleteByRdValues.action",
		
	//删除数据权限数据
	del: function(options){
		var defaults = $.extend({
			"rdValue" : "",
			"rdModel" : "",
			"success" : null,
		}, options);
		
		if (validator.isEmpty(defaults.rdValue)) {
			notify("提示", "rdValue值为空");
			return;
		}
		
		if (validator.isEmpty(defaults.rdModel)) {
			notify("提示", "rdModel值为空");
			return;
		}
		
		var queryParams = {
			"rdValues" : defaults.rdValue,
			"rdModel" : defaults.rdModel,
		};
		
		$.post(assigneeComponent.deleteAction, queryParams, function(json){
			if (defaults.success != null) {
				defaults.success(json);
			}
		});
	},
	
	//退回申请方法
	withdraw: function(options){
		
		$.messager.confirm({
			ok: "是", 
			cancel: "否",
			title: "提示",
			msg: "确定撤回？",
			fn: function(r) {
				if (r) {
					var defaults = $.extend({
						"action": "",
						"params": "",
						"rdValue" : "",
						"rdModel" : "",
						"success" : null,
					}, options);
					
					if (validator.isEmpty(defaults.action)) {
						notify("提示", "action值为空");
						return;
					}
					
					if (validator.isEmpty(defaults.rdValue)) {
						notify("提示", "rdValue值为空");
						return;
					}
					
					if (validator.isEmpty(defaults.rdModel)) {
						notify("提示", "rdModel值为空");
						return;
					}
					
					var queryParams = $.extend({
						"rdValue" : defaults.rdValue,
						"rdModel" : defaults.rdModel,
						"rdProcess" : 2,//表示撤回
					}, options.params);
					
					$.post(defaults.action, queryParams, function(json){
						if (defaults.success != null) {
							defaults.success(json);
						}
					});
				}
			}
		});
		
	}
}