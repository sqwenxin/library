<@sq.html5 easyui="true" bootstrap="false">
    <@sq.head title="确认提交">
    </@sq.head>
    <@sq.body>
        <@sq.workFlowInfo id="workFlowInfo" processInstanceId="${processInstance.processInstanceId}">
            <form id="info">
                <table class="altrowstable text-center" style="margin-top: 50px">
                    <tr>
                        <td colspan="4">
                            采购信息
                        </td>
                    </tr>
                    <tr>
                        <td>
                            采购名称
                        </td>
                        <td>
                            <input class="easyui-textbox"  name="pbName" data-options="editable:false">
                        </td>
                        <td>
                            采购价格
                        </td>
                        <td>
                            <input class="easyui-textbox"  name="pbPrice" data-options="editable:false">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            采购内容
                        </td>
                        <td colspan="3">
                            <input class="easyui-textbox"  name="pbContent" data-options="editable:false,multiline:true," style="height:50px">
                        </td>
                    </tr>
                </table>
            </form>
        </@sq.workFlowInfo>

        <@sq.workFlowSolve id="workFlowSolve" taskId="${taskId}" processInstanceId="${processInstance.processInstanceId}" backText="退回" passText="通过">
           <div style="width;100%;margin-top:20px" align="center">
               <input type="hidden" id="financeResult" name="financeResult">
               <input type="hidden" id="financeAdvice" name="financeAdvice">
               <input class="easyui-textbox" label="意见:" name="comment" data-options="multiline:true,required:true" style="height:60px;width:80%">
           </div>
        </@sq.workFlowSolve>
    </@sq.body>
</@sq.html5>
<script>

    //取消事件
    workFlowSolve.back = function(){
        $("#financeResult").val("no");
        $("#financeAdvice").val("disagree");
    };

    //通过事件
    workFlowSolve.pass = function(){
        $("#financeResult").val("yes");
        $("#financeAdvice").val("agree");
    }

    //异步加载采购信息数据
    $.post("${base}/manager/purchaseBook/get.do","pbId=${processInstance.businessKey}",function(returnData){
        if(returnData.result){
            $("#info").form("load",returnData.data);
        }
    })

</script>