<@sq.html5 easyui="true" bootstrap="false">
	<@sq.head title="用户列表">
		<style>
			#tool input{
				width:100px
			}
			.easyInput{
				margin-top:15px
			}
		</style>
	</@sq.head>
	<@sq.body>
	<div data-options="region:'center',title:''"  style="height:99%">
		 <div id="toolBar" style="background:#eee;">
		 	 <a id="add" class="easyui-linkbutton" iconcls="icon-add" plain="true">创建采购信息</a> 
		 	 <a id="edit" class="easyui-linkbutton" iconcls="icon-edit" plain="true">编辑采购信息</a> 
		 	 <a id="delete" class="easyui-linkbutton" iconcls="icon-remove" plain="true">删除采购信息</a> 
		 	 <a id="start" class="easyui-linkbutton" iconcls="icon-save" plain="true">启动审批</a> 
		 </div>
		 <table id="purchaseList"  style="height:95%"></table>
	</div>
	<!-- modelwin -->
	<div id="win" class="easyui-window"  title="采购信息" style="width:450px;height:300px;display:none"
		        data-options="iconCls:'icon-save',modal:true,closed:true">
		    <div class="easyui-layout" data-options="fit:true">
		    	<div data-options="region:'center'">
		           <form id="purchaseInfo" action="create" style="width:350px;margin:0 auto" >
		           		<input type="hidden" name="pbId" id="pbId">
		           		<div class="easyInput" >
		           			<input id="pbName" data-options="required:true,labelWidth:'20%'" class="easyui-textbox" style="width:100%;"   label="采购名称:" name="pbName"  value="">
		           		</div>
		           		<div class="easyInput">
		           			<input id="pbContent" data-options="required:true,labelWidth:'20%',multiline:true" name="pbContent" class="easyui-textbox" type="text" label="采购内容:" style="width:100%;height:50px">
		           		</div>  
		           		<div class="easyInput">
		           			<input id="pbPrice" data-options="required:true,labelWidth:'20%'" name="pbPrice" class="easyui-textbox" type="text" label="采购金额:" style="width:100%;">
		           		</div>           			           		
		           </form>
		        </div>
		        <div id="dlg-buttons" data-options="region:'south',split:true" style="height:35px">
		        	<div style="float:right">
		        		<a class="easyui-linkbutton" id="cancel" iconcls="icon-cancel">取消</a> 
		        		<a class="easyui-linkbutton" id="save" onclick="" iconcls="icon-save" style="margin-right:5px">保存</a> 		        		
		        	</div>        	
		        </div>       
		    </div>
		</div>	
	</@sq.body>
</@sq.html5>
<script>
	$(function() {
		var url = "${base}/manager/purchaseBook/save.do"
		
		//新建采购信息
		$("#add").click(function(){
			$('#purchaseInfo').form('clear');
			$('#win').window('open');
			url = "${base}/manager/purchaseBook/save.do"
		})
		
		//保存采购信息
		$("#save").click(function(){
			$.post(url,$("#purchaseInfo").serialize(),function(returnData){
				if(returnData.result){
					notify("提示","新增成功");
					$('#purchaseList').datagrid('reload'); 
					$('#win').window('close');
				}
			});			
		})
		 
		//编辑采购信息
		$("#edit").click(function(){
			$('#purchaseInfo').form('clear');
			var selected = $('#purchaseList').datagrid('getSelected');
			if(selected == null){
				notify("提示","请先选择一个模型");
				return;
			}
			$('#purchaseInfo').form("load",selected);
			url = "${base}/manager/purchaseBook/update.do"
			$('#win').window('open');
		})	
		  
		//删除模型
		$("#delete").click(function(){
			var selected = $('#purchaseList').datagrid('getSelected');
			if(selected == null){
				notify("提示","请先选择一个模型");
				return;
			}
			$.post("${base}/manager/purchaseBook/delete.do","pbId=" + selected.pbId,function(returnData){
				if(returnData.result){
					notify("提示","删除成功");
					$('#purchaseList').datagrid('reload');
				}else{
					notify("提示","发生错误删除失败");
				}				
			})
		})
		
		//启动流程
		$("#start").click(function(){
			var selected = $('#purchaseList').datagrid('getSelected');
			if(selected == null){
				notify("提示","请先选择一个模型");
				return;
			}
			if(selected.pbProcessInstanceId == null || selected.pbProcessInstanceId == undefined  || selected.pbProcessInstanceId == 0){
				
			}else{
				notify("提示","该任务已启动流程");
				return;
			}
			var map = {
				workFlowName :  "采购流程:" + selected.pbName,
				workFlowDescribe : "test",
				userId : "${user.userId}",
			}
			//启动所需参数
			var parm = {
				cls : "com.sq.book.management.entity.PurchaseBookEntity",
				bussinessKey : selected.pbId + "",
				state : 1,
			}
			parm.map = JSON.stringify(map);
			$.post("${base}/workFlow/start.do",parm,function(returnData){
				if(returnData.result){
					notify("提示","流程启动成功");
					$('#purchaseList').datagrid('reload');
				}
			})
		})		
		  
		//取消模型
		$("#cancel").click(function(){
			$('#win').window('close');
		})
		
		//列表数据加载
		$('#purchaseList').datagrid({
			pagination:true,
			title : '采购信息列表',
			url : '${base}/manager/purchaseBook/list.do',
			singleSelect : true,
			columns : [[ 
				{field : 'pbName',title : '采购名称',width : '20%'}, 
				{field : 'pbContent',title : '采购内容',width : '40%'},
				{field : 'pbPrice',title : '采购金额',width : '10%'}, 
				{field : 'pbCreateDate',title : '创建时间',width : '20%'},
                {field : 'pbProcessState',title : '状态',width : '10%',
                    formatter: function(value,row,index){
                       return purchaseBookSateConstant.getValue(row.pbProcessState);
                    }
				},

			]],
		});
	})
</script>