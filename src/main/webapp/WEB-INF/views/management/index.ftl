<@sq.html5 easyui="true" >
	<@sq.head title="主页">
		<style>
			<#-- 去除bootstrop的padding样式 -->
			.panel-body{
				padding : 0px !important ;
			}
		</style>
	</@sq.head>
	<@sq.body>
		<div id="wrapper">
	        <nav class="navbar navbar-default top-navbar" role="navigation">
	            <div class="navbar-header">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	                <a class="navbar-brand" href="index.html">后台管理</a>
	            </div>
	
	            <ul class="nav navbar-top-links navbar-right">
	                <li class="dropdown">
	                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
	                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
	                    </a>
	                    <ul class="dropdown-menu dropdown-user">
	                        <li class="divider"></li>
	                        <li><a href="${base}/manager/logout.do"><i class="fa fa-sign-out fa-fw"></i> 退出</a>
	                        </li>
	                    </ul>
	                </li>
	            </ul>
	        </nav>
	        <!-- 左侧菜单  -->
	        <nav class="navbar-default navbar-side" role="navigation">
	            <div class="sidebar-collapse">
	                <ul class="nav" id="main-menu">
	                    <li>
	                        <a class="active-menu" href="index.html"><i class="fa fa-dashboard"></i> 首页</a>
	                    </li>
						<@shiro.hasPermission name="01010000">
							<li>
								<a id="workDesk"><i class="fa fa-bar-chart-o"></i> 工作台   </a>
							</li>
						</@shiro.hasPermission>
						<@shiro.hasPermission name="01020000">
							<li>
								<a dataUrl="${base}/manager/chart/index.do" title="统计报表" class="left-menu"><i class="fa fa-bar-chart-o"></i> 统计报表 </a>
							</li>
						</@shiro.hasPermission>
						<@shiro.hasPermission name="01030000">
							<li>
								<a dataUrl="${base}/manager/user/index.do" title="用户管理" class="left-menu"><i class="fa fa-desktop"></i> 用户管理   </a>
							</li>
						</@shiro.hasPermission>
						<@shiro.hasPermission name="01040000">
							<li>
								<a dataUrl="${base}/manager/book/index.do" title="图书及分类管理 " class="left-menu"><i class="fa fa-qrcode"></i> 图书及分类管理 </a>
							</li>
						</@shiro.hasPermission>
						<@shiro.hasPermission name="01050000">
							<li>
								<a dataUrl="${base}/manager/purchaseBook/index.do" title="采购管理" class="left-menu"><i class="fa fa-qrcode"></i> 采购管理 </a>
							</li>
						</@shiro.hasPermission>
						<@shiro.hasPermission name="01060000">
							<li>
								<a dataUrl="${base}/manager/role/index.do" title="角色及模块管理" class="left-menu"><i class="fa fa-qrcode"></i> 角色模块管理及权限分配  </a>
							</li>
						</@shiro.hasPermission>
						<@shiro.hasPermission name="01070000">
							<li>
								<a href="#"><i class="fa fa-sitemap"></i> 工作流  <span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
									<@shiro.hasPermission name="01070100">
										<li>
											<a dataUrl="${base}/model/modelPage.do" title="模型管理" class="left-menu" > 模型管理 </a>
										</li>
									</@shiro.hasPermission>
									<@shiro.hasPermission name="01070200">
										<li>
											<a dataUrl="${base}/process/processPage.do.do" title="流程管理" class="left-menu" > 流程管理 </a>
										</li>
									</@shiro.hasPermission>
								</ul>
							</li>
						</@shiro.hasPermission>
	                </ul>
	            </div>
	        </nav>
	        <div id="page-wrapper">
	            <!-- 中：内容容器 -->
				<div id="content" data-options="region:'center'" style="color:#E5EBF2;width:100%;height:100%;">
					<div id="main" class="easyui-tabs" data-options="border:false,height:'100%'" style="overflow:hidden">
						   
					</div>
				</div>
	        </div>
	    </div>
	</@sq.body>
</@sq.html5>
<script>
	
	
	$(function(){
		//根据浏览器设置页面高度
		$("body").height($(window).height());	
		$("#wrapper").css("min-height",$(window).height())
		$("#wrapper").height($(window).height());	
		
		
		//tabs 的嵌入页面，需要减去头部高度60px
		$("#page-wrapper").css("min-height",$(window).height()-60) 
		$("#page-wrapper").height($(window).height()-60);	
		$("#main").height($(window).height()-60);	
		
		/*
		 *tabs组件绑定菜单功能
		 */
		var tabs = $("#main");
		var tabMenu = {
			title: "",
			util: {
				closeTab: function(title){
					tabs.tabs('close', title);
				},
				getAllTab: function(){
					var tab = tabs.tabs('tabs');
					var titles = [];
					for (var i = 0; i < tab.length; i++) {
						titles.push(tab[i].panel('options').title);
					}
					return titles;
				},
				getTabUrl: function(title){
					var tab = tabs.tabs('getTab', title);
					var url = tab.children("iframe").get(0).src;
					return url;
				}
			},
			closeOne: function(){
				this.util.closeTab(this.title);
			},
			closeOther: function(){
				var titles = this.util.getAllTab();				
				for (var i = 0; i < titles.length; i++) {
					if (titles[i] != this.title) {
						this.util.closeTab(titles[i]);
					}
				}
			},
			closeAll: function(){
				var titles = this.util.getAllTab();
				for (var i = 0; i < titles.length; i++) {
					this.util.closeTab(titles[i]);
				}
			},
			openBlank: function(){
				var url = this.util.getTabUrl(this.title);
				window.open(url);
			},
			reload: function(){
				var tab = tabs.tabs('getTab', this.title);
				var url = tab.children("iframe").get(0).src;
				tab.children("iframe").attr("src", url);
			}
		}
	
		//tab的菜单绑定函数
		$("#tabsMenu").menu({
			onClick: function(item) {
				var callName = item.name;
				tabMenu[callName]();
			}
		});
	
		//开启tabs的菜单功能
		tabs.tabs({
			onContextMenu: function(event, title, index){
				event.preventDefault();//取消浏览器点击鼠标右键的默认动作
				$('#tabsMenu').menu('show', {
					left: event.pageX,
					top: event.pageY
				});
				tabMenu.title = title;
			},
		});
		


        $("#workDesk").click(function(){
            //加载待办事项
            addTab("main", "已处理事项", "${base}/workFlow/alreadySolvePage.action");

            //加载待办事项
            addTab("main", "待办事项", "${base}/workFlow/taskPage.action");
		})

        //加载待办事项
        addTab("main", "已处理事项", "${base}/workFlow/alreadySolvePage.action");

        //加载待办事项
        addTab("main", "待办事项", "${base}/workFlow/taskPage.action");

		//左侧菜单点击事件
		$(".left-menu").click(function(){
			if(!isBlank($(this).attr("dataUrl"))){//验证是是否有链接
				addTab("main",$(this).attr("title"), $(this).attr("dataUrl"));
			}else{
				
			}
		})
	})
</script>