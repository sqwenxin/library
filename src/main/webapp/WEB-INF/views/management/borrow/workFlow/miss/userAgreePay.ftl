<@sq.html5 easyui="true" bootstrap="false">
    <@sq.head title="管理员审核">
    </@sq.head>
    <@sq.body>
        <@sq.workFlowInfo id="workFlowInfo" processInstanceId="${processInstance.processInstanceId}">
            <form id="info">
                <table class="altrowstable text-center" >
                    <input type="hidden" id="bookId" name="bookId">
                    <input type="hidden" id="bookCode" name="bookCode">
                    <input type="hidden" id="bookBcId" name="bookBcId">
                    <input type="hidden" id="bookDescribe" name="bookDescribe">
                    <tr>
                        <td colspan="2">
                            图书信息编辑
                        </td>
                    </tr>
                    <tr>
                        <td>
                            书籍名称
                        </td>
                        <td>
                            <input class="easyui-textbox"  id="bookName" name="bookName" readonly data-options="required:true" style="width:30%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            作者
                        </td>
                        <td>
                            <input class="easyui-textbox"  id="bookAuthor" readonly data-options="required:true" name="bookAuthor" style="width:30%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            图片
                        </td>
                        <td>
                            <div id="localImag">
                                <img id="preview"  src="" style="width: 100px; height: 100px;"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            损坏描述
                        </td>
                        <td>
                            <input class="easyui-textbox" readonly id="bookDamageDescribe" name="bookDamageDescribe" style="width:30%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            所属分类
                        </td>
                        <td>
                            <input class="easyui-textbox" readonly  id="bookCategory" name="bookCategoryName" data-options="editable:false" style="width:30%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            简介
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr style="height:600px" align="center">
                        <td>
                        </td>
                        <td>
                            <script id="container" name="content" type="text/plain">
                            </script>
                        </td>
                    </tr>
                </table>
            </form>
        </@sq.workFlowInfo>

        <@sq.workFlowSolve id="workFlowSolve" taskId="${taskId}" processInstanceId="${processInstance.processInstanceId}"  backText="不同意" passText="同意">
           <div style="width;100%;margin-top:20px" align="center">
               <input type="hidden" id="missIsAgree" name="missIsAgree">
               <input class="easyui-textbox" label="意见:" name="comment" data-options="multiline:true,required:true" style="height:60px;width:80%">
           </div>
        </@sq.workFlowSolve>
    </@sq.body>
</@sq.html5>
<!-- 注意这两句话顺序一定不能反过来用 -->
<script type="text/javascript" src="${base}/static/plugin/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${base}/static/plugin/ueditor/ueditor.all.js"></script>

<script type="text/javascript" src="${base}/static/plugin/ueditor/ueditor.parse.js"></script>
<script type="text/javascript">
    var ue = UE.getEditor('container',{
        initialFrameWidth:1000,  //初始化编辑器宽度,默认1000
        initialFrameHeight:600,  //初始化编辑器高度,默认320
        readonly:true      //一加载即为只读状态--一般不用
    });

</script>
<script>

    //取消事件
    workFlowSolve.back = function(){
        $("#missIsAgree").val(false);
    };

    //通过事件
    workFlowSolve.pass = function(){
        $("#missIsAgree").val(true);
    }

    var param = {
        bbId : ${processInstance.businessKey}
    };
    $.post("${base}/common/view/getBorrowInfo.do",param,function(returnData){
        if(returnData.flag){
            //异步加载书籍信息
            $.post("${base}/manager/book/get.do","bookId=" + returnData.obj.book.bookId,function(returnData){
                if(returnData.result){
                    $("#info").form("load",returnData.data);
                    ue.ready(function() {
                        ue.setContent(returnData.data.bookDescribe);
                    });
                    //添加图片
                    $("#preview").attr("src","${base}/Book/" + returnData.data.bookImage)
                    //异步加载分类信息
                    $.post("${base}/manager/bookCatagory/get.do","bcId=" + returnData.data.bookBcId,function(returnData){
                        if(returnData.result){
                            $("#bookCategory").textbox("setValue",returnData.data.bcName);
                        }
                    });
                }
            });
        }
    });
</script>