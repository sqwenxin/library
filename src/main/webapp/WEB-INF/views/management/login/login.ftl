<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<title>Login PHP</title>
		<link rel="stylesheet" href="${base}/static/skin/default/css/mainLogin.css" />
		<script type="text/javascript" src="${base}/static/plugin/jquery/1.9.1/jquery-1.9.1.js"></script>
	</head>
	<body>
		<div class="lg-container">
			<h1>学校图书管理系统</h1>
			<form action="waka-login.php" id="lg-form" name="lg-form" method="post">
				
				<div>
					<label for="username">Username:</label>
					<input type="text" name="username" id="username" placeholder="username"/>
				</div>
				
				<div>
					<label for="password">Password:</label>
					<input type="password" name="password" id="password" placeholder="password" />
				</div>
				
				<div>				
					<button type="button" id="login">Login</button>
				</div>
				
			</form>
			<div id="message"></div>
		</div>
	</body>
</html>
<script>
	//保存，读取，删除cookies方法
	var cookie = {
	    //写cookies
	    setCookie:function(name, value){
	        var Days = 365;
	        var exp = new Date();
	        exp.setTime(exp.getTime() + Days*24*60*60*1000);
	        document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
	    },
	      
	    //读取cookies
	    getCookie:function(name){
	        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
	        if(arr=document.cookie.match(reg)) 
	            return unescape(arr[2]);
	        else 
	            return null;
	    },
	      
	    //删除cookies
	    delCookie:function(name)
	    {
	        var exp = new Date();
	        exp.setTime(exp.getTime() - 1);
	        var cval= cookie.getCookie(name);
	        if(cval!=null) document.cookie= name + "="+cval+";expires="+exp.toGMTString();
	    }
	}
	
	$(function(){
		//检测页面在iframe中
		if(self != top){  
   			top.window.location = self.window.location;  
		}
	
		if(cookie.getCookie("userLoginName") != null){
			$(".userLoginName").val(cookie.getCookie("userLoginName"));
		}
		
		//登录按钮点击事件
		$("#login").click(function(){
			//提交登录参数
			var param = {
					userCode : $("#username").val(),
					password : $("#password").val(),
				}
			//异步校验	
			$.post("${base}/manager/checkLogin.do", param ,function(returnData){
				if(returnData.result){
					cookie.setCookie("userLoginName", $("#username").val());
					window.location.href = "${base}/manager/index.do";
				}else{
					alert(returnData.msg);
				}
			});
		})		
	})
</script>