<@sq.html5 easyui="true" bootstrap="false">
	<@sq.head title="图书及分类管理">
		<style>
			#bookCatagoryForm div,#modelForm div{
				width : 80%;
				padding-top : 15px;
			}
		</style>
        <script>
            //检查图片的格式是否正确,同时实现预览
            function setImagePreview(obj, localImagId, imgObjPreview) {
                debugger;
                var array = new Array('gif', 'jpeg', 'png', 'jpg', 'bmp'); //可以上传的文件类型
                if (obj.value == '') {
                    $.messager.alert("让选择要上传的图片!");
                    return false;
                }
                else {
                    var fileContentType = obj.value.match(/^(.*)(\.)(.{1,8})$/)[3]; //这个文件类型正则很有用
                    ////布尔型变量
                    var isExists = false;
                    //循环判断图片的格式是否正确
                    for (var i = 0 ; i < array.length ; i++) {
                        if (array[i] != undefined && fileContentType.toLowerCase() == array[i].toLowerCase()) {
                            //图片格式正确之后，根据浏览器的不同设置图片的大小
                            if (obj.files && obj.files[0]) {
                                //火狐下，直接设img属性
                                imgObjPreview.style.display = 'block';
                                imgObjPreview.style.width = '100px';
                                imgObjPreview.style.height = '100px';
                                //火狐7以上版本不能用上面的getAsDataURL()方式获取，需要一下方式
                                imgObjPreview.src = window.URL.createObjectURL(obj.files[0]);
                            }
                            else {
                                //IE下，使用滤镜
                                obj.select();
                                var imgSrc = document.selection.createRange().text;
                                //必须设置初始大小
                                localImagId.style.width = "100px";
                                localImagId.style.height = "100px";
                                //图片异常的捕捉，防止用户修改后缀来伪造图片
                                try {
                                    localImagId.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
                                    localImagId.filters.item("DXImageTransform.Microsoft.AlphaImageLoader=").src = imgSrc;
                                }
                                catch (e) {
                                    $.messager.alert("您上传的图片格式不正确，请重新选择!");
                                    return false;
                                }
                                imgObjPreview.style.display = 'none';
                                document.selection.empty();
                            }
                            isExists = true;
                            return true;
                        }else{
                            isExists = false;
                        }
                    }
                    if (isExists == false) {
                        notify("提示","上传图片类型不正确!");
                        $("#idFile").val("");
                        return false;
                    }
                    return false;
                }
            }
        </script>
	</@sq.head>
	<@sq.body>
		<form id="book" enctype="multipart/form-data" style="padding-top:30px">
			<table class="altrowstable text-center" >
				<input type="hidden" id="bookId" name="bookId">
                <input type="hidden" id="bookCode" name="bookCode">
                <input type="hidden" id="bookBcId" name="bookBcId">
                <input type="hidden" id="bookDescribe" name="bookDescribe">
				<tr>
					<td colspan="2">
						图书信息编辑
					</td>
				</tr>
				<tr>
					<td>
						书籍名称	
					</td>
					<td>
						<input class="easyui-textbox"  id="bookName" name="bookName" data-options="required:true" style="width:30%">
					</td>
				</tr>
				<tr>
                    <td>
                        作者
                    </td>
                    <td>
                        <input class="easyui-textbox"  id="bookAuthor" data-options="required:true" name="bookAuthor" style="width:30%">
                    </td>
				</tr>
                <tr>
                    <td>
                        图片
                    </td>
                    <td>
                        <div id="localImag">
                            <img id="preview"  src="" style="width: 100px; height: 100px;"/>
                        </div>
                        <input id="idFile" style="width:224px" runat="server" name="bookImagePath" onchange="setImagePreview(this,localImag,preview);" type="file" />
                    </td>
                </tr>
                <tr>
                    <td>
                        损坏描述
                    </td>
                    <td>
                        <input class="easyui-textbox"  id="bookDamageDescribe" name="bookDamageDescribe" style="width:30%">
                    </td>
                </tr>
                <tr>
                    <td>
                        所属分类
                    </td>
                    <td>
                        <input class="easyui-textbox"  id="bookCategory" name="bookCategoryName" data-options="editable:false" style="width:30%">
                    </td>
                </tr>
                <tr>
                    <td>
                        简介
                    </td>
                    <td>

                    </td>
                </tr>
                <tr style="height:600px" align="center">
                    <td>
                    </td>
                    <td>
                        <script id="container" name="content" type="text/plain">
                        </script>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <a id="cancel" class="easyui-linkbutton" data-options="iconCls:'icon-no'"  >取消</a>
                        <a id="update" class="easyui-linkbutton" data-options="iconCls:'icon-ok'"  >保存</a>
                    </td>
                </tr>
			</table>
		</form>
	</@sq.body>
</@sq.html5>
 <#---->
 <#---->
<!-- 注意这两句话顺序一定不能反过来用 -->
<script type="text/javascript" src="${base}/static/plugin/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${base}/static/plugin/ueditor/ueditor.all.js"></script>

<script type="text/javascript" src="${base}/static/plugin/ueditor/ueditor.parse.js"></script>
<script type="text/javascript">
	var ue = UE.getEditor('container',{
        initialFrameWidth:1000,  //初始化编辑器宽度,默认1000
        initialFrameHeight:600,  //初始化编辑器高度,默认320
        readonly:false      //一加载即为只读状态--一般不用
    });


    //var content = ue.getContent();
</script>
<script>
	$(function(){
        var url = "${base}/manager/book/save.do";
        <#if book?exists && book.bookId?exists>
            $.post("${base}/manager/book/get.do","bookId=${book.bookId}",function(returnData){
                if(returnData.result){
                    $("#book").form("load",returnData.data);
                    debugger;
                    ue.ready(function() {
                        ue.setContent(returnData.data.bookDescribe);
                    });
                    $("#preview").attr("src","${base}/Book/" + returnData.data.bookImage)
                    url = "${base}/manager/book/update.do";
                }
            });
        </#if>
        $("#bookBcId").val("${book.bookBcId}");
	    $.post("${base}/manager/bookCatagory/get.do","bcId=${book.bookBcId}",function(returnData){
            if(returnData.result){
                $("#bookCategory").textbox("setValue",returnData.data.bcName);
            }
        });
	    //上传文件点点击事件
        $("#upload").click(function(){
            loadImage(img,obj);
		})

        //取消，及清空事件
        $("#cancel").click(function(){
            $("#book").form("clear")
            $("#bookBcId").val("${book.bookBcId}");
            $.post("${base}/manager/bookCatagory/get.do","bcId=${book.bookBcId}",function(returnData){
                if(returnData.result){
                    $("#bookCategory").textbox("setValue",returnData.data.bcName);
                }
            });
        })

        //保存事件
        $("#update").click(function(){
            if (!$('#book').form('validate')) {
                notify("提示","有信息尚未填写");
                return;
            }
            $('#update').linkbutton("disable");
            $('#cancel').linkbutton("disable");
            $("#bookDescribe").val(ue.getContent());
            var formData = new FormData($( "#book" )[0]);
            $.ajax({
                url: url ,
                type: 'POST',
                data: formData,
                async: true,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    //得到上传文件的全路径
                    $.messager.progress({
                        title:'请稍后',
                        msg:'正在努力...'
                    });
                },
                error:function(){
                    $.messager.progress('close');
                    notify("提示","更新失败");
                },
                success: function (returndata) {
                    $.messager.progress('close');
                    if(returndata.result){
                        notify("提示","更新成功");
                    }else{
                        notify("提示","发生错误更新失败");
                    }
                },
            });
        })
	})
</script>