<@sq.html5 easyui="true" bootstrap="false">
	<@sq.head title="图书及分类管理">
		<style>
			#bookCatagoryForm div,#modelForm div{
				width : 80%;
				padding-top : 15px;
			}
		</style>
	</@sq.head>
	<@sq.body>
		<div id="pannel" class="easyui-layout" style="height:100%;width:100%">   
		    <div data-options="region:'west',title:'角色管理',split:true" style="width:20%;height:100%">
		    	<div id="modelToolbar" style="background:#E0ECFF;">
		 			<@auth.linkbutton id="bookCatagoryAdd" code="0101010201" options="iconCls:'icon-add'" text="添加"/> 	
					<@auth.linkbutton id="bookCatagoryEdit" code="0101010202" options="iconCls:'icon-edit'" text="修改"/> 	
					<@auth.linkbutton id="bookCatagoryRemove" code="0101010203" options="iconCls:'icon-remove'" text="删除"/>
		   		</div>
		   		<@sq.tree id="bookCatagory" 
		   					idFile="bcId" 
		   					valueFile="bcName" 
		   					pIdFile="bcParentId" 
		   					url="${base}/manager/bookCatagory/query.do" >
		   		</@sq.tree>
		    </div>   
		    <div data-options="region:'center',title:'书籍管理'" style="background:#eee;height:100%">
		    	<@sq.tableList id="bookList" url="${base}/manager/book/list.do" title="" singleSelect="true" style="height:99%" tableStyle="height:89%"
					columns="[[
						{field:'ck',checkbox:true },
						{field:'bookCode',title:'书籍编号',width:100},
						{field:'bookName',title:'书籍名称',width:200},
						{field:'bookAuthor',title:'作者',width:100},
						{field:'bookManageState',title:'管理状态',width:100,
							formatter: function(value,row,index){
								if (row.bookManageState == 0){
									return '未借出';
								} else {
									return '已借出';
								}
							}
						},
						{field:'bookState',title:'图书状态',width:100,
							formatter: function(value,row,index){
								if (row.bookState == 0){
									return '正常';
								}else if(row.bookState == 1){
									return '损坏';
								}else if(row.bookState == 2){
									return '丢失';
								}
							}
						},
						{field:'bookDamageDescribe',title:'损坏描述',width:100},
					]]">	
					<div id="tool" style="padding-top:5px">
						<form id="searchForm">
							<label>书籍名称：</label>
							<input class="easyui-textbox"  id="bookName" name="bookName" style="width:30%">
							<a id="reset"  class="easyui-linkbutton" data-options="iconCls:'icon-remove'">重置</a>   
							<a id="search"  class="easyui-linkbutton" data-options="iconCls:'icon-search'">检索</a> 
						</form>
					</div>		
					<@auth.linkbutton id="bookAdd" code="0101010201" options="iconCls:'icon-add'" text="添加"/> 	
					<@auth.linkbutton id="bookEdit" code="0101010202" options="iconCls:'icon-edit'" text="修改"/> 	
					<@auth.linkbutton id="bookRemove" code="0101010203" options="iconCls:'icon-remove'" text="删除"/> 	
				</@sq.tableList>
		    </div>   
		</div>
		
		<!-- 分类弹窗界面   -->
		<@sq.commonWin id="bookCatagoryWin" title="角色管理" style="width:400px;height:250px"> 
			<form id="bookCatagoryForm" style="margin:0 auto;width:90%">
    			<input type="hidden" id="bcId" name="bcId">
    			<input type="hidden" id="bcParentId" name="bcParentId">
    			<div>
    				<input class="easyui-textbox" label="上级分类名称:" id="bcParentName" name="bcParentName" data-options="editable:false" style="width:100%"> 	   
    			</div>    			
    			<div >
    				<input class="easyui-textbox" label="分类名称：" id="bcName" name="bcName" data-options="required:true" style="width:100%;"> 	    			
    			</div>
    			<div >
    				<input class="easyui-textbox" label="分类描述：" id="bcDescribe" name="bcDescribe" data-options="multiline:true" style="width:100%;height:60px"> 	    			
    			</div>	    			
    		</form>
		</@sq.commonWin>

	</@sq.body>
</@sq.html5>	
<script>
	$(function(){		
		//role curd链接
		var bookCatagoryUrl = "${base}/manager/bookCatagory/save.do";
		//model curd链接
		var bookUrl = "${base}/manager/model/save.do";
		
		//角色添加
		$("#bookCatagoryAdd").click(function(){
			$("#bookCatagoryForm").form("clear");
			bookCatagoryUrl = "${base}/manager/bookCatagory/save.do";
			var selected = $("#bookCatagory").tree("getSelected");
			if(selected != null){
				$("#bcParentId").val(selected.bcId);
				$("#bcParentName").textbox("setValue",selected.bcName);
			}
			bookCatagoryWin.open();
		})

		//编辑文件分类节点
		$("#bookCatagoryEdit").click(function(){
			var selected = $("#bookCatagory").tree("getSelected");
			if(selected == null){
				notify("提示","请选择需要编辑的节点");
				return;
			}
			$("#bookCatagoryForm").form("clear");
			$("#bookCatagoryForm").form("load",selected);
			if(!isBlank(selected.bcParentId)){
				$.post("${base}/manager/bookCatagory/get.do","bcId=" + selected.bcParentId,function(returnData){
					if(returnData.result){
						$("#bcParentId").val(returnData.data.bcId);
						$("#bcParentName").textbox("setValue",returnData.data.bcName);
					}
				})
			}else{
				
			}
			bookCatagoryUrl = "${base}/manager/bookCatagory/update.do";
			bookCatagoryWin.open();
		})
		
		//删除文件分类
		$("#bookCatagoryRemove").click(function(){
			var nodes = $('#bookCatagory').tree('getSelected');
			if(nodes == null){
				notify("提示","请选择需要删除的节点");
				return;
			}
			var children = $('#bookCatagory').tree('getChildren',nodes.target);
			if(children.length != 0){
				notify("提示","请先删除子节点");
				return;
			}
			$.messager.confirm('确认','您确认想要选中的节点吗？',function(r){    
			    if (r){    
			       var params = {
			       		bcId : nodes.bcId
			       }
			       $.post("${base}/manager/bookCatagory/delete.do",params,function(returnData){
			       		notify("提示",returnData.msg);
			       		if(returnData.result){
			       			$('#bookCatagory').tree('reload');
			       		}
			       })
			    }    
			});
		})
		
		//分类弹窗确认
		bookCatagoryWin.confirm = function(){
			if (!$('#bookCatagoryForm').form('validate')) {
				notify("提示","有信息尚未填写");	
				return;
			}
			var params = $("#bookCatagoryForm").serializeObject();
			
			//异步请求
			$.post(bookCatagoryUrl,params,function(returnData){
				notify("提示",returnData.msg);
				if(returnData.result){
					$('#bookCatagory').tree('reload');
					bookCatagoryWin.close();
				}
			})
		}
		
		//选中树节点
		$('#bookCatagory').tree({
			onSelect: function(node){
			    var children  = $('#bookCatagory').tree('getChildren',node.target);
			    var bookCategoryIds = "";
				if(children.length == 0){
                    bookCategoryIds = node.bcId;
				}else{
                    bookCategoryIds = children.getFieldData("bcId") + "," + node.bcId;
				}
                var formData = $("#searchForm").serializeObject();
                formData.bookCategoryIds = bookCategoryIds;
                $('#bookList').datagrid('load',formData);
			}
		});
		
		//书籍新增
		$("#bookAdd").click(function(){
			var nodes = $('#bookCatagory').tree('getSelected');
			if(nodes == null){
				notify("提示","请选择该书籍所属分类");
				return;
			}
			addTab("main","新增书籍", "${base}/manager/book/form.do?bookBcId=" + nodes.bcId);
		})
		
		//书籍编辑
		$("#bookEdit").click(function(){
		    debugger;
		    var seelcted = $('#bookList').datagrid('getSelected');
		    if(seelcted == null){
		        notify("提示","请选择需要修改的书籍");
		        return;
			}
			addTab("main",seelcted.bookName + "编辑书籍", "${base}/manager/book/form.do?bookBcId=" + seelcted.bookBcId + "&bookId=" + seelcted.bookId);
		})

		//书籍删除
		$("#bookRemove").click(function(){
            var seelcted = $('#bookList').datagrid('getSelected');
            if(seelcted == null){
                notify("提示","请选择需要删除的书籍");
                return;
            }
            $.messager.confirm('确认','您确认想要删除选中书籍吗？',function(r){
                if (r){
                    var params = {
                        bookId : seelcted.bookId
                    }
                    $.post("${base}/manager/book/delete.do",params,function(returnData){
                        notify("提示",returnData.msg);
                        if(returnData.result){
                            $('#bookList').datagrid('reload');
                        }
                    })
                }
            });
		})

		//检索按钮
		$("#search").click(function(){
			var formData = $("#searchForm").serializeObject();
			$('#bookList').datagrid('load', formData);
		})
		
		//重置
		$("#reset").click(function(){
			$("#searchForm").form("clear");
			var formData = $("#searchForm").serializeObject();
			$('#bookList').datagrid('load', formData);
		})
	})
</script>	
