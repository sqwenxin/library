<@sq.html5 easyui="true" bootstrap="false">
	<@sq.head title="用户管理">
		<style>
			#roleForm div,#modelForm div{
				width : 80%;
				padding-top : 15px;
			}
		</style>
	</@sq.head>
	<@sq.body>
		<div id="pannel" class="easyui-layout" style="height:100%;width:100%">   
		    <div data-options="region:'west',title:'角色管理',split:true" style="width:30%;height:100%">
		    	<@sq.tableList id="roleList" url="${base}/manager/role/list.do" title="" singleSelect="true" style="height:99%" tableStyle="height:89%"
					columns="[[
						{field:'roleName',title:'角色名称',width:100},
						{field:'roleDescribe',title:'角色描述',width:200},
					]]">	
					<div id="tool" style="padding-top:5px">
						<form id="searchForm">
							<label>角色名称：</label>
							<input class="easyui-textbox"  id="roleName" name="roleName" style="width:30%"> 	    			 			
							<a id="reset"  class="easyui-linkbutton" data-options="iconCls:'icon-remove'">重置</a>   
							<a id="search"  class="easyui-linkbutton" data-options="iconCls:'icon-search'">检索</a> 
						</form>
					</div>		
					<@auth.linkbutton id="roleAdd" code="0101010201" options="iconCls:'icon-add'" text="添加"/> 	
					<@auth.linkbutton id="roleEdit" code="0101010202" options="iconCls:'icon-edit'" text="修改"/> 	
					<@auth.linkbutton id="roleRemove" code="0101010203" options="iconCls:'icon-remove'" text="删除"/> 	
				</@sq.tableList>
		    </div>   
		    <div data-options="region:'center',title:'模块管理'" style="background:#eee;height:100%">
		    	<div id="modelToolbar" style="background:#E0ECFF;">
		 			<@auth.linkbutton id="modelAdd" code="0101010201" options="iconCls:'icon-add'" text="添加"/> 	
					<@auth.linkbutton id="modelEdit" code="0101010202" options="iconCls:'icon-edit'" text="修改"/> 	
					<@auth.linkbutton id="modelRemove" code="0101010203" options="iconCls:'icon-remove'" text="删除"/>
					<@auth.linkbutton id="assignPermissions" code="0101010204" options="iconCls:'icon-ok'" text="确认分配权限"/>
		   		</div>
		    	<table id="modelTree" style="height:90%"></table> 
		    </div>   
		</div>
		
		<!-- 角色弹窗界面   -->
		<@sq.commonWin id="roleWin" title="角色管理" style="width:400px;height:250px"> 
			<form id="roleForm" style="margin:0 auto;width:90%">
    			<input type="hidden" id="roleId" name="roleId">
    			<div>
    				<input class="easyui-textbox" label="角色名称:" id="roleName" name="roleName" data-options="required:true" style="width:100%"> 	   
    			</div>    			
    			<div >
    				<input class="easyui-textbox" label="角色描述：" id="roleDescribe" name="roleDescribe" data-options="required:true,multiline:true" style="width:100%;height:60px"> 	    			
    			</div>	    			
    		</form>
		</@sq.commonWin>
		
		<!-- 模块弹窗界面   -->
		<@sq.commonWin id="modelWin" title="模块管理" style="width:400px;height:350px"> 
			<form id="modelForm" style="margin:0 auto;width:90%">
    			<input type="hidden" id="modelId" name="modelId"/>
    			<input type="hidden" id="parentId" name="modelParentCode"/>
    			<div>
    				<input class="easyui-textbox" label="父级模块:" id="modelParentCode"   data-options="editable:false" style="width:100%"> 	   
    			</div>
    			 
    			<div>
    				<input class="easyui-textbox" label="模块编号:" id="modelCode" name="modelCode" data-options="required:true" style="width:100%"> 	   
    			</div>    			
    			<div >
    				<input class="easyui-textbox" label="模块名称：" id="modelName" name="modelName" data-options="required:true" style="width:100%"> 	    			
    			</div>
    			<div >
    				<input class="easyui-textbox" label="模块链接：" id="modelLink" name="modelLink" data-options="required:true" style="width:100%"> 	    			
    			</div>	 	 
    			<div >
    				<input class="easyui-textbox" label="模块描述：" id="modelDescribe" name="modelDescribe" data-options="required:true,multiline:true" style="width:100%;height:60px"> 	    			
    			</div>	     			
    		</form>
		</@sq.commonWin>
	</@sq.body>
</@sq.html5>	
<script>
	$(function(){		
		//role curd链接
		var roleUrl = "${base}/manager/role/save.do";
		//model curd链接
		var modelUrl = "${base}/manager/model/save.do";
		
		//角色添加
		$("#roleAdd").click(function(){
			$("#roleForm").form("clear");
			roleUrl = "${base}/manager/role/save.do";
			
			roleWin.open();
		})
		
		//角色编辑
		$("#roleEdit").click(function(){
			var selected = $('#roleList').datagrid('getSelected');
			if(selected == null){
				notify("提示","请选择一个角色编辑");
				return;
			}
			$("#roleForm").form("clear");
			$("#roleForm").form("load",selected);
			roleUrl = "${base}/manager/role/update.do";
			roleWin.open();
		})
		
		//角色删除
		$("#roleRemove").click(function(){
			var selected = $('#roleList').datagrid('getSelected');
			if(checked.length == 0){
				notify("提示","请选择需要删除的记录");
				return;
			}
			$.messager.confirm('确认','您确认想要选中删除记录吗？',function(r){    
			    if (r){    
			       var params = {
			       		roleId : selected.roleId
			       }
			       $.post("${base}/manager/user/delete.do",params,function(returnData){
			       		notify("提示",returnData.msg);
			       		if(returnData.result){
			       			$('#roleList').datagrid('reload');
			       		}
			       })
			    }    
			});
		})
		
		//角色框确认按钮
		roleWin.confirm = function(){
			if (!$('#roleForm').form('validate')) {
				notify("提示","有信息尚未填写");	
				return;
			}
			var params = $("#roleForm").serializeObject();
			
			//异步请求
			$.post(roleUrl,params,function(returnData){
				notify("提示",returnData.msg);
				if(returnData.result){
					$('#roleList').datagrid('reload');
					roleWin.close();
				}
			})
		}
		
		//模块新增
		$("#modelAdd").click(function(){
			$("#modelForm").form("clear");
			modelUrl = "${base}/manager/model/save.do";
			var selected =  $('#modelTree').treegrid('getSelected');
			if(selected != null){
				$("#modelParentCode").textbox("setValue",selected.modelCode);
				$("#parentId").val(selected.modelId);
			}
			modelWin.open();
		})
		
		//模块编辑
		$("#modelEdit").click(function(){
			var selected = $('#modelTree').treegrid('getSelected');
			if(selected == null){
				notify("提示","请选择需要编辑的记录");
				return;
			}
			$("#modelForm").form("clear");
			$("#modelForm").form("load",selected);
			var param = {
				modelId : selected.modelParentCode
			};
			$.post("${base}/manager/model/get.do",param,function(returnData){
				if(returnData.result){
					$("#modelParentCode").textbox("setValue",returnData.data.modelCode);
					$("#parentId").val(returnData.data.modelId);
				}
				modelUrl = "${base}/manager/model/update.do";
				modelWin.open();
			})
		})
		
		//模块删除
		$("#modelRemove").click(function(){
			var checked = $('#modelTree').treegrid('getChecked');
			if(checked.lenght == 0){
				notify("提示","请勾选需要删除的模块");
				return;
			}
			
			$.messager.confirm('确认','您确认想要选中删除记录吗？',function(r){    
			    if (r){    
			       var params = {
			       		ids : checked.getFieldData("modelId")
			       }
			       $.post("${base}/manager/model/deleteBatch.do",params,function(returnData){
			       		notify("提示",returnData.msg);
			       		if(returnData.result){
			       			$('#modelTree').treegrid('reload');
			       		}
			       })
			    }    
			}); 
		})
		
		modelWin.confirm = function(){
			if (!$('#modelForm').form('validate')) {
				notify("提示","有信息尚未填写");	
				return;
			}
			var params = $("#modelForm").serializeObject();
			
			//异步请求
			$.post(modelUrl,params,function(returnData){
				notify("提示",returnData.msg);
				if(returnData.result){
					$('#modelTree').treegrid('reload');
					roleWin.close();
				}
			})
			modelWin.close();
		}
		
		//模块树加载
		$('#modelTree').treegrid({    
		    url:'${base}/manager/model/query.do',    
		    idField:'modelId',    	    
		    treeField:'modelName',   
		    //parentId:'modelParentCode',
		    checkbox : true,
			lines : true,

			loadFilter : function(data,parentId){
				for(var i = 0; i < data.rows.length ; i++){
					if(data.rows[i].modelParentCode != undefined){
						data.rows[i]._parentId = data.rows[i].modelParentCode;
					}else{
						data.rows[i]._parentId = null;
					}
				}
		    	return data;
		    },
		    columns:[[    
		        {field:'modelName',title:'模块名称',width:180},
		        {field:'modelCode',title:'模块编号',width:100},  
		        {field:'modelLink',title:'模块链接',width:100},
		        {field:'modelDescribe',title:'模块描述',width:100},  
		    ]]    
		});
		
		//角色列表选中事件
		$('#roleList').datagrid({
			onSelect : function(index, row){
				//取消已勾选的数据
				var checked = $("#modelTree").treegrid("getCheckedNodes");
				for(var i = 0 ; i < checked.length ; i++){
					$('#modelTree').treegrid('uncheckNode',checked[i].modelId);
				}
				
				var param = {
					rmRoleId : row.roleId
				};
				//异步加载数据
				$.post("${base}/manager/roleModel/query.do",param,function(returnData){
					for(var i = 0 ; i < returnData.total ; i++){
						$('#modelTree').treegrid('checkNode', returnData.rows[i].rmModelId);
					}
				})
			}
		});
		
		//确认分配权限
		$("#assignPermissions").click(function(){
			//选中的角色
			var selected = $('#roleList').datagrid('getSelected')
			if(selected == null){
				notify("提示","请选择需要分配的权限");
				return;
			}
			$.messager.confirm('确认','确认给该角色分配选中的权限吗？',function(r){    
			    if (r){    
			    	//勾选的节点
			  	   var checked = $("#modelTree").treegrid("getCheckedNodes");
			  	   var params = {
			  	   		roleId : selected.roleId,
			  	   		modelIds : checked.getFieldData("modelId")
			  	   }
			       $.post("${base}/manager/roleModel/update.do",params,function(returnData){
			       		notify("提示",returnData.msg);			       		
			       })
			    }    
			}); 
		})
		
		//检索按钮
		$("#search").click(function(){
			var formData = $("#searchForm").serializeObject();
			$('#roleList').datagrid('load', formData);
		})
		
		//重置
		$("#reset").click(function(){
			$("#searchForm").form("clear");
			var formData = $("#searchForm").serializeObject();
			$('#roleList').datagrid('load', formData);
		})
	})
</script>	
