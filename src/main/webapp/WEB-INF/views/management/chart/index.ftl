<@sq.html5 easyui="false" bootstrap="true">
    <@sq.head title="图书及分类管理">
        <script type="text/javascript" src="${base}/static/plugin/echarts/echarts.js"></script>
		<style>
            #bookCatagoryForm div,#modelForm div{
                width : 80%;
                padding-top : 15px;
            }
        </style>
    </@sq.head>
    <@sq.body>
        <div id="" >
            <div id="">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Charts <small>书籍报表</small>
                        </h1>
                    </div>
                </div>
                <div class="row" style="height:50%">
                    <div class="col-md-6 col-sm-12 col-xs-12" style="height:300px">
                        <div class="panel panel-default" style="height:100%">
                            <div class="panel-heading">
                                书籍分类
                            </div>
                            <div class="panel-body" style="height:100%">
                                <div id="bookCatagory" style="height:100%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12" style="height:300px">
                        <div class="panel panel-default" style="height:100%">
                            <div class="panel-heading">
                                最近阅读情况
                            </div>
                            <div class="panel-body" style="height:100%">
                                <div id="month" style="height:100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12" style="height:300px">
                        <div class="panel panel-default" style="height:100%">
                            <div class="panel-heading">
                                借阅排行
                            </div>
                            <div class="panel-body" style="height:100%">
                                <div id="order" style="height:100%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12" style="height:300px">
                        <div class="panel panel-default" style="height:100%">
                            <div class="panel-heading">
                                书籍概况
                            </div>
                            <div class="panel-body" style="height:100%">
                                <div id="managerState" style="height:100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </@sq.body>
</@sq.html5>
<script>
    $(function(){
        //异步加载图书分类信息
        $.post("${base}/manager/chart/queryByCategory.do","",function(returnData){
            // 基于准备好的dom，初始化echarts实例
            var bookCatagory = echarts.init(document.getElementById('bookCatagory'));
            // 指定图表的配置项和数据
            var option = {
                series : [{
                    name: '图书分类',
                    type: 'pie',
                    radius: '70%',
                    data:[

                    ],
                }]
            };

            var chartData = [];

            $.each(returnData, function(index, element){
                chartData.push({
                    value: element.bookNumber,
                    name: element.bcName + "," + element.bookNumber,
                });
            });

            option.series[0].data = chartData;
            bookCatagory.setOption(option);
        });

        //异步加载借阅情况折线图
        $.post("${base}/manager/chart/queryByMonth.do","",function(returnData){
            // 基于准备好的dom，初始化echarts实例
            var bookCatagory = echarts.init(document.getElementById('month'));

            // 指定图表的配置项和数据
            var option = {
                title: {
                    text: '最近一年借阅情况',
                },

                xAxis: {
                    type: 'category',
                    data: []
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    itemStyle : { normal: {label : {show: true}}},
                    data: [],
                    type: 'line'
                }]
            };
            var xAxisData = [];
            var seriesData = [];
            $.each(returnData, function(index, element){
                xAxisData.push(element.year + "-" + element.month);

                seriesData.push({
                    value: element.size,
                    name: element.year + "," + element.month,
                });
            });
            option.xAxis.data = xAxisData;
            option.series[0].data = seriesData;
            bookCatagory.setOption(option);
        });

        //借阅排行
        $.post("${base}/manager/chart/queryByOrder.do","",function(returnData){
            // 基于准备好的dom，初始化echarts实例
            var bookCatagory = echarts.init(document.getElementById('order'));

            // 指定图表的配置项和数据
            var option = {

                xAxis: {
                    type: 'category',
                    data: []
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    itemStyle : { normal: {label : {show: true}}},
                    data: [],
                    type: 'bar'
                }]
            };

            var xAxisData = [];
            var seriesData = [];

            $.each(returnData, function(index, element){
                xAxisData.push(element.bookName);

                seriesData.push(element.size);
            });

            option.xAxis.data = xAxisData;
            option.series[0].data = seriesData;
            bookCatagory.setOption(option);
        });

        //书籍状态
        $.post("${base}/manager/chart/queryByManagerState.do","",function(returnData){
            // 基于准备好的dom，初始化echarts实例
            var bookCatagory = echarts.init(document.getElementById('managerState'));

            // 指定图表的配置项和数据
            var option = {
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                        type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                legend: {
                    data: ['未借出', '已借出']
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis:  {
                    type: 'value'
                },
                yAxis: {
                    type: 'category',
                    data: returnData.bookState
                },
                series: [
                    {
                        name: '未借出',
                        type: 'bar',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight'
                            }
                        },
                        data: returnData.curInfo
                    },
                    {
                        name: '已借出',
                        type: 'bar',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight'
                            }
                        },
                        data: returnData.borrowInfo
                    }
                ]
            };

            bookCatagory.setOption(option);
        });
    })
</script>