<@sq.html5 easyui="true" bootstrap="false">
	<@sq.head title="用户管理">
		<style>
			#userForm div{
				width : 80%;
				padding-top : 15px;
			}
		</style>
	</@sq.head>
	<@sq.body>
		<@sq.tableList id="userList" url="${base}/manager/user/list.do" title="" singleSelect="false" style="height:99%" tableStyle="height:89%"
			columns="[[
				{field:'ck',checkbox:true },
				{field:'userCode',title:'用户编号',width:100},
				{field:'userName',title:'用户名称',width:100},
				{field:'userRoleId',title:'角色名称',width:100,
					formatter: function(value,row,index){
						if(row.role == undefined || row.role == null){
							return '';
						}else{
							return row.role.roleName;						
						}
					}
				},								
			]]">	
			<div id="tool" style="padding-top:5px">
				<form id="searchForm">
					<label>角色名称：</label>
					<input id="roleList" name="userRoleId" class="easyui-combobox"  data-options="valueField:'roleId',textField:'roleName',url:'${base}/manager/role/queryAll.do'" />
					<label>姓名：</label>
					<input id="userName"  class="easyui-textbox"   name="userName"  >
					<label>编号：</label>
					<input id="userCode"  class="easyui-textbox"   name="userCode"  >
					<a id="remove"  class="easyui-linkbutton" data-options="iconCls:'icon-remove'"></a>   
					<a id="search"  class="easyui-linkbutton" data-options="iconCls:'icon-search'">检索</a> 
				</form>
			</div>		
			<@auth.linkbutton id="userAdd" code="0101010201" options="iconCls:'icon-add'" text="添加"/> 	
			<@auth.linkbutton id="userEdit" code="0101010202" options="iconCls:'icon-edit'" text="修改"/> 	
			<@auth.linkbutton id="userRemove" code="0101010203" options="iconCls:'icon-remove'" text="删除"/> 	
			<!--
			<@auth.linkbutton id="import" code="0101010204" options="iconCls:'icon-add'" text="用户信息导入"/> 	
			<@auth.linkbutton id="export" code="0101010205" options="iconCls:'icon-save'" text="用户信息导出"/> 	
			-->
			<@auth.linkbutton id="reset" code="0101010206" options="iconCls:'icon-reload'" text="密码重置"/> 		 	
		</@sq.tableList>
		
		<!-- 弹窗界面   -->
		<@sq.commonWin id="userWin" title="用户管理" style="width:400px;height:250px"> 
			<form id="userForm" action="save.do" style="margin:0 auto;width:90%">
    			<input type="hidden" id="userId" name="userId">
    			<input type="hidden" id="userState" name="userState" value="0">
    			<div>
    				<input class="easyui-textbox" label="用户编号:" id="userCode" name="userCode" data-options="required:true" style="width:100%"> 	   
    			</div>    			
    			<div >
    				<input class="easyui-textbox" label="姓名：" id="userName" name="userName" data-options="required:true" style="width:100%"> 	    			
    			</div>	    			
    			<div>
    				<input id="userRoleId" label="角色：" name="userRoleId" class="easyui-combobox" style="width:100%" data-options="valueField:'roleId',textField:'roleName',url:'${base}/manager/role/queryAll.do'" /> 
    			</div>	    			
    		</form>
		</@sq.commonWin>
	</@sq.body>
</@sq.html5>
<script>
	$(function(){
		//用户更新链接
		var url = "${base}/manager/user/save.do" ; 
		
		//用户新增
		$("#userAdd").click(function(){
			$("#userForm").form("clear");
			url = "${base}/manager/user/save.do" ; 
			userWin.open();			 
		})
		
		//用户编辑
		$("#userEdit").click(function(){
			var selected = $('#userList').datagrid('getSelected');
			if(selected == null){
				notify("提示","请选择一条记录");
				return;
			}
			$("#userForm").form("clear");			 
			$("#userForm").form("load",selected)
			url = "${base}/manager/user/update.do" ; 
			userWin.open();	
		})
		
		//用户删除
		$("#userRemove").click(function(){
			var checked = $('#userList').datagrid('getChecked');
			if(checked.length == 0){
				notify("提示","请选择一条记录");
				return;
			}
			$.messager.confirm('确认','您确认想要选中删除记录吗？',function(r){    
			    if (r){    
			       var params = {
			       		ids : checked.getFieldData("userId")
			       }
			       $.post("${base}/manager/user/deleteBatch.do",params,function(returnData){
			       		notify("提示",returnData.msg);
			       		if(returnData.result){
			       			$('#userList').datagrid('reload');
			       		}
			       })
			    }    
			}); 		
		})
		
		//点击重置按钮
		$("#reset").click(function(){
			var selected = $('#userList').datagrid('getSelected');
			if(selected == null){
				notify("提示","请选择一条记录");
				return;
			}
			$.messager.confirm('确认','您确认重置' + selected.userCode +'的密码？',function(r){    
			    if (r){    
			       var params = {
			       		userId : selected.userId,
			       		userPassword : "000000"
			       }
			       $.post("${base}/manager/user/update.do",params,function(returnData){
			       		notify("提示",returnData.msg);
			       		if(returnData.result){
			       			$('#userList').datagrid('reload');
			       		}
			       })
			    }    
			}); 
		})
		
		//点击确认事件
		userWin.confirm = function(){
			if (!$('#userForm').form('validate')) {
				notify("提示","有信息尚未填写");	
				return;
			}
			var params = $("#userForm").serializeObject();
			
			//新增的时候设置密码
			if("${base}/manager/user/save.do" == url){
				params.userPassword = "000000";
			}
			
			$.post(url,params,function(returnData){
				notify("提示",returnData.msg)
				//更新成功则，关闭弹窗
				if(returnData.result){
					$('#userList').datagrid('reload');
					userWin.close();
				}
			});
		}
		
		//检索按钮
		$("#search").click(function(){
			var formData = $("#searchForm").serializeObject();
			$('#userList').datagrid('load', formData);
		})
		
		//重置表单
		$("#remove").click(function(){
			$("#searchForm").form("clear");
			var formData = $("#searchForm").serializeObject();
			$('#userList').datagrid('load', formData);
		})
	})
</script>