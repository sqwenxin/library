<@sq.html5 easyui="true" bootstrap="false">
	<@sq.head title="用户列表">
		<style>
			#tool input{
				width:100px
			}
			.easyInput{
				margin-top:15px
			}
		</style>
	</@sq.head>
	<@sq.body>
	<div data-options="region:'center',title:''" style="height:100%">
		 <div id="toolBar" style="background:#eee;">
		 	 <a id="addModel" class="easyui-linkbutton" iconcls="icon-add" plain="true">新建模型</a> 
		 	 <a id="editModel" class="easyui-linkbutton" iconcls="icon-edit" plain="true">编辑模型</a> 
		 	 <a id="deleteModel" class="easyui-linkbutton" iconcls="icon-remove" plain="true">删除模型</a> 
		 	 <a id="deployment" class="easyui-linkbutton" iconcls="icon-save" plain="true">部署模型</a> 
		 </div>
		 <table id="modelList" style="height:95%"></table>
	</div>
	<!-- modelwin -->
	<div id="win" class="easyui-window"  title="模型信息" style="width:600px;height:250px;display:none"
		        data-options="iconCls:'icon-save',modal:true,closed:true">
		    <div class="easyui-layout" data-options="fit:true">
		    	<div data-options="region:'center'">
		           <form id="modelInfo" action="create" style="width:500px;margin:0 auto" >
		           		<div class="easyInput" >
		           			<input id="name" data-options="required:true,labelWidth:'40%'" class="easyui-textbox" style="width:100%;"   label="模型名称" name="name"  value="">
		           		</div>
		           		<div class="easyInput">
		           			<input id="key" data-options="required:true,labelWidth:'40%'" name="key" class="easyui-textbox" type="text" label="模型唯一标识：" style="width:100%;">
		           		</div>  
		           		<div class="easyInput">
		           			<input id="key" data-options="required:true,labelWidth:'40%'" name="description" class="easyui-textbox" type="text" label="模型描述：" style="width:100%;">
		           		</div>           			           		
		           </form>
		        </div>
		        <div id="dlg-buttons" data-options="region:'south',split:true" style="height:35px">
		        	<div style="float:right">
		        		<a class="easyui-linkbutton" id="cancel" iconcls="icon-cancel">取消</a> 
		        		<a class="easyui-linkbutton" id="save" onclick="" iconcls="icon-save" style="margin-right:5px">保存</a> 		        		
		        	</div>        	
		        </div>       
		    </div>
		</div>	
	</@sq.body>
</@sq.html5>
<script>
	$(function() {
		
		//新建模型
		$("#addModel").click(function(){
			$('#modelInfo').form('clear');
			$('#win').window('open');
		})
		
		//保存模型
		$("#save").click(function(){
			$('#modelInfo').submit();
			$('#win').window('close');
			$('#modelList').datagrid('reload'); 
		})
		 
		//编辑模型
		$("#editModel").click(function(){
			var model = $('#modelList').datagrid('getSelected');
			window.location.href = "../process-editor/modeler.html?modelId="+model.id;
		})	
		  
		//删除模型
		$("#deleteModel").click(function(){
			var model = $('#modelList').datagrid('getSelected');
			if(model == null){
				notify("提示","请先选择一个模型");
				return;
			}
			var returnData = post("deleteModel","id="+model.id);
			if(returnData){
				notify("提示","删除成功");
				$('#modelList').datagrid('reload');
			}else{
				notify("提示",returnData);				
			}
		})
		  
		//部署模型
		$("#deployment").click(function(){
			var model = $('#modelList').datagrid('getSelected');
			if(model == null){
				notify("提示","请先选择一个模型");
				return;
			}
			var returnData = post("deploymentModel","id="+model.id);
			if(returnData){
				notify("提示","部署成功");
				$('#modelList').datagrid('reload');
			 }else{
				notify("提示",returnData);
			 }
		});
		  
		//取消模型
		$("#cancel").click(function(){
			$('#win').window('close');
		})
		
		//列表数据加载
		$('#modelList').datagrid({
			pagination:true,
			title : '流程模型列表',
			url : 'modelList.do',
			singleSelect : true,
			columns : [[ 
				{field : 'id',title : 'ID',width : 80}, 
				{field : 'name',title : '名称',width : 80}, 
				{field : 'key',title : '流程标识',width : 80}, 
				{field : 'createTime',title : '创建时间',width : 80},
				{field : 'lastUpdateTime',title : '最后更新时间',width : 80},
				{field : 'version',title : '版本',width : 80},
			]],
		});
	})
</script>