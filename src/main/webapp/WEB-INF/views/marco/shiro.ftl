<#assign authTest=true>

<#--
	按钮视图权限控制
-->
<#macro linkbutton code id="" href="" text="" plain="true" style="" options="">
	<#if authTest?string=="true">
		<a id="${id}" class="easyui-linkbutton" style="${style}" data-options="plain:${plain},<#if options?has_content>${options}</#if><#if href?has_content>,href:'${href}'</#if>">${text}</a>
	<#else>
		<@shiro.hasPermission name="${code}">
			<a id="${id}" class="easyui-linkbutton" style="${style}" data-options="plain:${plain},<#if options?has_content>${options}</#if><#if href?has_content>,href:'${href}'</#if>">${text}</a>
		</@shiro.hasPermission>
	</#if>
</#macro>

<#--
	手风琴子列表视图权限控制
-->
<#macro accordionLi code id="" text="" style="padding-left:10px;" options="">
	<#if authTest?string=="true">
		<div title="${text}" style="${style}" data-options="${options}">
			<#nested/>
		</div>
	<#else>
		<@shiro.hasPermission name="${code}">
			<div title="${text}" style="${style}" data-options="${options}">
				<#nested/>
			</div>
		</@shiro.hasPermission>
	</#if>
</#macro>

<#--
	树子列表视图权限控制
-->
<#macro treeLi code id="" url="" text="" style="" options="" title="" state="closed">
	<#if authTest?string=="true">
		<li id="${id}" data-options="attributes:{url:'${url}', title: '${title}', modelCode: '${code}'},${options}" style="padding:2px;${style}">
			<span>${text}</span>
			<#nested/>
		</li>
	<#else>
		<@shiro.hasPermission name="${code}">
			<li id="${id}" data-options="attributes:{url:'${url}', title: '${title}', modelCode: '${code}'},${options}" style="padding:2px;${style}">
				<span>${text}</span>
				<#nested/>
			</li>
		</@shiro.hasPermission>
	</#if>
</#macro>