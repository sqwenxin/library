<#--部门弹窗树-->
<#assign base = request.contextPath/>
<#macro departmentTree id url="${base}/department/queryAll.action"  style="width:600px;height:400px;display:none">
	<div id="${id}" class="easyui-window"  title="部门信息" style="${style}"
        data-options="iconCls:'icon-save',modal:true,closed:true">
    <div class="easyui-layout" data-options="fit:true">
    	<div data-options="region:'center'">
           <div >
	   			<ul id="${id}Tree">	   			
	   			</ul>   			
	   		</div>
        </div>
        <div id="dlg-buttons" data-options="region:'south',split:true" style="height:35px">
        	<div style="float:right">
        		<a class="easyui-linkbutton" id="${id}Cancel" iconcls="icon-cancel">取消</a> 
        		<a class="easyui-linkbutton" id="${id}Confirm" onclick="" iconcls="icon-save" style="margin-right:5px">确认</a> 		        		
        	</div>        	
        </div>       
    </div>
    <script>
    	$(function(){
    		$('#${id}Tree').tree({
	    		url:"${url}",
				parentField:"departmentParentId",
				textFiled:"departmentName",
				idFiled:"departmentId",
				lines:true,											
			});
    	}) 	
    </script>	
</#macro>
<#--弹窗树-->
<#macro winTree id parentField textFiled idFiled url title="" lines="true" style="width:600px;height:400px;display:none">
	<div id="${id}" class="easyui-window"  title="${title}" style="${style}"
        data-options="iconCls:'icon-save',modal:true,closed:true">
    <div class="easyui-layout" data-options="fit:true">
    	<div data-options="region:'center'">
           <div >
	   			<ul id="${id}Tree">	   			
	   			</ul>   			
	   		</div>
        </div>
        <div id="dlg-buttons" data-options="region:'south',split:true" style="height:35px">
        	<div style="float:right">
        		<a class="easyui-linkbutton" id="${id}Cancel" iconcls="icon-cancel">取消</a> 
        		<a class="easyui-linkbutton" id="${id}Confirm" onclick="" iconcls="icon-save" style="margin-right:5px">确认</a> 		        		
        	</div>        	
        </div>       
    </div>
    <script>
    	$(function(){
	    	$('#${id}Tree').tree({
	    		url:"${url}",
				parentField:"${parentField}",
				textFiled:"${textFiled}",
				idFiled:"${idFiled}",
				lines:${lines},											
			});
    	})   	
    </script>	
</#macro>
<#-- 封装树 -->
<#macro tree id  idFile valueFile pIdFile url="" data="" style="" lines="true">
	<ul id="${id}" style="${style}"> </ul> 
    <script>
    	$(function(){
	    	$('#${id}').tree({
	    		<#if data == "">
	    			url:"${url}",
	    		</#if>
	    		<#if data != "">
	    			data:"${data}",
	    		</#if>
				parentField:"${pIdFile}",
				textFiled:"${valueFile}",
				idFiled:"${idFile}",
				lines:${lines},											
			});
    	})  	
    </script>	
</#macro>
<#--列表数据-->
<#macro tableList id columns url  idField=''  title='' pagination="true" singleSelect="true" toolbarStyle="" style="height:100%" tableStyle="height:94%">
	<div data-options="region:'center',title:'${title}'" style="${style}">
		<div id="${id}Toolbar" style="background:#E0ECFF;${toolbarStyle}">
 			<#nested/>
   		</div>
		<table id="${id}" style="${tableStyle}"></table>
	 </div>  
	 <script>
	 	var ${id}Data = "";
	 	$(function(){
	 		/*
			 * 列表数据
			 */
			$('#${id}').datagrid({
		    	url:"${url}",
		    	pagination:${pagination},
		    	singleSelect:${singleSelect},
		    	<#if idField == "">
	    			idField:'${idField}',
	    		</#if>		    	
			    columns:${columns}
			});
	 	})
	 </script>
	 
</#macro>

<#--
	管控点弹框
	openButton：打开弹框的DOM id
	js
	onClick：点击管控点触发函数
-->
<#macro controlPointWindow openButton id="controlPoint" options="modal:true,closed:true,iconCls:'icon-save'">
	<!-- 管控点弹框 -->
	<div id="${id}Window" class="easyui-window" title="管控点列表" data-options="${options}" style="width:60%;height:80%">
		<div class="easyui-layout" style="height:100%;">
		
			<div data-options="region:'center',border:false" style="height:85%;padding:10px;">
				<ul id="controlPointTree" class="easyui-tree" data-options="
					idFiled: 'id',
					textFiled: 'text',
					parentField: 'parentId',
					lines:true,
				"></ul>
			</div>
			
    	</div>
    </div>
    <script>   	
    	var ${id} = {
    		onClick: null,
    	}
    	
    	$(function(){
    		var controlPointTreeData = [];
		
			//加载管控点树
			$.post("${base}/system/workProccess/list.action", {rows:0}, function(wpJson){
				if (wpJson.total > 0) {
					var wpList = wpJson.rows;
					$.post("${base}/system/controlPoint/list.action", {rows:0}, function(cpJson){
						if (cpJson.total > 0) {
							var cpList = cpJson.rows;
							
							$.each(wpList, function (index, wp) {
				                wp.id = "wp" + wp.wpId;
								wp.text = wp.wpName;
								controlPointTreeData.push(wp);
				            });
				            
				            $.each(cpList, function (index, cp) {
				            	cp.id = "cp" + cp.cpId;
								cp.text = cp.cpName;
								cp.parentId = "wp" + cp.cpWpId;
								controlPointTreeData.push(cp);
				            });
							
							$("#controlPointTree").tree('loadData', controlPointTreeData);
						}
					});
				}
			});
			
			//初始化管控点树
			$("#controlPointTree").tree({
				onSelect: function(node) {
					//只有点击管控点才有效
					if (node.parentId != undefined) {
						
						if (${id}.onClick != null) {
							${id}.onClick(node);
						}
						
						$("#${id}Window").window('close');
					}
				},
				onLoadSuccess: function(node, data) {
					$(this).tree('collapseAll');
				}
			});
		
			//打开弹框
			$("#${openButton}").click(function(){
				$("#${id}Window").window('open');
			});
    	});
    </script>
</#macro>
<#--
	测量系统tmpl追加
-->
<#macro systemTmpl id >
	<script id="${id}" type="text/x-jquery-tmpl">
		<#noparse>
			<tr class="text-center">
				<td colspan="7">
					测量设备计量要求
				</td>			
			</tr>
			<tr>
				<td>
					测量系统编号
				</td>
				<td colspan="2">
					${deviceCode}
				</td>
				<td>
					测量系统名称
				</td>
				<td colspan="3">
					${deviceName}
				</td>
			</tr>
			<tr>
				<td>
					测量范围
				</td>
				<td colspan="2">
					${deviceMeasuringRange}
				</td>
				<td>
					准确度等级/允许最大误差
				</td>
				<td colspan="3">
					${deviceMaximum}
				</td>
			</tr>
			<tr>
				<td>
					分辨力
				</td>
				<td colspan="2">
					${deviceResolution}
				</td>
				<td>
					其他
				</td>
				<td colspan="3">
					${deviceOther}
				</td>
			</tr>
		</#noparse>
	</script>
</#macro>

<#-- 消息中通知人  -->
<#macro messageWindow modelCode name="message" title="选择通知人员">
	<div id="${name}Window" class="easyui-window" title="${title}" data-options="modal:true,closed:true,iconCls:'icon-mini-alone'" style="width:60%;height:80%">
		<div class="easyui-layout" style="height:100%;">
		
			<div data-options="region:'center',border:false" style="height:85%;padding:10px;">
				<div class="assigneeContainer">
					<label><input id="${name}All" type="checkbox" />全选</label>
					<form id="${name}Form" class="assigneeForm">
						<!-- tmpl追加 -->
					</form>
				</div>
			</div>
			
			<div class="border-top" data-options="region:'south',border:false" style="height:15%;text-align:right;">
		    	<div class="vetically" style="right:1%;">
		    		<a id="${name}Cancel" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'">取消</a>
		    		<a id="${name}Save" class="easyui-linkbutton" data-options="iconCls:'icon-save'">选定</a>
		    	</div>
			</div>
		</div>
	</div>
	
	<!-- tmpl模板 -->
	<script type="text/x-jquery-tmpl" id="${name}UserTmpl">
		<#noparse>
			<div class="userInfo">
				<label><input type="checkbox" name="userIds" value="${userId}" />${userName}</label>
			</div>
		</#noparse>
	</script>
	<script>
		//组件外调对象
		var ${name} = {
			modelCode: "${modelCode}",
			sendBefore: null,
			sendAfter: null,
			openWindow: function(){
				$("#${name}Form input[name='userIds']").prop("checked", false);
				$("#${name}Window").window('open');
			},
		}
		$(function(){
			
			//全选
			$("#${name}All").click(function(){
				var isChecked = this.checked;
				$("#${name}Form input[name='userIds']").prop("checked", isChecked);
			});
			
			//加载通知人
			$.post("${base}/user/queryByModelCode.action", {modelCode:${name}.modelCode}, function(json){
				if (json.result) {
					$('#${name}UserTmpl').tmpl(json.data).appendTo('#${name}Form');
				}
			});
			
			//选定通知人
			$("#${name}Save").click(function(){
				var checked = $("#${name}Form input:checkbox:checked[name='userIds']");
				if (checked.length == 0) {
					notify("提示信息", "未选取任何通知人员");
					return;
				}
			
				var userIds = "";
				checked.each(function(index, element){
					userIds += element.value + ",";
				});
				userIds = userIds.substring(0, userIds.length - 1);
				
				if (${name}.sendBefore == null) {
					notify("提示信息", "组件数据未初始化");
				}
				
				$("#${name}Window").window('close');
				
				var initData = ${name}.sendBefore();
				//返回值为false将直接return操作
				if (!initData) {
					return;
				}
				
				var queryData = $.extend(initData, {
					userIds : userIds,
				});
			
				$.post("${base}/system/message/notice.do", queryData, function(json){
					if (json.result) {
						notify("信息提示", "发送成功");
						if (${name}.sendAfter != null) {
							${name}.sendAfter(queryData);
						}
					}
				})
			});
			
			$("#${name}Cancel").click(function(){
				$("#${name}Window").window('close');
			});
		})
	</script>	
</#macro>
