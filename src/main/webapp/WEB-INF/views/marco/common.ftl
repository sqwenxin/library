<#--通用弹窗组件
	id 数据列表Id
	title : 弹窗名称
	style ： 弹窗样式
	
	${id}.open() : 打开弹窗
	${id}.close() : 关闭弹窗
-->
<#macro commonWin id title="" style="">
	<div id="${id}" class="easyui-window"  title="${title}" style="${style}"
	    data-options="iconCls:'icon-save',modal:true,closed:true">
		<div class="easyui-layout" data-options="fit:true">
	    	<div data-options="region:'center'">
	          <#nested/>   
	        </div>
	        <div id="dlg-buttons" data-options="region:'south',split:true" style="height:35px">
	        	<div style="float:right">
	        		<a class="easyui-linkbutton" id="${id}Cancel" iconcls="icon-cancel">取消</a> 
	        		<a class="easyui-linkbutton" id="${id}Confirm" onclick="" iconcls="icon-save" style="margin-right:5px">确认</a> 		        		
	        	</div>        	
	        </div>  
		</div>     
	</div>
	<script>
		var ${id} = {
			open : function(){
				$('#${id}').window('open'); 
			},
			close : function(){
				 $('#${id}').window('close'); 
			},
			//取消事件
			cancel : function(){
				 $('#${id}').window('close'); 
			},
			//确认时间
			confirm : function(){
				 $('#${id}').window('close'); 
			},
		}
		
		//取消事件
		$("#${id}Cancel").click(function(){
			${id}.cancel();
		})
		
		//确认事件
		$("#${id}Confirm").click(function(){
			${id}.confirm();
		})
	</script>
</#macro>
<#--
	workFlowInfo 工作流任务信息界面
	id 标签ID
	processInstanceId 流程实例ID
	centerTitle 业务相关信息title名称
	${id}.getSelectedhistory()  获取选中的历史记录信息
-->
<#macro workFlowInfo id processInstanceId centerTitle="业务信息">
    <div id="${id}Form" class="easyui-layout" style="width:70%;height:100%;float: left;">
        <div data-options="region:'center',title:'${centerTitle}'" style="width:1000%;height:70%">
            <#nested/>
        </div>
        <div data-options="region:'south',title:'历史处理记录',split:true" style="width:100%;height:30%">
                <@sq.tableList id="${id}historyList" url="${base}/workFlow//history.do?procInstId=${processInstanceId}"
						title=""
						singleSelect="true"
						pagination="false"
						style="height:99%"
						tableStyle="height:89%"
						columns="[[
							 {field:'taskName',title:'任务名称',width:100,
								formatter: function(value,row,index){
										if(!isBlank(row.taskName)){
											return row.taskName;
										}else if(!isBlank(row.variables.TaskName)){
											return row.variables.TaskName;
										}else{
											return '结束';
										}
									}
								 },
								{field:'startTime',title:'任务开始时间',width:100},
								{field:'endTime',title:'任务结束时间',width:100},
								{field:'userName',title:'任务处理人',width:100,
									formatter: function(value,row,index){
										if(isBlank(row.variables)){
											return '';
										}else{
											if(isBlank(row.taskId)){
												return row.variables.startUserName;
											}
											return row.variables.userName;
										}
									}
								},
								{field:'comment',title:'意见/备注',width:100,
									formatter: function(value,row,index){
										if(isBlank(row.taskId)){
											return '';
										}
										if(isBlank(row.variables)){
											return '';
										}else{
											if(isBlank(row.taskId)){
												return '';
											}
											return row.variables.comment;
										}
									}
								},
					]]">
				</@sq.tableList>
        </div>
    </div>
	<script>
		var ${id} = {
		    getSelectedhistory : function(){
               return $('#${id}historyList').datagrid('getSelected');
			}
        }
	</script>
</#macro>

<#--
	workFlowSolve 工作流处理界面
	id
	taskId 任务ID
	processInstanceId 流程实例ID
	backShow 退回按钮是否显示 ，默认显示
	backText 退回按钮值
	passText 通过按钮值
	${id}.formData() 获取需要提交的表单数据
	${id}.back() 点击退回按钮事件
	${id}.pass() 点击通过按钮事件
-->
<#macro workFlowSolve id taskId processInstanceId backShow="true" backText="退回" passText="通过" >
	<div id="id" class="easyui-layout" style="width:29%;height:100%">
        <div data-options="region:'center',title:'处理任务'" style="width:1000%;height:100%">
            <form id="${id}Form">
				<input type="hidden" id="${id}result" name="result" value="agree">
				<#nested />
                <div  align="center" style="margin-top:30px">
					<#if backShow="true">
						<a class="easyui-linkbutton" id="${id}Back" iconcls="icon-cancel">${backText}</a>
					</#if>
                    <a class="easyui-linkbutton" id="${id}pass" iconcls="icon-ok">${passText}</a>
                </div>
            </form>
        </div>
    </div>
	<script>
		var ${id} = {
		    //获取表单对象话的值
            formData : function(){
                return $("#${id}Form").serializeObject();
			},
			//退回事件
			back : function(){
				$("#${id}result").val("disagree");
			},
			//通过事件
			pass : function(){
                $("#${id}result").val("disagree");
			} ,
			complete : function(){
                if (!$('#${id}Form').form('validate')) {
                    notify("提示","有信息尚未填写");
                    return;
                }
                debugger;
                var formData = $("#${id}Form").serializeObject();
                var param = {
                    taskId : "${taskId}",
                    procInstId : "${processInstanceId}",
                };
                param.map = JSON.stringify(formData);
                $.post("${base}/workFlow/completeTask.do",param,function(returnData){
                    if(returnData.result){
                        notify("提示","任务完成");
                        return;
					}
                    notify("提示","发生错误");
                    $("#${id}Back").linkbutton("enable");
                    $("#${id}pass").linkbutton("enable");
                })
			}
		}

		//退回事件
		$("#${id}Back").click(function(){
            $("#${id}Back").linkbutton("disable")
            $("#${id}pass").linkbutton("disable")
			${id}.back();
			${id}.complete();
        })

		//通过事件
		$("#${id}pass").click(function(){
            $("#${id}Back").linkbutton("disable")
            $("#${id}pass").linkbutton("disable")
			${id}.pass();
			${id}.complete();
		})
	</script>
</#macro>