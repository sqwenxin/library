<#--
	ui组件：
	封装了完整的模块主页逻辑
-->
<#macro ui title="">
<@qs.html5>
	<@qs.head title="${title}">
		<link rel="stylesheet" href="${base}/static/skin/default/css/module-index.css" type="text/css"/>
		<style>
			#passwordForm div{
				margin-top : 10px;
			}
		</style>
	</@qs.head>
	
	<@qs.body>
		<div id="canvas" class="easyui-layout" style="height:100%;">
		
			<!-- 上：头部 -->
			<div id="head" data-options="region:'north',border:false" style="height:36px">
				<div class="header">
					<div id="logo">
						<!--<img src="" />-->
					</div>
					
					<div class="menu-buttons">
						<#nested/>
					</div>
					<a class="easyui-linkbutton" id="totoThings" data-options="iconCls:'icon-undo',plain:true" style="float:right;margin-top:5px">待办事项</a>
					<a class="easyui-linkbutton" id="goBack" href="${base}/index.do" data-options="iconCls:'icon-mini-home',plain:true">返回主界面</a>
				</div>
			</div>
			
			<!-- 左：菜单 -->
			<div id="menu" data-options="region:'west',split:true,border:true,title:'菜单'" style="min-width:200px;width:16%;height:80%;overflow:hidden">
				
			</div>
			
			<!-- 中：内容容器 -->
			<div id="content" data-options="region:'center'" style="width:70%">
				<div id="main" class="easyui-tabs" data-options="border:false,height:'100%'" style="overflow:hidden">
					   
				</div>
			</div>
			
			<!-- 右：消息列表 -->
			<div id="userMessage" data-options="region:'east',title:'消息列表',split:true" style="width:15%">
				<div id="userMessageToolbar" style="background:#E0ECFF;" >
		 			<a id="messageDetail" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">详情</a>
		 			<a id="messageAlreadyRead" class="easyui-linkbutton" data-options="iconCls:'icon-ok',plain:true">标记为已读</a>
		   		</div>
				<table id="messageList" style="height:94%"></table>  
			</div>		
			<!-- 下：用户信息 -->
			<div data-options="region:'south',border:false" style="height:8%">
				<div class="function-buttons">
					<div class="user-info">
		                <span class="icon-mini-alone"></span>
		                <span id="userName">${user.userName?default("星星人")}</span>
		            </div>
			        
		            <div class="message-center">
		            	<span class="icon-mini-information"></span>
		            	<span><a id="messageCenter">消息中心</a></span>
		            </div>
		            <div class="chang-password">
		            	<span class="icon-mini-tool"></span>
		            	<span><a id="modifyPassword">修改密码</a></span>
		            </div>	
			        <div class="exit">
			        	<span class="icon-exit"></span>
			        	<span><a href="${base}/logout.action">退&nbsp;&nbsp;&nbsp;&nbsp;出</a></span>
			        </div>
			    </div>
		    </div>
		</div>
		
		<!-- tabs右键菜单 -->
		<div id="tabsMenu" class="easyui-menu" style="width:120px;" data-options="
			hideOnUnhover: true,
			">
			<div data-options="iconCls:'icon-reload',name:'reload'">重新加载</div>
			<div data-options="iconCls:'icon-submit',name:'openBlank'">新选项卡打开</div>
			<div class="menu-sep"></div>
			<div data-options="iconCls:'icon-clear',name:'closeOne'">关闭标签页</div>
			<div data-options="iconCls:'icon-turnOver',name:'closeOther'" >关闭其它标签页</div>
			<div data-options="iconCls:'icon-money',name:'closeAll'" >关闭所有标签页</div>
		</div>
	</@qs.body>
</@qs.html5>

<script>
	$(function(){
		var messageTotal = 0;
		//消息列表是否已打开
		var messageIsOpen = false;
		
		//默认闭合消息列表面板
		$("#canvas").layout('collapse', 'east');

		
		//消息面板增加刷新按钮
		$("#userMessage").panel({
			tools: [{
				iconCls: 'icon-reload',
				handler: function() {
					$('#messageList').datagrid('reload');  
					notify("", "刷新消息列表");
				}
		    }]
		});
		
		//左侧菜单默认加载第一按钮菜单
		var options = $(".menu-buttons a:first").linkbutton('options');
		addMune("menu", options.text, options.href);
		
		//点击菜单按钮，加载菜单
		$(".menu-buttons a").click(function(){
			var options = $(this).linkbutton('options');
			addMune("menu", options.text, options.href);
			//tabMenu.closeAll();
		});
	
		/*******tabs组件绑定菜单功能*********/
		var tabs = $("#main");
		var tabMenu = {
			title: "",
			util: {
				closeTab: function(title){
					tabs.tabs('close', title);
				},
				getAllTab: function(){
					var tab = tabs.tabs('tabs');
					var titles = [];
					for (var i = 0; i < tab.length; i++) {
						titles.push(tab[i].panel('options').title);
					}
					return titles;
				},
				getTabUrl: function(title){
					var tab = tabs.tabs('getTab', title);
					var url = tab.children("iframe").get(0).src;
					return url;
				}
			},
			closeOne: function(){
				this.util.closeTab(this.title);
			},
			closeOther: function(){
				var titles = this.util.getAllTab();
				
				for (var i = 0; i < titles.length; i++) {
					if (titles[i] != this.title) {
						this.util.closeTab(titles[i]);
					}
				}
			},
			closeAll: function(){
				var titles = this.util.getAllTab();
				
				for (var i = 0; i < titles.length; i++) {
					this.util.closeTab(titles[i]);
				}
			},
			openBlank: function(){
				var url = this.util.getTabUrl(this.title);
				window.open(url);
			},
			reload: function(){
				var tab = tabs.tabs('getTab', this.title);
				var url = tab.children("iframe").get(0).src;
				tab.children("iframe").attr("src", url);
			}
		}
	
		//tab的菜单绑定函数
		$("#tabsMenu").menu({
			onClick: function(item) {
				var callName = item.name;
				tabMenu[callName]();
			}
		});
	
		//开启tabs的菜单功能
		tabs.tabs({
			onContextMenu: function(event, title, index){
				event.preventDefault();//取消浏览器点击鼠标右键的默认动作
				
				$('#tabsMenu').menu('show', {
					left: event.pageX,
					top: event.pageY
				});
				
				tabMenu.title = title;
			},
		});
		
		//消息列表
		$('#messageList').datagrid({    
		    url:'${base}/system/messageLog/list.do',  
		    pagination:true,  
		    singleSelect:false,
		    onLoadSuccess : function(data){
		    	messageTotal =  data.total;
		    	loadTodo();
		    },
		    columns:[[  
		    	{ field:'ck',checkbox:true },  
		        {field:'messageContent',title:'消息内容',width:100,
		        	formatter: function(value,row,index){
		        		if(row.message == undefined){
		        			return;
		        		}
						return row.message.messageContent;
					}	
		        },    
		        {field:'mlState',title:'消息状态',width:100,
		        	formatter: function(value,row,index){
						if (row.mlState == 1){
							return '已读';
						} else {
							return '未读';
						}
					}		        	
		        },    
		        {field:'mlDate',title:'阅读时间',width:100},   		        
		    ]]    
		}); 	
		
		//查看消息详情按钮
		$("#messageDetail").click(function(){
			var checked = $('#messageList').datagrid('getChecked');        
			if(checked.length == 0){
				notify("提示","请选择一条消息");
				return;
			}			
			if(checked[0].message != undefined){
				$("#messageContent").textbox("setValue",checked[0].message.messageContent);
			}else{
				$("#messageContent").textbox("setValue","");
			}	
			$('#userMessageWin').window('open');  
			//消息标记为已读	
			post("${base}/system/messageLog/update.do","mlState=1&mlId=" +　checked[0].mlId + "&mlDate=" + (new Date()).format("yyyy-MM-dd"));	
			$('#messageList').datagrid('reload');  
		})	
		
		//消息标记为已读
		$("#messageAlreadyRead").click(function(){
			var checked = $('#messageList').datagrid('getChecked');    
			var ids = "0";    
			if(checked.length == 0){
				notify("提示","请选择一条消息");
				return;
			}
			for(var i = 0 ; i < checked.length ; i++){
				if(checked[i].mlState == 0){
					ids += "," + checked[i].mlId; 
				}			
			}
			var returnData = post("${base}/system/messageLog/update.do","mlState=1&ids=" + ids + "&mlDate=" + (new Date()).format("yyyy-MM-dd"));	
			if(returnData.result){
				notify("提示","标记成功");
			}else{
				notify("提示","发生错误标记失败");
			}
			$('#messageList').datagrid('reload');  
		})
		
		//关闭消息弹窗
		$("#userMessageClose").click(function(){
			$('#userMessageWin').window('close');  
		})
		
		//定时刷新函数
		function reloadData(){
			var returnData = post("${base}/system/messageLog/list.do","");
			if(messageTotal == returnData.total) {
				return ;
			}else{
				messageTotal = returnData.total;
				loadTodo();
				$('#messageList').datagrid('reload');  
			}
		}
		
		//3秒刷新
		setInterval(reloadData,3000);		
		
		//点击消息中心，打开消息列表
		$("#messageCenter").click(function(){
			if(messageIsOpen){
				$('#canvas').layout('collapse','east');  
			}else{
				$('#canvas').layout('expand','east');  
			}
		})	
		
		//组件折叠触发事件
		$('#canvas').layout({    
		    onExpand : function(region){
		    	if (region == 'east'){
					messageIsOpen = true;
				}		    	
		    },
		    onCollapse : function(region){
		    	if (region == 'east'){
					messageIsOpen = false;
				}
		    },
		}); 
		
		//加载未读消息数量
		function loadTodo(){
			var returnData = post("${base}/system/messageLog/list.do","mlState=0");
			$("#messageCenter").text("消息中心(" + returnData.total + ")")
		}
		
		//加载待办事项
		addTab("main", "待办事项", "${base}/system/todo.action");	
		
		//待办事项添加
		$("#totoThings").click(function(){
			addTab("main", "待办事项", "${base}/system/todo.action");	
		})
	});
</script>
<#--弹窗 -->
<div id="userMessageWin" class="easyui-window"  title="日常检查审核意见" style="width:500px;height:250px;display:none"
    data-options="iconCls:'icon-save',modal:true,closed:true">
  	<div class="easyui-layout" data-options="fit:true">
		<form id="userMessageForm"  style="width:400px;margin:0 auto">
			<div>
				<input class="easyui-textbox"  label="消息内容:" readonly id="messageContent" name="messageContent" data-options="
					labelWidth:'30%',
					required:true,
					height:100,
					" style="width:100%"> 	   
			</div>						   			
		</form>
        <div id="dlg-buttons" data-options="region:'south',split:true" style="height:35px">
        	<div style="float:right">
        		<a class="easyui-linkbutton" id="userMessageClose" iconcls="icon-no">关闭</a>         		
        	</div>        	
        </div>       
 	</div>
</div>

<#-- 密码修改弹窗 -->
<div id="passwordWin" class="easyui-window"  title="修改密码" style="width:500px;height:200px;display:none"
    data-options="iconCls:'icon-save',modal:true,closed:true">
  	<div class="easyui-layout" data-options="fit:true">
		<form id="passwordForm"  style="width:400px;margin:0 auto">
			<div>
				<input class="easyui-passwordbox"  label="原密码:"  id="password" name="password" data-options="
					labelWidth:'30%',
					required:true,
					" style="width:100%"> 	  					 
			</div>	
			<div>
				<input class="easyui-passwordbox"  label="新密码:"  id="newPassword" name="newPassword" data-options="
					labelWidth:'30%',
					required:true,
					" style="width:100%"> 
			</div>	
			<div>
				<input class="easyui-passwordbox"  label="确认密码:"  id="confirmPassword" name="confirmPassword" data-options="
					labelWidth:'30%',
					required:true,
					" style="width:100%"> 
			</div>				   			
		</form>
        <div id="dlg-buttons" data-options="region:'south',split:true" style="height:35px">
        	<div style="float:right">
        		<a class="easyui-linkbutton" id="passwordCancel" iconcls="icon-no">取消</a> 
        		<a class="easyui-linkbutton" id="passwordConfirm" iconcls="icon-ok">确认</a>             		
        	</div>        	
        </div>       
 	</div>
</div>
<script>
	$(function(){
		//打开修改密码弹窗
		$("#modifyPassword").click(function(){	
			$('#passwordForm').form('clear');
			$('#passwordWin').window('open');  
		})
		
		//确认弹窗
		$("#passwordConfirm").click(function(){
			if (!$('#passwordForm').form('validate')) {
				notify("提示","请把原密码，新密码，确认密码填写完整");	
				return;
			}
			if($('#confirmPassword').passwordbox("getValue") != $('#newPassword').passwordbox("getValue")){
				notify("提示","两次密码不一致");
				return;
			}
			var returnData = post("${base}/user/updatePassword.do",
								$("#passwordForm").serialize() 
								+ "&userId=${user.userId}" 								 
							 );
			if(returnData.result){
				notify("提示",returnData.msg);
				$('#passwordWin').window('close');  
				return;
			}else{
				notify("提示",returnData.msg);
				return;
			}
		})
		
		//取消
		$("#passwordCancel").click(function(){
		 	$('#passwordWin').window('close');  
		})		
	})
</script>
</#macro>