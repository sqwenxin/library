<#include "form.ftl">
<#include "ui.ftl">
<#include "audit.ftl">
<#include "upload.ftl">
<#include "common.ftl">

<#assign path = request.contextPath/>
<#assign base = request.contextPath/>
<#--<#assign imagePath = request.getSession().getServletContext().getRealPath("/") + path + "/" />-->

<#macro html5 easyui="true" bootstrap="true">
<!DOCTYPE html>
<html>
	<#if easyui == "true">
		<#assign easyui="true">
	<#elseif easyui == "false">
		<#assign easyui="false">
	</#if>
	<#if bootstrap == "true">
		<#assign bootstrap="true">
	<#elseif bootstrap == "false">
		<#assign bootstrap="false">
	</#if>
	<#nested/>
</html>
</#macro>

<#macro head title="测试">
<head>

	<meta charset="UTF-8">
	
	<title>${title}</title>
	<!-- 系统引用文件 -->
	<link rel="stylesheet" href="${base}/static/skin/default/css/base.css" type="text/css"/>
	<link rel="stylesheet" href="${base}/static/skin/default/css/icon.css" type="text/css"/>
	<script type="text/javascript" src="${base}/static/plugin/jquery/1.9.1/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="${base}/static/plugin/jquery-validation/8.0/validator.js"></script>
	<script type="text/javascript" src="${base}/static/plugin/jquery.tmpl/1.4.2/jquery.tmpl.min.js"></script>
	<script type="text/javascript" src="${base}/static/plugin/jquery-fileupload/ajaxfileupload.js"></script>
	<script type="text/javascript" src="${base}/static/skin/default/js/base.js"></script>
	<script type="text/javascript" src="${base}/static/skin/default/js/extend-jquery.js"></script>
	<script type="text/javascript" src="${base}/static/skin/default/js/constant.js"></script>
	
	<#if easyui == "true">
		<!-- easyui引用文件 -->
		<link rel="stylesheet" href="${base}/static/plugin/jquery-easyui-1.5.1/themes/icon.css" type="text/css"/>
		<link rel="stylesheet" href="${base}/static/plugin/jquery-easyui-1.5.1/themes/default/easyui.css" type="text/css"/>
		<link rel="stylesheet" href="${base}/static/skin/default/css/table.css" type="text/css"/>
		<link rel="stylesheet" href="${base}/static/skin/default/css/extend-easyui.css" type="text/css"/>
		<script type="text/javascript" src="${base}/static/plugin/jquery-easyui-1.5.1/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${base}/static/plugin/jquery-easyui-1.5.1/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="${base}/static/skin/default/js/extend-easyui.js"></script>
		<script type="text/javascript" src="${base}/static/plugin/plupload/2.3.1/plupload.full.min.js"></script>
	</#if>
	<#if bootstrap == "true">
		 <!-- Bootstrap Styles-->
	    <link href="${base}/static/plugin/bootstrap/css/bootstrap.css" rel="stylesheet" />
	    <!-- FontAwesome Styles-->
	    <link href="${base}/static/plugin/bootstrap/css/font-awesome.css" rel="stylesheet" />
	    <!-- Morris Chart Styles-->
	    <link href="${base}/static/plugin/bootstrap/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
	    <!-- Custom Styles-->
	    <link href="${base}/static/plugin/bootstrap/css/custom-styles.css" rel="stylesheet" />	
	    <!-- Bootstrap Js -->
	    <script src="${base}/static/plugin/bootstrap/js/bootstrap.min.js"></script>
	    <!-- Metis Menu Js -->
	    <script src="${base}/static/plugin/bootstrap/js/jquery.metisMenu.js"></script>
	    <!-- Morris Chart Js
	    <script src="${base}/static/plugin/bootstrap/js/morris/raphael-2.1.0.min.js"></script>
	    <script src="${base}/static/plugin/bootstrap/js/morris/morris.js"></script> -->
	    <!-- Custom Js -->
	    <script src="${base}/static/plugin/bootstrap/js/custom-scripts.js"></script>	
	</#if>
	<#nested/>
</head>
</#macro>

<#macro body class="body-mask">

<body  <#if easyui == "true">class="${class}"</#if> >
	<style>
		label{
			font-size:12px;
		}
	</style>
	<#nested/>

</body>

<script>
	<#if easyui == "true">
		$.parser.onComplete = function() {
			$('body').removeClass("body-mask");
		}
	</#if>	
	console.log("如果，你找到了这里，\n那么，你发现了我，\n我是谁？\n我是孙乾，联系方式：1391265900@qq.com");	
</script>
</#macro>

