<#--
	上传控件
	id:唯一id-必须
	folder：上传文件夹
	plain
	code
	text：上传按钮名称，默认“附件”
	readOnly：附件只读数据
	size：单个文件上传大小限制，单位可为b, kb, mb, gb, tb，默认8mb
-->
<#macro upload id folder childFolder="" plain="false" code="" text="附件" title="附件管理" readOnly="false" size="8mb" required="false">

<#if code?has_content>
	<@auth.linkbutton id="${id}" code="${code}" options="iconCls:'icon-mini-folder',plain:${plain}" text="${text}"/>
<#else>
	<a id="${id}" class="easyui-linkbutton" data-options="iconCls:'icon-mini-folder',plain:${plain}">${text}</a>
</#if>

	<!-- 附件管理弹框 -->
	<div id="${id}Window" class="easyui-window" data-options="title:'${title}',modal:true,closed:true" style="width:50%;height:70%">
		<div class="easyui-layout" style="height: 100%;">
			<div data-options="region:'center',border:false" style="height:85%;">
				
				<div id="${id}Table-toolbar" >
					<a id="${id}add" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-mini-plus'">选择文件</a>
					<a id="${id}upload" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-mini-above'">确认上传</a>
					<a id="${id}remove" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-mini-minus'">删除文件</a>
					<a id="${id}preview" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-mini-preview'">预览文件</a>
					<a id="${id}download" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-mini-under'">下载文件</a>
				</div>
				
				<div id="${id}Table-progressbar" style="display:none">
					<div id="${id}Table-progressbar" class="easyui-progressbar" data-options="value:0" style="width:80%;line-height:29px"></div>
				</div>
				
				<table id="${id}Table" class="easyui-datagrid" style="width:100%;height:100%" data-options="
					border:false,
					toolbar:'#${id}Table-toolbar',
					fit:true,
					checkbox:true,
					striped:true,
					singleSelect:true,
					rownumbers:true,
					">
					<thead>
						<tr>
							<th data-options="field:'name',width:'60%',align:'left'">文件名</th>
							<th data-options="field:'size',width:'20%',align:'left',formatter:function(value, row, index){
					        	if (value < 1024) {
					        		return Math.round(value) + '字节';
					        	} else if (value > 1024) {
					        		if(value / 1024 > 1024) {
							        	return Math.round(value / 1024 / 1024) + 'MB';
					        		} else {
							        	return Math.round(value / 1024) + 'KB';
					        		}
					        	}
							}">文件大小</th>
			            </tr>
				    </thead>
				</table>
		    </div>
		    
	    </div>
	</div>
	
	<script>
		var ${id} = {
			rootFolder: "${folder}",//根路径
			folder : "${childFolder}",//子路径
			readOnly: ${readOnly},
			required: ${required},
			upload: function() {
				if (${id}.folder == "") {
					notify("提示信息", "附件未设置路径，文件上传失败");
					return false;
				}
				if (${id}.required) {
					if (${id}Util.getFilelist().total == 0) {
						notify("提示信息", "附件未上传");
						return false;
					}
				}
				${id}Uploader.start();
				return true;
			}
		};
		
		var ${id}Util = {
			getFolder: function() {
				return ${id}.rootFolder + "/" + ${id}.folder;
			},
			getFilelist: function() {
				return $("#${id}Table").datagrid("getData");
			},
		}
		
		//实例化plupload对象
		var ${id}Uploader = new plupload.Uploader({
			browse_button : '${id}add', //触发文件选择对话框的按钮，为那个元素id
			url : '${base}/file/upload.action', //服务器端的上传页面地址
			filters : {
				/*mime_types : [ //设置允许文件上传的类型
			  		{ title : "Image files", extensions : "jpg,gif,png" }, 
			   		{ title : "Zip files", extensions : "zip" }
				],*/
				max_file_size : '${size}', //最大只能上传400kb的文件
				//prevent_duplicates : true //不允许选取重复文件
			},
			multipart_params : {//设置上传附加参数
				uploadPath : "",
			},
		});
		
		$(function() {
			if(${id}.rootFolder == "") {
				notify("提示信息", "附件根目录未设置");
				return;
			}
			<#if readOnly != "">
			if (${id}.readOnly) {
				$("#${id}add, #${id}upload, #${id}remove").hide();
			}
			</#if>
			
			<#if childFolder != "">
			$('#${id}Table').datagrid({
			    url: '${base}/file/list.action',
			    queryParams: {
			    	folder: ${id}Util.getFolder(),
			    },
			});
			</#if>
			
			//附件按钮点击事件
			$("#${id}").click(function() {
				<#if childFolder == "">
				if(${id}.folder != "") {
					$("#${id}upload, #${id}preview, #${id}download").show();
					$('#${id}Table').datagrid({
					    url: '${base}/file/list.action',
					    queryParams: {
					    	folder: ${id}Util.getFolder(),
					    },
					});
				} else {
					$("#${id}upload, #${id}preview, #${id}download").hide();
				}
				</#if>
				if (${id}.readOnly) {
					$("#${id}add, #${id}upload, #${id}remove").hide();
				}
				$("#${id}Window").window('center').window("open");
			});
	
			//初始化实例对象
			${id}Uploader.init();
	
			//文件添加时触发事件
			${id}Uploader.bind('FilesAdded', function(uploader, files) {
				for(i = 0; i < files.length; i++) {
					var file = files[i];
					$('#${id}Table').datagrid('appendRow', {
							id: file.id,
							name: file.name,
							size: file.origSize,
							uploaded: false,
					});
				}
			});
			
			//文件上传前触发
			${id}Uploader.bind('BeforeUpload', function(uploader, files) {
				var params = uploader.getOption("multipart_params");
				params.uploadPath = ${id}Util.getFolder();
				uploader.setOption("multipart_params", params);
			});
			
			//上传进行中触发
			${id}Uploader.bind('UploadProgress', function(uploader, file) {
				$('#${id}Table-progressbar').progressbar({
				    value: file.percent,
				    width: "100%",
				    text: "正在上传:" + file.name,
				});
			});
			
			//上传完成触发
			${id}Uploader.bind('UploadComplete', function(uploader, files) {
				$('#${id}Table').datagrid("reload");
				//移除列队中的所有文件
				${id}Uploader.files.splice(0, ${id}Uploader.files.length);
				$("#${id}Table-progressbar").hide();
				$("#${id}Table-toolbar").show();
				
				if ($("#${id}upload").is(":visible")) {
					notify("提示信息", "附件上传成功");
				}
			});	
			
			//上传出错时触发
			${id}Uploader.bind('Error', function(uploader, errObject) {
				if (errObject.code == -600) {
					notify("提示信息", "该上传文件超过单个文件大小限制：${size}");
				}
			});
			
			//确认上传
			$("#${id}upload").click(function() {
				if (${id}Uploader.files.length == 0) {
					notify("提示信息", "文件已上传完毕或者上传文件列队为空");
					return false;
				}
				${id}.upload()
				$("#${id}Table-progressbar").show();
				$("#${id}Table-toolbar").hide();
			});
			
			//删除文件		
			$("#${id}remove").click(function() {
				var selected = $('#${id}Table').datagrid('getSelected');
				if (selected == null) {
					notify("提示信息", "请选择一条需要删除的文件");
					return;
				}
				
				if(!selected.uploaded) {
					//上传队列中删除
					var file = ${id}Uploader.getFile(selected.id);
					${id}Uploader.removeFile(file);
					//视图中删除
					var rowIndex = $('#${id}Table').datagrid('getRowIndex', selected);
					$("#${id}Table").datagrid("deleteRow", rowIndex).datagrid("clearSelections");
				} else {
					confirm('提示信息', '文件已上传，确认删除？', function() {
						var folder = ${id}Util.getFolder();
						var queryParams = {
							"filePath": folder + "/" + selected.name,
						}
						$.post("${base}/file/remove.do", queryParams, function(){
							$("#${id}Table").datagrid("reload");
						});
					})
				}
			});
			
			//预览文件
			$("#${id}preview").click(function() {
				var selected = $('#${id}Table').datagrid('getSelected');
				if (selected == null) {
					notify("提示信息", "请选择一条需要查看的文件");
					return;
				}
				if (!selected.uploaded) {
					notify("提示信息", "文件需上传后才能预览");
					return;
				}
				
				var filePath = ${id}Util.getFolder() + "/" + selected.name;
				fileComponent.preview({
					"fileName" : selected.name,
					"filePath" : filePath,
				});
			});
			
			//下载文件
			$("#${id}download").click(function() {
				var selected = $('#${id}Table').datagrid('getSelected');
				if (selected == null) {
					notify("提示信息", "请选择一条需要下载的文件");
					return;
				}
				if (!selected.uploaded) {
					notify("提示信息", "文件未上传，不能下载");
					return;
				}
				
				var filePath = ${id}Util.getFolder() + "/" + selected.name;
				fileComponent.download({
					"fileName" : selected.name,
					"filePath" : filePath,
				});
			});
			
		})
	</script>
</#macro>