<#--选择通知人员弹框
modelCode：模块菜单编号，组件将选取所有拥有该菜单权限的人员
action：提交审核的action
name：组件名称，外调变量名
-->
<#macro assigneeWindow modelCode action="" name="assignee" title="选择通知人员">
<div id="${name}Window" class="easyui-window" title="${title}" data-options="modal:true,closed:true,iconCls:'icon-mini-alone'" style="width:60%;height:80%">
	<div class="easyui-layout" style="height:100%;">
	
		<div data-options="region:'center',border:false" style="height:85%;padding:10px;">
			<div class="assigneeContainer">
				<label><input id="${name}All" type="checkbox" />全选</label>
				<form id="${name}Form" class="assigneeForm">
					<!-- tmpl追加 -->
				</form>
			</div>
		</div>
		
		<div class="border-top" data-options="region:'south',border:false" style="height:15%;text-align:right;">
	    	<div class="vetically" style="right:1%;">
	    		<a id="${name}Cancel" class="easyui-linkbutton" data-options="iconCls:'icon-cancel'">取消</a>
	    		<a id="${name}Save" class="easyui-linkbutton" data-options="iconCls:'icon-save'">选定</a>
	    	</div>
		</div>
	</div>
</div>

<!-- tmpl模板 -->
<script type="text/x-jquery-tmpl" id="${name}UserTmpl">
<#noparse>
	<div class="userInfo">
		<label><input type="checkbox" name="userIds" value="${userId}" />${userName}</label>
	</div>
</#noparse>
</script>

<script>
	//组件外调对象
	var ${name} = {
		action: "${action}",
		modelCode: "${modelCode}",
		sendBefore: null,
		sendAfter: null,
		openWindow: function(){
			$("#${name}Form input[name='userIds']").prop("checked", false);
			$("#${name}Window").window('open');
		},
		del: function(queryParams){
			assigneeComponent.del(queryParams);
		}
	}
	
	$(function(){
		
		//全选
		$("#${name}All").click(function(){
			var isChecked = this.checked;
			$("#${name}Form input[name='userIds']").prop("checked", isChecked);
		});
		
		//加载通知人
		$.post("${base}/user/queryByModelCode.action", {modelCode:${name}.modelCode}, function(json){
			if (json.result) {
				$('#${name}UserTmpl').tmpl(json.data).appendTo('#${name}Form');
			}
		});
		
		//选定通知人
		$("#${name}Save").click(function(){
			var checked = $("#${name}Form input:checkbox:checked[name='userIds']");
			if (checked.length == 0) {
				notify("提示信息", "未选取任何通知人员");
				return;
			}
		
			var userIds = "";
			checked.each(function(index, element){
				userIds += element.value + ",";
			});
			userIds = userIds.substring(0, userIds.length - 1);
			
			if (${name}.sendBefore == null) {
				notify("提示信息", "组件数据未初始化");
			}
			
			$("#${name}Window").window('close');
			
			var initData = ${name}.sendBefore();
			//返回值为false将直接return操作
			if (!initData) {
				return;
			}
			
			var queryData = $.extend(initData, {
				rdUserIds: userIds,
				rdModel: ${name}.modelCode,
				rdProcess: 0,//表示新建
			});
		
			$.post(${name}.action, queryData, function(json){
				if (json.result) {
					notify("信息提示", "发送成功");
					if (${name}.sendAfter != null) {
						${name}.sendAfter(queryData);
					}
				}
			})
		});
		
		$("#${name}Cancel").click(function(){
			$("#${name}Window").window('close');
		});
	})
</script>
</#macro>

<#macro auditPanel id="auditBusiness" options="">
<div id="${id}" class="easyui-layout" data-options="${options}" style="height:100%;">
	<#nested/>
</div>
</#macro>

<#macro auditBusiness id="auditBusiness" options="">
<div id="${id}" data-options="region:'center',border:false,${options}" class="border-bottom" style="height:60%">
	<#nested/>
</div>
</#macro>

<#macro auditOperate id="auditOperate">
<div id="${id}" data-options="region:'south',title:'',border:false,collapsible:false" style="height:40%">
	<div class="easyui-layout" style="height:100%;">
		<div data-options="region:'center',border:false" style="padding:10px;text-align:center;" class="border-bottom">
			<form id="${id}Form" style="" >
				<div>
					<input id="${id}Comment" class="easyui-textbox" name="comment" style="width:90%;height:60px" data-options="
						multiline:true,
						required: true,
						label:'审核意见:',
						onChange: function(){},
			   		">
		   		</div>
				<#nested/>
			</form>
		</div>
		<div data-options="region:'south',border:false" style="height:20%;padding:5px;text-align:center;background-color: #f9f9f9;" class="">
    		<a id="${id}Ok" class="easyui-linkbutton" data-options="iconCls:'icon-ok',plain:true">审核通过</a>
			<a id="${id}No" class="easyui-linkbutton" data-options="iconCls:'icon-no',plain:true">审核不通过</a>
	    </div>
   </div>
</div>

<script>
	var ${id} = {
		onConfirm: function(data){},
		disable: function(){
			$("#${id}Ok, #${id}No").linkbutton('disable');
		},
	}
		
	$(function(){
		$("#${id}Ok, #${id}No").click(function(){
			var options = $(this).linkbutton('options');
			if (options.disabled) {
				return;
			}
			
			//验证操作表单
			if (!$('#${id}Form').form('validate')) {
				//notify("提示信息", "审核表单未填写完整");
				return;
			}
			
			$.messager.confirm({
				ok: "是", 
				cancel: "否",
				title: "提示",
				msg: "是否确定？",
				fn: function(r) {
					if (r) {
						var data = $("#${id}Form").serializeObject();
						if (options.id == "${id}Ok") {
							data.passed = true;
						} else {
							data.passed = false;
						}
						${id}.onConfirm(data);
						$("#${id}Ok, #${id}No").linkbutton('disable');
					}
				}
			});
		});
	});
</script>
</#macro>