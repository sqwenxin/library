<!DOCTYPE html>
<html lang="en">

<head>
    <#include "headerFile.ftl">
</head>

<body>
<!--  Preloader  -->
<div id="preloader">
    <div id="loading"> </div>
</div>
<#include "header.ftl">
<section class="shopping-cart">
    <!-- .shopping-cart -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">主页</a></li>
                    <li class="breadcrumb-item active">登陆</li>
                </ol>
            </div>
            <div class="col-md-12">
                <!-- Accordions -->
                <div class="tabContent" id="tabContent1">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">确认借阅 </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div id="checkout-step-review" class="step a-item" style="">
                                        <div class="order-review" id="checkout-review-load">
                                            <div id="checkout-review-table-wrapper">
                                                <table class="data-table linearize-table checkout-review-table" id="checkout-review-table">
                                                    <thead>
                                                    <tr>
                                                        <th rowspan="1">书籍名称</th>
                                                        <th colspan="1" class="a-center">作者</th>
                                                        <th rowspan="1" class="a-center"></th>
                                                        <th colspan="1" class="a-center"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="borrowDetail">

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="checkout-review-submit">
                                                <div class="col-xs-12 col-sm-6 col-md-6 text-left">
                                                    <ul class="pagination">
                                                        <li><a id="prePage" href="#">上一页</a></li>
                                                        <li><a id="nextPage" href="#">下一页</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">借阅事项处理</a>
                        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div id="checkout-step-review" class="step a-item" style="">
                                <div class="order-review" id="checkout-review-load">
                                    <div id="checkout-review-table-wrapper">
                                        <table class="data-table linearize-table checkout-review-table" id="checkout-review-table">
                                            <thead>
                                            <tr>
                                                <th rowspan="1">事项名称</th>
                                                <th colspan="1" class="a-center">当前任务</th>
                                                <th colspan="1" class="a-center">任务开始时间</th>
                                                <th rowspan="1" class="a-center" style="width:170px"></th>
                                                <th rowspan="1" class="a-center" style="width:190px"></th>
                                            </tr>
                                            </thead>
                                            <tbody id="flowList">

                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="checkout-review-submit">
                                        <div class="buttons-set" id="review-buttons-container">
                                            <button type="submit" title="Place Order" class="button btn-checkout">Place Order</button>
                                            <p class="f-left">Forgot an Item? <a href="#">Edit Your Cart</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<#include "foot.ftl">

</body>

</html>
<script>
    $(function(){
        $(".header-outer").remove();
        $(".all-open").removeClass("all-open")
    })
</script>

<#noparse>
    <script id="borrowPreview" type="text/x-jquery-tmpl">
        <tr>
            <td >
                ${book.bookName}
            </td>
            <td >
                <span class="price">${book.bookAuthor}</span>
            </td>
            <td>
                <a class="add-btn confirm" onclick="confirm(${bbId})">确认借阅</a>
            </td>
            <td>
                <a class="add-btn confirm" onclick="cancel(${bbId})">删除</a>
            </td>
        </tr>
    </script>
</#noparse>
<script>

    //确认借阅
    function confirm(bbId){
        var param = {
            bbId : bbId
        };
        $.post("${base}/common/view/getBorrowInfo.do",param,function(returnData){
            if(returnData.flag){
                if(returnData.obj.bbProcessInstanceId == null || returnData.obj.bbProcessInstanceId == undefined  || returnData.obj.bbProcessInstanceId == 0){

                }else{
                    notify("提示","该任务已启动流程");
                    return;
                }
                var map = {
                    workFlowName :  "借阅流程:" + returnData.obj.book.bookName,
                    workFlowDescribe : "借阅流程",
                    userId : "${user.userId}",
                }

                //启动所需参数
                var parm = {
                    cls : "com.sq.book.management.entity.BookBorrowEntity",
                    bussinessKey : returnData.obj.bbId + "",
                    state : 1,
                }
                parm.map = JSON.stringify(map);
                //异步请求数据
                $.post("${base}/workFlow/start.do",parm,function(returnData){
                    if(returnData.result){
                        alert("流程启动成功");
                    }
                })
                window.location.reload();
            }
        })
    }

    //取消借阅
    function cancel(bbId){
        var param = {
            bbId : bbId
        };
        $.post("${base}/common/view/cancelBorrow",param,function(returnData){
            if(returnData.flag){
                location.reload()
            }else{
                alert("删除失败");
            }
        });
    }

    $(function(){
        //异步加载预借阅信息
        var param = {
            bbProcessState : 0,
            page : ${page}
        }
        $.post("${base}/common/view/queryBorrowInfo.do",param,function(returnData){
            if(returnData.flag == false){
                $("#bookSize").text(0);
                return;
            }
            var pageNum = returnData.page.pageNum;
            if(returnData.page.pageNum == 0){
                $("#prePage").attr("href","${base}/common/view/reading.do?page=1");
                $("#nextPage").attr("href","${base}/common/view/reading.do?page=1");
            }else{
                if(returnData.page.isFirstPage || returnData.page.pageNum == 0){
                    $("#prePage").attr("href","${base}/common/view/reading.do?page=1");
                }else{
                    $("#prePage").attr("href","${base}/common/view/reading.do?page=" + (pageNum-1));
                }
                if(returnData.page.isLastPage){
                    $("#nextPage").attr("href","${base}/common/view/reading.do?page=" + pageNum);
                }else{
                    $("#nextPage").attr("href","${base}/common/view/reading.do?page=" + (pageNum+1));
                }
            }
            if(returnData.total > 0){
                $("#bookSize").text(returnData.total);
                $( "#borrowPreview" ).tmpl( returnData.rows ) .appendTo( "#borrowDetail" );
            }
        })
    })
</script>
<!-- 流程相关代码 -->
<#noparse>
    <script id="todoInfo" type="text/x-jquery-tmpl">
        <tr>
            <td >${processVariables.workFlowName}</td>
            <td class="a-center">${taskName}</td>
            <td class="a-center">${createTime}</td>
            <td class="a-center">
                <a class="add-btn confirm" onclick="solve('${pocessInstanceId}','${taskFormKey}','${taskId}')">处理任务</a>
            </td>
            <td class="a-center">
                <a class="add-btn confirm" onclick="look('${pocessInstanceId}')">查看流程图</a>
            </td>
        </tr>
    </script>
</#noparse>
<script>
    $(function(){
        var param = {
          page : 1,
          rows : 10000
        };
        $.post("${base}/workFlow/todoList.do",param,function(returnData){
            $( "#todoInfo" ).tmpl( returnData.rows ) .appendTo( "#flowList" );
        })
    })

    //处理任务
    function solve(pocessInstanceId,taskFormKey,taskId){
        window.open("${base}/workFlow/form.do?procInstId="  + pocessInstanceId + "&formKey=" + taskFormKey + "&taskId=" + taskId)
    }

    //查看流程图
    function look(pocessInstanceId){
        window.open("${base}/workFlow/viewer.do?procInstId=" + pocessInstanceId)
    }


</script>