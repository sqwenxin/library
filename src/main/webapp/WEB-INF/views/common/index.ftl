
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 公共文件 -->
    <#include "headerFile.ftl">
</head>
<body>
<div id="preloader">
    <div id="loading">
    </div>
</div>
<!-- 公共头部文件 -->
<#include "header.ftl">

<!-- 分类名称 + 现实的数据 -->
<section class="all-product">
    <div class="container">
        <div class="row">
            <div class="title">
                <h2>
                    LATEST POSTS BLOG
                </h2>
            </div>
            <div class="home-blog">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="row">
                        <div class="owl-demo-outer">
                            <div class="item" id="book1">
                            </div>
                            <div class="item" id="book2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="testimonal">
            </div>
            <div class="free-shipping">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="icon-shipping">
                            <i class="icon-rocket icons"></i>
                        </div>
                        <div class="shipping-text">
                            <h4>free shipping </h4>
                            <p>Free Shipping On All Order</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="icon-shipping">
                            <i class="icon-earphones-alt icons"></i>
                        </div>
                        <div class="shipping-text">
                            <h4>online support 24/7</h4>
                            <p>Technical Support 24/7</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="icon-shipping">
                            <i class="icon-refresh icons"></i>
                        </div>
                        <div class="shipping-text">
                            <h4>MONEY BACK GUARANTEE </h4>
                            <p>30 Day Money Back</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="icon-shipping">
                            <i class="icon-badge icons"></i>
                        </div>
                        <div class="shipping-text">
                            <h4>MEMBER DISCOUNT</h4>
                            <p>Upto 40% Discount</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<#include "foot.ftl">
</body>
</html>
<#noparse>
    <script id="bookTmpl" type="text/x-jquery-tmpl">
        <div class="item">
            <div class="bdr col-xs-12 col-sm-6 col-md-4">
                <div class="blog-outer">
                    <div class="blog-img">
                        <img src="/Book/Book/${bookImage}" style="width: 220px;height: 300px;" alt="lt-blog-img1">
                    </div>
                    <div class="blog-text-outer">
                        <a href="#">
                            <h4>${bookName}</h4>
                        </a>
                        <p><span class="dt"></span> <span class="ath">${bookAuthor}</span></p>
                        <p class="content-text"></p>
                        <a  onclick="borrow(${bookId})" class="add-btn">借阅</a>
                        <a  onclick="detail(${bookId})" class="add-btn">详情</a>
                    </div>
                </div>
            </div>
        </div>
    </script>
</#noparse>

<script>

    //书籍详情
    function detail(id){
        var param = {
            bookId : id
        };
        window.location.href = "${base}/common/view/detail.do?bookId=" + id;
    }

    //借阅书籍
    function borrow(id){
        var param = {
            bookId : id
        };
        //异步借阅书籍
        $.post("${base}/common/view/borrow.do",param,function(returnData){
            alert(returnData.message);
        })
    }

    $(function(){
        //异步书籍数据幻灯
        $.post("${base}/common/view/queryBook.do","pageSize=5",function(returnData){
            if(returnData.total > 0){
                var firstRow = new Array();
                var secondRow = new Array();
                for(var i = 0 ; i < returnData.rows.length ; i++){
                    if(i >=0 && i < 3){
                        firstRow.push(returnData.rows[i])
                    }
                    if(i >=3 && i < 6){
                        secondRow.push(returnData.rows[i])
                    }
                    if(i >= 6){
                        break;
                    }
                }
                $( "#bookTmpl" ).tmpl( firstRow ) .appendTo( "#book1" );
                $( "#bookTmpl" ).tmpl( secondRow ) .appendTo( "#book2" );
            }
        })

    })
</script>