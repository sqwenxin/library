
<#assign path = request.contextPath/>
<#assign base = request.contextPath/>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>
<link rel="stylesheet" href="${base}/static/plugin/html/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="${base}/static/plugin/html/css/bootstrap-dropdownhover.min.css" type="text/css">
<link rel="stylesheet" href="${base}/static/plugin/html/font/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="${base}/static/plugin/html/simple-line-icon/css/simple-line-icons.css" type="text/css">
<link rel="stylesheet" href="${base}/static/plugin/html/stroke-gap-icons/stroke-gap-icons.css" type="text/css">
<link rel="stylesheet" href="${base}/static/plugin/html/css/animate.min.css" type="text/css">
<link rel="stylesheet" href="${base}/static/plugin/html/css/style.css" type="text/css">
<link rel="stylesheet" href="${base}/static/plugin/html/css/baguetteBox.css">
<link href="${base}/static/plugin/html/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="${base}/static/plugin/html/owl-carousel/owl.theme.css" rel="stylesheet">
<script src="${base}/static/plugin/jquery/1.9.1/jquery-1.9.1.js"></script>
<script src="${base}/static/plugin/html/js/bootstrap.min.js"></script>
<script src="${base}/static/plugin/html/js/bootstrap-dropdownhover.min.js"></script>
<script src="${base}/static/plugin/html/js/jquery.easing.min.js"></script>
<script src="${base}/static/plugin/html/js/wow.min.js"></script>
<script src="${base}/static/plugin/html/owl-carousel/owl.carousel.js"></script>
<script src="${base}/static/plugin/html/js/custom.js"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<!--  jcarousel Theme JavaScript  -->
<script type="text/javascript" src="${base}/static/plugin/html/js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="${base}/static/plugin/html/js/jcarousel.connected-carousels.js"></script>
<script type="text/javascript" src="${base}/static/plugin/html/js/jquery.elevatezoom.js"></script>

<!--  Custom Theme JavaScript  -->
<script src="${base}/static/plugin/html/js/filter-price.js"></script>
<script src="${base}/static/plugin/html/js/custom.js"></script>