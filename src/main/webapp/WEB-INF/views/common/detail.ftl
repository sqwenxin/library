<#assign path = request.contextPath/>
<#assign base = request.contextPath/>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 公共文件 -->
    <#include "headerFile.ftl">
</head>
<body>
<div id="preloader">
    <div id="loading"> </div>
</div>
<#include "header.ftl">
<section class="grid-shop">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="weight">
                </div>
                <div class="weight">
                </div>
                <div class="weight">
                </div>
                <div class="weight">
                </div>
            </div>
            <div class="col-sm-9 col-md-9">
                <div class="grid-spr">
                    <div class="col-sm-6 col-md-6"> <a href="#" class="grid-view-icon"><i class="fa fa-th-large" aria-hidden="true"></i></a> <a href="#" class="list-view-icon"><i class="fa fa-list" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-sm-6 col-md-6 text-right"> <strong> ${book.bookName}-${book.bookAuthor}<span></span></strong> </div>
                </div>
                <div id="">
                ${book.bookDescribe}
                </div>
            </div>
        </div>
    </div>
</section>
<#include "foot.ftl">
</body>
</html>
<script>
    $(function(){
        $(".header-outer").remove();
    })
</script>