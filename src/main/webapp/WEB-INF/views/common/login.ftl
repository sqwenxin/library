<!DOCTYPE html>
<html lang="en">

<head>
    <#include "headerFile.ftl">
</head>

<body>
<!--  Preloader  -->
<div id="preloader">
    <div id="loading"> </div>
</div>
<#include "header.ftl">
<section class="shopping-cart">
    <!-- .shopping-cart -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Accordions -->
                <div class="tabContent" id="tabContent1">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">

                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="col-sm-6 col-md-6" style="width:30%!important;">

                                    </div>
                                    <div class="col-sm-6 col-md-6" >
                                        <div class="shipping-outer">
                                            <div class="row">
                                                <div class="col-md-12 counttry">
                                                    <div class="lable">账号:</div>
                                                    <input id="username" placeholder="用户名" type="text">
                                                </div>
                                                <div class="col-md-12 counttry">
                                                    <div class="lable">密码:</div>
                                                    <input id="password" placeholder="密码" type="password">
                                                </div>
                                            </div>
                                            <div class="fbtw">
                                                <a  id="login" class="twi-btn"><i class="fa fa-twitter" aria-hidden="true"></i>Login</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<#include "foot.ftl">

</body>

</html>
<script>
    $(function(){
        $(".header-outer").remove();

        //登录按钮点击事件
        $("#login").click(function(){
            //提交登录参数
            var param = {
                userCode : $("#username").val(),
                password : $("#password").val(),
            }
            //异步校验
            $.post("${base}/manager/checkLogin.do", param ,function(returnData){
                if(returnData.result){
                    window.location.href = "${base}/common/view/index.do";
                }else{
                    alert(returnData.msg);
                }
            });
        })
    })

</script>