<#assign path = request.contextPath/>
<#assign base = request.contextPath/>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 公共文件 -->
    <#include "headerFile.ftl">
</head>
<body>
<div id="preloader">
    <div id="loading"> </div>
</div>
<#include "header.ftl">
<section class="grid-shop">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="weight">
                </div>
                <div class="weight">
                </div>
                <div class="weight">
                </div>
                <div class="weight">
                </div>
            </div>
            <div class="col-sm-9 col-md-9">
                <div class="grid-spr">
                    <div class="col-sm-6 col-md-6"> <a href="#" class="grid-view-icon"><i class="fa fa-th-large" aria-hidden="true"></i></a> <a href="#" class="list-view-icon"><i class="fa fa-list" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-sm-6 col-md-6 text-right"> <strong>Showing 1-12 <span>of 30 relults</span></strong> </div>
                </div>
                <div id="bookList">
                </div>



                <div class="col-xs-12">
                    <div class="grid-spr pag">
                        <!-- .pagetions -->
                        <div class="col-xs-12 col-sm-6 col-md-6 text-left">
                            <ul class="pagination">
                                <li><a id="prePage" href="#">上一页</a></li>
                                <li><a id="nextPage" href="#">下一页</a></li>
                            </ul>
                        </div>
                        <!-- /.pagetions -->
                        <!-- .Showing -->
                        <div class="col-xs-12 col-sm-6 col-md-6 text-right">
                            <strong>Showing 1-12 <span>of 30 relults</span></strong>
                        </div>
                        <!-- /.Showing -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<#include "foot.ftl">
</body>
</html>
<script>
    $(function(){
        $(".header-outer").remove();
    })
</script>
<#noparse>
    <script id="bookInfo" type="text/x-jquery-tmpl">
         <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="pro-text text-center">
                <div class="pro-img"> <img src="/Book/Book/${bookImage}" style="width:220px;height:300px" alt="${bookName}">
                </div>
                <div class="pro-text-outer"> <span>${bookName}</span>
                    <a href="#">
                        <h4> ${bookAuthor}</h4>
                    </a>
                    <p class="wk-price"> </p>
                    <a onclick="borrow(${bookId})" class="add-btn">借阅</a>
                    <a  onclick="detail(${bookId})" class="add-btn">详情</a>
                </div>
            </div>
        </div>
    </script>
</#noparse>


<script>

    //书籍详情
    function detail(id){
        var param = {
            bookId : id
        };
        window.location.href = "${base}/common/view/detail.do?bookId=" + id;
    }


    //借阅书籍
    function borrow(id){
        var param = {
            bookId : id
        };
        //异步借阅书籍
        $.post("${base}/common/view/borrow.do",param,function(returnData){
            alert(returnData.message);
        })
    }

    $(function(){
        //异步书籍数据幻灯
        $.post("${base}/common/view/queryBook.do","rows=12&bookName=${bookName}&categoryId=${categoryId}&page=${pageNum}",function(returnData){
            var pageNum = returnData.page.pageNum;
            if(returnData.page.pageNum == 0){
                $("#prePage").attr("href","${base}/common/view/category.do?bookName=${bookName}&categoryId=${categoryId}&page=1");
                $("#nextPage").attr("href","${base}/common/view/category.do?bookName=${bookName}&categoryId=${categoryId}&page=1");
            }else{
                if(returnData.page.isFirstPage || returnData.page.pageNum == 0){
                    $("#prePage").attr("href","${base}/common/view/category.do?bookName=${bookName}&categoryId=${categoryId}&page=1");
                }else{
                    $("#prePage").attr("href","${base}/common/view/category.do?bookName=${bookName}&categoryId=${categoryId}&page=" + (pageNum-1));
                }
                if(returnData.page.isLastPage){
                    $("#nextPage").attr("href","${base}/common/view/category.do?bookName=${bookName}&categoryId=${categoryId}&page=" + pageNum);
                }else{
                    $("#nextPage").attr("href","${base}/common/view/category.do?bookName=${bookName}&categoryId=${categoryId}&page=" + (pageNum+1));
                }
            }
            if(returnData.total > 0){
                $( "#bookInfo" ).tmpl( returnData.rows) .appendTo( "#bookList" );
            }
        })

    })
</script>