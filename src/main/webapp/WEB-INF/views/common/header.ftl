<script type="text/javascript" src="${base}/static/plugin/jquery.tmpl/1.4.2/jquery.tmpl.min.js"></script>
<header>
    <div class="top-header">
        <div class="container">
            <div class="col-md-6">
                <div class="top-header-left">
                    <ul>
                        <li>
                            <div class="dropdown">
                                <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                                    中文
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">中文</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                        </li>
                        <li>
                            <span>欢迎登陆图书借阅系统</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div class="top-header-right">
                    <ul>
                        <li><i class="icon-location-pin icons" aria-hidden="true"></i> Store Location</li>
                        <li><i class="icon-note icons" aria-hidden="true"></i> Track Your Order</li>
                        <li>
                            <div class="dropdown">
                                <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                                    <i class="icon-settings icons" aria-hidden="true"></i> Setting
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="${base}/common/view/login.do">登陆</a></li>
                                    <li><a id="changePassword">修改密码</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <section class="top-md-menu">
        <div class="container">
            <div class="col-sm-3">
                <div class="logo">
                    <h6><img src="${base}/static/plugin/html/images/logo.png" alt="logo" /></h6>
                </div>
            </div>
            <div class="col-sm-6">
                <form>
                    <div class="well carousel-search hidden-phone">
                        <div class="btn-group">
                            <a class="btn dropdown-toggle btn-select" data-toggle="dropdown" href="#">图书检索 <span class="caret"></span></a>
                        </div>
                        <div class="search">
                            <input type="text" id="searchValue" placeholder="图书名称" />
                        </div>
                        <div class="btn-group">
                            <button type="button" id="btnSearch" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-3">
                <div class="cart-menu">
                    <ul>
                        <li id="myCart" class="dropdown">
                            <a href="#" data-toggle="dropdown" data-hover="dropdown"><i class="icon-basket-loaded icons" aria-hidden="true"></i></a><span id="bookSize" class="subno"></span><strong>预借阅</strong>
                            <div id="preview" class="dropdown-menu  cart-outer">
                            </div>
                        </li>

                    </ul>
                </div>
                <!-- cart-menu End -->
            </div>
            <div class="main-menu">
                <!--  nav  -->
                <nav class="navbar navbar-inverse navbar-default">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" data-hover="dropdown" data-animations=" fadeInLeft fadeInUp fadeInRight">
                        <ul class="nav navbar-nav">
                            <li class="all-departments dropdown">
                                <a href="index.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span> 图书分类 </span> <i class="fa fa-bars" aria-hidden="true"></i> </a>
                                <ul id="menu" class="dropdown-menu dropdownhover-bottom all-open" role="menu">

                                </ul>
                            </li>
                            <li><a href="${base}/common/view/index.do">主页</a></li>
                            <li><a href="${base}/common/view/category.do">分类检索</a></li>
                            <li><a href="${base}/common/view/reading.do">借阅管理</a></li>
                            <li><a href="${base}/common/view/records.do">借阅记录</a></li>
                            <li><a href="${base}/common/view/login.do">登陆</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </section>
    <section class="header-outer">
        <div class="header-slider">
            <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner" id="Carousel">

                </div>
                <ol class="carousel-indicators">
                    <li data-target="#home-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#home-slider" data-slide-to="1"></li>
                    <li data-target="#home-slider" data-slide-to="2"></li>
                </ol>
            </div>
        </div>
    </section>
</header>
<script>
    $(function(){

        //查询父及分类
        $.post("${base}/common/view/queryCategoryParent.do","",function(returnData){
            if(returnData.length > 0){
                $( "#movieTemplate" ).tmpl( returnData ) .appendTo( "#menu" );
            }
        })

        //异步加载循环幻灯
        $.post("${base}/common/view/queryBook.do","pageSize=5",function(returnData){
            if(returnData.total > 0){
                for(var i = 0 ; i < returnData.rows.length ; i++){
                    returnData.rows[i].index = i;
                }
                $( "#indexShowTmpl" ).tmpl( returnData.rows ) .appendTo( "#Carousel" );
            }
        })

        //异步加载预借阅信息
        $.post("${base}/common/view/queryBorrowInfo.do","bbProcessState=0",function(returnData){
            if(returnData.flag == false){
                $("#bookSize").text(0);
                return;
            }
            if(returnData.total > 0){
                $("#bookSize").text(returnData.total);
                $( "#borrowInfo" ).tmpl( returnData.rows ) .appendTo( "#preview" );
            }
        })

        //检索书籍
        $("#btnSearch").click(function(){
            window.location.href="${base}/common/view/category.do?bookName=" + $("#searchValue").val();
        })
    })
</script>
<#-- 使用tmpl插件，追加数据 -->
<#noparse>
    <script id="movieTemplate" type="text/x-jquery-tmpl">
        <li class="dropdown parentCategory">
            <a href="category.do?categoryId=${bcId}">
                 ${bcName}
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
            {{if children.length > 0}}
                <ul class="dropdown-menu right childrenCategory">
                    {{each children}}
                        <li><a href="category.do?categoryId=${$value.bcId}">${$value.bcName}</a></li>
                    {{/each}}
                </ul>
            {{/if}}
        </li>
    </script>
</#noparse>
<script>
    $(function(){
        /**
         * 由于元素是后期添加的会导致后期添加的元素事件失效，所以需要使用delegate事件处理
         */
        //鼠标移开事件
        $("#menu").delegate(".parentCategory","mouseout",function(){
            if($(this).find(".childrenCategory").length > 0){
                $(this).find(".childrenCategory").removeClass('show');
            }
        });

        //鼠标移入父级菜单事件
        $("#menu").delegate(".parentCategory","mouseover",function(){
            if($(this).find(".childrenCategory").length > 0){
                $(this).find(".childrenCategory").addClass('show')
            }
        });
    })
</script>

<#-- 使用tmpl插件，追加数据 -->
<#noparse>
    <script id="indexShowTmpl" type="text/x-jquery-tmpl">
        <div class="item {{if index == 0}} active {{/if}}" style="background-image: url(/Book/Book/${bookImage});  background-repeat: no-repeat; background-position: top;">
            <div class="container">
                <div class="caption">
                    <div class="caption-outer">
                        <div class="col-xs-12 col-sm-12 col-md-4">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6" style="margin-top: 310px;">
                            <h1>${bookName}</h1>
                            <h2 class="animated wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms"></h2>
                            <h4>${bookAuthor} </h4>
                            <a data-scroll class="btn get-start animated fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms" href="#">borrow NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>
</#noparse>

<#-- 追加图书借阅信息 -->
<#noparse>
    <script id="borrowInfo" type="text/x-jquery-tmpl">
        <div class="cart-content">
            <div class="col-sm-4 col-md-4"><img src="/Book/Book/${book.bookImage}" alt="13"></div>
            <div class="col-sm-8 col-md-8">
                <div class="pro-text">
                    <a href="#">${book.bookName} </a>
                    <div class="close">x</div>
                    <strong>${book.bookAuthor}</strong>
                </div>
            </div>
        </div>
    </script>
</#noparse>
<!-- 弹窗 -->
<div class="modal  fade" id="userWin" tabindex="-1" role="dialog"  >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="adminWinTitle">修改密码</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="userForm">
                    <div class="form-group">
                        <label  class="col-form-label">原密码</label>
                        <input type="password"  class="form-control" id="old" name="oldPassword">
                    </div>
                    <div class="form-group" id="passwordArea">
                        <label class="col-form-label">新密码</label>
                        <input type="password" class="form-control" id="new" name="newPassword">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                <button type="button" id="saveChange" class="btn btn-primary">确认修改</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $("#changePassword").click(function () {
            $('#userWin').modal('show');
        })

        //确认修改
        $("#saveChange").click(function(){

            if($("#old").val() == ""){
                alert("请输入原密码");
                return;
            }
            if($("#new").val() == ""){
                alert("请输入新密码");
                return;
            }
            var param = {
                oldPassword : $("#old").val(),
                newPassword : $("#new").val(),
            };
            $.post("${base}/common/view/changePassword.do",param,function (returnData) {
                alert(returnData.message);
                if(returnData.flag){
                    $("#userWin").modal("hide");
                }
            })
        })
        //
    })
</script>