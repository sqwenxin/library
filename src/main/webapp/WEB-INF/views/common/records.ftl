<!DOCTYPE html>
<html lang="en">

<head>
    <#include "headerFile.ftl">
    <script type="text/javascript" src="${base}/static/skin/default/js/constant.js"></script>
</head>

<body>
<!--  Preloader  -->
<div id="preloader">
    <div id="loading"> </div>
</div>
<#include "header.ftl">
<section class="shopping-cart">
    <!-- .shopping-cart -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">主页</a></li>
                    <li class="breadcrumb-item active">登陆</li>
                </ol>
            </div>
            <div class="col-md-12">
                <!-- Accordions -->
                <div class="tabContent" id="tabContent1">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">借阅中的书籍 </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div id="checkout-step-review" class="step a-item" style="">
                                        <div class="order-review" id="checkout-review-load">
                                            <div id="checkout-review-table-wrapper">
                                                <table class="data-table linearize-table checkout-review-table" id="checkout-review-table">
                                                    <thead>
                                                    <tr>
                                                        <th rowspan="1">书籍名称</th>
                                                        <th colspan="1" class="a-center">作者</th>
                                                        <th rowspan="1" class="a-center"></th>
                                                        <th colspan="1" class="a-center"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="borrowDetail">

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="checkout-review-submit">
                                                <div class="col-xs-12 col-sm-6 col-md-6 text-left">
                                                    <ul class="pagination">
                                                        <li><a id="prePage" href="#">上一页</a></li>
                                                        <li><a id="nextPage" href="#">下一页</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<#include "foot.ftl">

</body>

</html>
<script>
    $(function(){
        $(".header-outer").remove();
        $(".all-open").removeClass("all-open")
    })
</script>

<#noparse>
    <script id="borrowPreview" type="text/x-jquery-tmpl">
        <tr>
            <td >
                ${book.bookName}
            </td>
            <td >
                <span class="price">${book.bookAuthor}</span>
            </td>
            <td>
                <span class="price">${state}</span>
            </td>
            <td>
                <a class="add-btn confirm" onclick="look(${bbProcessInstaceId})">流程图</a>
            </td>
        </tr>
    </script>
</#noparse>
<script>

    //查看流程图
    function look(pocessInstanceId){
        if(pocessInstanceId == "" || pocessInstanceId == null || pocessInstanceId == undefined){
            alert("改记录为启动流程");
            return;
        }
        window.open("${base}/workFlow/viewer.do?procInstId=" + pocessInstanceId)
    }

    $(function(){
        //异步加载预借阅信息
        var param = {
            page : ${page}
        }
        $.post("${base}/common/view/borrowInfo.do",param,function(returnData){
            if(returnData.flag == false){
                $("#bookSize").text(0);
                return;
            }
            var pageNum = returnData.page.pageNum;
            if(returnData.page.pageNum == 0){
                $("#prePage").attr("href","${base}/common/view/records.do?page=1");
                $("#nextPage").attr("href","${base}/common/view/records.do?page=1");
            }else{
                if(returnData.page.isFirstPage || returnData.page.pageNum == 0){
                    $("#prePage").attr("href","${base}/common/view/records.do?page=1");
                }else{
                    $("#prePage").attr("href","${base}/common/view/records.do?page=" + (pageNum-1));
                }
                if(returnData.page.isLastPage){
                    $("#nextPage").attr("href","${base}/common/view/records.do?page=" + pageNum);
                }else{
                    $("#nextPage").attr("href","${base}/common/view/records.do?page=" + (pageNum+1));
                }
            }

            if(returnData.total > 0){
                $("#bookSize").text(returnData.total);
                for(var i = 0 ; i < returnData.rows.length ; i++){
                    returnData.rows[i].state = borrowState.getValue(returnData.rows[i].bbProcessState);
                }
                $("#borrowPreview" ).tmpl( returnData.rows ) .appendTo( "#borrowDetail" );
            }
        })
    })
</script>
