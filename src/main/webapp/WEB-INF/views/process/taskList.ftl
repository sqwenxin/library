<@sq.html5 easyui="true"  bootstrap="false">
	<@sq.head title="用户列表">
		<style>
			#tool input{
				width:100px
			}
			.easyInput{
				margin-top:15px
			}
		</style>
	</@sq.head>
	<@sq.body>
	<div id="cc" class="easyui-layout" style="width:100%;height:100%;">
        <div data-options="region:'east',title:'',split:true" style="width:40%;height:100%">
            <table id="historyList" style="height:96%"></table>
		</div>
        <div data-options="region:'center',title:''" style="width:60%;height:98%">
            <div id="toolBar" style="background:#eee;">
                <a id="solve" class="easyui-linkbutton" iconcls="icon-edit" plain="true">处理任务</a>
                <a id="image" class="easyui-linkbutton" iconcls="icon-search" plain="true">查看流程图</a>
            </div>
            <table id="taskList" style="height:96%"></table>
        </div>
	</div>

	</@sq.body>
</@sq.html5>
<script>
	$(function() {
		//处理任务
		$("#solve").click(function(){
			var selected = $('#taskList').datagrid("getSelected");
			if(selected == null){
				notify("提示","请选择一条记录");
				return;
			}
            addTab("main",selected.taskName ,"${base}/workFlow/form.do?procInstId="  +
					selected.pocessInstanceId + "&formKey=" + selected.taskFormKey + "&taskId=" + selected.taskId);
		})

		//查看流程图
		$("#image").click(function(){
			var selected = $('#taskList').datagrid("getSelected");
			if(selected == null){
				notify("提示","请选择一条记录");
				return;
			}
            addTab("main","流程图" + selected.processVariables.workFlowName + selected.pocessInstanceId, "${base}/workFlow/viewer.do?procInstId=" + selected.pocessInstanceId);
		})

		//列表数据加载
		$('#taskList').datagrid({
			pagination:true,
			title : '任务列表',
			url : '${base}/workFlow/todoList.do',
			singleSelect : true,
			columns : [[ 
				{field : 'projectName',title : '项目名称',width : 200,
					formatter: function(value,row,index){
						return row.processVariables.workFlowName;
					}
				}, 
				{field : 'taskName',title : '当前任务',width : 150},
				{field : 'createTime',title : '创建时间',width : 100},
			]],
			//选中查看历史流程信息
            onSelect : function (index,rows) {
                var param = {
                    procInstId : rows.pocessInstanceId,
                }
                $('#historyList').datagrid("load",param);
            }
		});

        //列表数据加载
        $('#historyList').datagrid({
            pagination:true,
            title : '历史列表',
            url : '${base}/workFlow/history.do',
            queryParams :{
                			procInstId : 0
			},
            singleSelect : true,
            columns : [[
                {field:'taskName',title:'任务名称',width:100,
                    formatter: function(value,row,index){
                        if(!isBlank(row.taskName)){
                            return row.taskName;
                        }else if(!isBlank(row.variables.TaskName)){
                            return row.variables.TaskName;
                        }else{
                            return '结束';
                        }
                    }
                },
                {field:'startTime',title:'任务开始时间',width:100},
                {field:'endTime',title:'任务结束时间',width:100},
                {field:'userName',title:'任务处理人',width:100,
                    formatter: function(value,row,index){
                        if(isBlank(row.variables)){
                            return '';
                        }else{
                            if(isBlank(row.taskId)){
                                return row.variables.startUserName;
                            }
                            return row.variables.userName;
                        }
                    }
                },
                {field:'comment',title:'意见/备注',width:100,
                    formatter: function(value,row,index){
                        if(isBlank(row.taskId)){
                            return '';
                        }
                        if(isBlank(row.variables)){
                            return '';
                        }else{
                            if(isBlank(row.taskId)){
                                return '';
                            }
                            return row.variables.comment;
                        }
                    }
                },
            ]],
        });
	})
</script>