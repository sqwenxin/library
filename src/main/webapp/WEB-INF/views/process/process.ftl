<@sq.html5 easyui="true" bootstrap="false">
	<@sq.head title="用户列表">
		<style>
			#tool input{
				width:100px
			}
		</style>
	</@sq.head>
	<@sq.body>
		<div data-options="region:'center',title:''" style="height:100%">
			 <div id="toolBar" style="background:#eee;">
			 	 <a id="activity" class="easyui-linkbutton" iconcls="icon-add" plain="true">激活</a> 
			 	 <a id="suspend" class="easyui-linkbutton" iconcls="icon-edit" plain="true">挂起</a> 
			 	 <a id="delete" class="easyui-linkbutton" iconcls="icon-remove" plain="true">删除</a> 
			 	 <a id="convertToModel" class="easyui-linkbutton" iconcls="icon-save" plain="true">转化为模型</a> 
			 </div>
			 <table id="processList" style="height:95%"></table>
		</div>
	</@sq.body>
</@sq.html5>
<script>
	$(function() {		
		
		//流程列表  
		$('#processList').datagrid({
			pagination:true,
			title : '流程列表',
			url : 'processList.do',
			singleSelect : true,
			columns : [[ 
				{field:'id',title:'流程ID',width:'150'},
				{field:'key',title:'流程标识',align:'left',width:'150'},
				{field:'name',title:'流程名称',align:'left',width:'150'},
				{field:'version',title:'流程版本',align:'left',width:'150'},
				{field:'date',title:'部署时间',align:'left',width:'150'},
				{field:'xml',title:'流程XML',align:'left',width:'250',
					formatter:function(value,row,index) {
						return '<a href=\"resource/read?procDefId='+row.id+'&resType=xml\" target=\"_ablank\">'+value+'</a>';
					}
				},
				{field:'png',title:'流程图片',align:'left',width:'250',
					formatter:function(value,row,index){
						return '<a href=\"resource/read?procDefId='+row.id+'&resType=image\" target=\"_ablank\">'+value+'</a>';
					}
				}
			]],
		});
		
		//激活流程
		$("#activity").click(function(){
			var selected = $('#processList').datagrid("getSelected");
			if(selected == null){
				notify("提示","请选择一条记录");
				return;
			}
			$.post("update/active.do","procDefId=" + selected.id,function(returnData){
				notify("提示",returnData.message);
			});
		})
		
		//挂起流程
		$("#suspend").click(function(){
			var selected = $('#processList').datagrid("getSelected");
			if(selected == null){
				notify("提示","请选择一条记录");
				return;
			}
			$.post("update/suspend.do","procDefId=" + selected.id,function(returnData){
				notify("提示",returnData.message);
			});
		})
		
		//挂起流程
		$("#delete").click(function(){
			var selected = $('#processList').datagrid("getSelected");
			if(selected == null){
				notify("提示","请选择一条记录");
				return;
			}
			$.post("delete.do","deploymentId=" + selected.deploymentId,function(returnData){
				notify("提示",returnData.message);
				if(returnData.flag){
					$('#processList').datagrid("reload");
				}
			});
		})
		
		//转化为模型
		$("#convertToModel").click(function(){
			var selected = $('#processList').datagrid("getSelected");
			if(selected == null){
				notify("提示","请选择一条记录");
				return;
			}
			//查询参数
			var parameter = {
				procDefId : selected.id,
				key : selected.key,
				name : selected.name,
			}
			
			$.post("toModel.do",parameter,function(returnData){
				notify("提示",returnData.message);
				if(returnData.flag){
					$('#processList').datagrid("reload");
				}
			});
			
		})
		
	})
</script>