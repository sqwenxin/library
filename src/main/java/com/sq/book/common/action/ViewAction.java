package com.sq.book.common.action;

import com.github.pagehelper.PageInfo;
import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.base.util.StringUtil;
import com.sq.book.commonBean.ReturnMessageBean;
import com.sq.book.management.biz.IBookBiz;
import com.sq.book.management.biz.IBookBorrowBiz;
import com.sq.book.management.biz.IBookCatagoryBiz;
import com.sq.book.management.biz.IUserBiz;
import com.sq.book.management.entity.BookBorrowEntity;
import com.sq.book.management.entity.BookCatagoryEntity;
import com.sq.book.management.entity.BookEntity;
import com.sq.book.management.entity.UserEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.print.Book;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 管理控制层
 * @author 孙乾
 * @version
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/common/view")
public class ViewAction extends BaseAction {

    /**
     * 书籍业务层
     */
    @Autowired
    private IBookBiz bookBiz;

    //书籍分类业务层
    @Autowired
    private IBookCatagoryBiz bookCatagoryBiz;

    //图书借阅信息
    @Autowired
    private IBookBorrowBiz bookBorrowBiz;

    //用户个人信息
    @Autowired
    private IUserBiz userBiz;


    private List categoryList = new ArrayList();

    /**
     * 修改密码
     *
     * @param response
     * @param request
     */
    @RequestMapping("/changePassword")
    public void changePassword(String oldPassword,String newPassword,HttpServletResponse response, HttpServletRequest request) {
        UserEntity user = this.getUserBySession();
        if(user == null){
            this.outJson(response, new ReturnMessageBean(false,"请先登录"));
            return ;
        }
        if(StringUtil.MD5(oldPassword).equals(user.getUserPassword())){
            user.setUserPassword(StringUtil.MD5(newPassword));
            userBiz.updateEntity(user);
            this.outJson(response, new ReturnMessageBean(true,"修改成功"));
        }else{
            this.outJson(response, new ReturnMessageBean(false,"密码输入错误"));
            return ;
        }
    }

    /**
     * 主页
     * @return
     */
    @RequestMapping("/index")
    public String index(HttpServletResponse response, HttpServletRequest request) {
        return "/common/index";
    }

    /**
     * 书籍详情
     * @return
     */
    @RequestMapping("/detail")
    public String detail(Integer bookId,HttpServletResponse response, HttpServletRequest request) {
        BookEntity book = (BookEntity)bookBiz.getEntity(bookId);
        request.setAttribute("book",book);
        return "/common/detail";
    }

    /**
     * 分类
     * @return
     */
    @RequestMapping("/category")
    public String category(String categoryId,String bookName,Integer pageNum,HttpServletResponse response, HttpServletRequest request) {
        if(categoryId == null){
            categoryId = "";
        }
        if(bookName == null){
            bookName = "";
        }
        if(pageNum == null){
            pageNum = 1;
        }
        request.setAttribute("categoryId",categoryId);
        request.setAttribute("bookName",bookName);
        request.setAttribute("pageNum",pageNum);
        return "/common/category";
    }



    /**
     * 详情
     * @return
     */
    @RequestMapping("/login")
    public String login(String bookName,HttpServletResponse response, HttpServletRequest request) {
        return "/common/login";
    }

    /**
     * 借阅管理
     * @return
     */
    @RequestMapping("/reading")
    public String reading(String bookName,Integer page,HttpServletResponse response, HttpServletRequest request) {
        if(page == null){
            page = 1;
        }
        request.setAttribute("page",page);
        return "/common/reading";
    }

    /**
     * 借阅历史记录
     * @return
     */
    @RequestMapping("/records")
    public String records(String bookName,Integer page,HttpServletResponse response, HttpServletRequest request) {
        if(page == null){
            page = 1;
        }
        request.setAttribute("page",page);
        return "/common/records";
    }

    /**
     * 根据实体查询
     *
     * @param response
     * @param request
     */
    @RequestMapping("/queryCategoryParent")
    public void queryCategoryParent(HttpServletResponse response, HttpServletRequest request) {
        BookCatagoryEntity bookCatagory = new BookCatagoryEntity();
        bookCatagory.setSqlWhere(" and ( bc_parent_id is null or bc_parent_id=0 ) ");
        List<BookCatagoryEntity> catagoryList = bookCatagoryBiz.query(bookCatagory);
        List list = new ArrayList();
        for(BookCatagoryEntity _bookCatagory : catagoryList){
            //每次都需要刷新数据
            categoryList = new ArrayList();
            _bookCatagory.setChildren(this.queryChildren(_bookCatagory.getBcId() + ""));
            list.add(_bookCatagory);
        }
        this.outJson(response, list, "yyyy-MM-dd");
    }

    /**
     * 分页查询书籍数据
     * @param book
     * @param response
     * @param request
     */
    @RequestMapping("/queryBook")
    public void queryBook(BookEntity book,String categoryId,HttpServletResponse response, HttpServletRequest request){
        categoryList = new ArrayList();
        if(StringUtils.isNotBlank(categoryId)){
            List<BookCatagoryEntity> catagoryList = this.queryChildren(categoryId);
            String ids = categoryId;
            for(BookCatagoryEntity _bookCatagory:catagoryList){
                if(StringUtils.isBlank(ids)){
                    ids += _bookCatagory.getBcId();
                }else{
                    ids += "," + _bookCatagory.getBcId();
                }
            }
            if(book == null){
                book = new BookEntity();
            }
            book.setQuerySql("and book_bc_id in ( " + ids + ")");
        }
        if(StringUtils.isBlank(book.getBookName())){
            book.setBookName(null);
        }
        PageUtil.startPage();
        List bookList = bookBiz.query(book);
        Map map = new HashMap();


        PageInfo page =  PageUtil.endPage(bookList);
        map.put("rows",bookList);
        map.put("total",page.getTotal());
        page.setList(null);
        map.put("page",page);

        this.outJson(response, map, "yyyy-MM-dd");
    }

    /**
     * 查询个人借阅信息
     */
    @RequestMapping("/queryBorrowInfo")
    public void queryBorrowInfo(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request){
        PageUtil.startPage();
        if(bookBorrow == null){
            bookBorrow = new BookBorrowEntity();
        }
        UserEntity user = this.getUserBySession();
        if(user == null){
            this.outJson(response, new ReturnMessageBean(false,"请先登录"));
            return ;
        }
        bookBorrow.setBbUserId(user.getUserId());
        List borrowList = bookBorrowBiz.query(bookBorrow);

        Map map = new HashMap();
        PageInfo page =  PageUtil.endPage(borrowList);
        map.put("rows",borrowList);
        map.put("total",page.getTotal());
        page.setList(null);
        map.put("page",page);
        this.outJson(response, map, "yyyy-MM-dd");
    }


    /**
     * 查看当前借阅中信息
     * @param bookBorrow
     * @param response
     * @param request
     */
    @RequestMapping("/borrowInfo")
    public void borrowInfo(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request){
        PageUtil.startPage();
        if(bookBorrow == null){
            bookBorrow = new BookBorrowEntity();
        }
        UserEntity user = this.getUserBySession();
        if(user == null){
            this.outJson(response, new ReturnMessageBean(false,"请先登录"));
            return ;
        }
        bookBorrow.setBbUserId(user.getUserId());
        bookBorrow.setSqlWhere(" and bb_process_state >=1 and bb_process_state <= 12");
        List borrowList = bookBorrowBiz.query(bookBorrow);
        Map map = new HashMap();
        PageInfo page =  PageUtil.endPage(borrowList);
        map.put("rows",borrowList);
        map.put("total",page.getTotal());
        page.setList(null);
        map.put("page",page);
        this.outJson(response, map, "yyyy-MM-dd");
    }


    /**
     * 获取借阅信息
     * @param bookBorrow
     * @param response
     * @param request
     */
    @RequestMapping("/getBorrowInfo")
    public void  getBorrowInfo(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request){
        if(bookBorrow == null || bookBorrow.getBbId() == null){
            this.outJson(response, new ReturnMessageBean(false,"请选择需要借阅的书籍"));
            return ;
        }
        UserEntity user = this.getUserBySession();
        if(user == null){
            this.outJson(response, new ReturnMessageBean(false,"请先登录"));
            return ;
        }
        this.outJson(response,new ReturnMessageBean(true,"查询成功",bookBorrowBiz.getEntity(bookBorrow.getBbId())));
    }

    /**
     * 删除准备借阅的书籍
     * @param bbIduy
     * @param response
     * @param request
     */
    @RequestMapping("/cancelBorrow")
    public void cancelBorrow(Integer bbId,HttpServletResponse response, HttpServletRequest request){
        bookBorrowBiz.deleteEntity(bbId);
        this.outJson(response, new ReturnMessageBean(true,"删除成功"));
    }

    /**
     * 借阅书籍
     * @param bookId
     * @param response
     * @param request
     */
    @RequestMapping("/borrow")
    public void borrow(Integer bookId,HttpServletResponse response, HttpServletRequest request){
        UserEntity user = this.getUserBySession();
        if(user == null){
            this.outJson(response, new ReturnMessageBean(false,"请先登录"));
            return ;
        }

        if(bookId == null || bookId == 0){
            this.outJson(response, new ReturnMessageBean(false,""));
            return ;
        }

        BookEntity book = (BookEntity)bookBiz.getEntity(bookId);
        BookEntity _book = new BookEntity();
        _book.setBookName(book.getBookName());
        _book.setBookAuthor(book.getBookAuthor());
        _book.setBookManageState(0);
        List list = bookBiz.query(_book);
        if(list.size() > 0){
            _book = (BookEntity)list.get(0);
            BookBorrowEntity bookBorrow = new BookBorrowEntity();
            bookBorrow.setBbUserId(user.getUserId());
            bookBorrow.setBbBookName(_book.getBookName());
            bookBorrow.setBbProcessState(0);
            List borrowList = bookBorrowBiz.query(bookBorrow);
            if(borrowList.size() > 0){
                this.outJson(response, new ReturnMessageBean(true,"借阅成功"));
                return;
            }
            bookBorrow.setBbBookId(_book.getBookId());
            bookBorrowBiz.saveEntity(bookBorrow);
            this.outJson(response, new ReturnMessageBean(true,"借阅成功"));
        }else{
            this.outJson(response, new ReturnMessageBean(false,"书籍均已借出，暂无存货"));
        }

    }

    /**
     * 根据扶ID查询所有的子分类集合
     *
     * @param response
     * @param request
     */
    @RequestMapping("/queryCategoryChildren")
    public void queryCategoryChildren(Integer parentId,HttpServletResponse response, HttpServletRequest request) {
        if(parentId == null){
            return;
        }
        this.outJson(response, this.queryChildren(parentId + ""), "yyyy-MM-dd");
    }

    /**
     * 查询所有的子分类
     * @param ids
     * @return
     */
    private synchronized List queryChildren(String ids){
        if(StringUtils.isBlank(ids)){
            return categoryList;
        }
        String queryIds = "";
        BookCatagoryEntity bookCatagory = new BookCatagoryEntity();
        bookCatagory.setSqlWhere(" and bc_parent_id in (" + ids + ")");
        List<BookCatagoryEntity> list = bookCatagoryBiz.query(bookCatagory);
        categoryList.addAll(list);
        for(BookCatagoryEntity category : list){
            if(StringUtils.isBlank(queryIds)){
                queryIds = category.getBcId() + "";
            }else{
                queryIds += "," + category.getBcId() + "";
            }
        }
        return this.queryChildren(queryIds);
    }
}
