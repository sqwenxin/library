package com.sq.book.commonBean;

public class TaskBean {
	
	/**
	 * 任务名称
	 */
	private String taskName;
	
	/**
	 * 审核意见
	 */
	private String taskAdvice;
	
	/**
	 * 审核是否通过
	 */
	private Boolean isAgree;
	
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskAdvice() {
		return taskAdvice;
	}

	public void setTaskAdvice(String taskAdvice) {
		this.taskAdvice = taskAdvice;
	}

	public Boolean getIsAgree() {
		return isAgree;
	}

	public void setIsAgree(Boolean isAgree) {
		this.isAgree = isAgree;
	}
}
