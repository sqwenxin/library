package com.sq.book.commonBean;
/**
 * 返回消息
 * @author sq
 *
 */
public class ReturnMessageBean {
	/**
	 * 操作成功还是失败
	 */
	private boolean flag;
	/**
	 * 返回的提示信�?
	 */
	private String message;
	/**
	 * 返回的其他对�?
	 */
	private Object obj;
	
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * ����ֵ
	 * @param flag
	 * @param message
	 * @param obj
	 */
	public ReturnMessageBean(boolean flag,String message,Object obj){
		this.flag = flag;
		this.message = message;
		this.obj = obj;
	} 
	
	/**
	 * 
	 * @param flag
	 * @param message
	 */
	public ReturnMessageBean(boolean flag,String message){
		this.flag = flag;
		this.message = message;
	} 
}
