package com.sq.book.commonUtils;

import java.util.List;
/**
 * 把数据转为easy UI的工�?
 * @author sq
 *
 */
public class EasyUIJson {
	/**
	 * 行数�?
	 */
	private List rows;
	/**
	 * 总数
	 */
	private int total;
	
	public EasyUIJson(){
		
	}
	public EasyUIJson(List rows,int total){
		this.rows = rows;
		this.total = total;		
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List getRows() {
		return rows;
	}
	public void setRows(List rows) {
		this.rows = rows;
	}
	
}
