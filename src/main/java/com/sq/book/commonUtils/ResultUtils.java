package com.sq.book.commonUtils;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;



import com.alibaba.fastjson.JSONArray;
/**
 * 杈撳嚭宸ュ叿绫�
 * @author sq
 *
 */
public class ResultUtils {
	/**
	 * json鏍煎紡杈撳嚭鏁版�?
	 * @param response
	 * @param data
	 * @throws IOException
	 */
	 public static void outJson(HttpServletResponse response, Object data) {
		 try{
	 			String dateFormat = "yyyy-MM-dd";
	 			String result= JSONArray.toJSONStringWithDateFormat(data, dateFormat);		
				response.setContentType("text/json; charset=utf-8");
				response.setHeader("Cache-Control", "no-cache"); //鍙栨秷娴忚鍣ㄧ紦�?��
				PrintWriter out = response.getWriter();
				out.print(result);
				out.flush();
				out.close();
				response.getWriter().close();
	 		}catch (Exception e) {
				e.printStackTrace();
			}				
	 }
	
	 /**
	  * 杈撳嚭TRUE or false
	  * @param response
	  * @param bool
	  * @throws IOException
	  */
	 public static void outBoolean(HttpServletResponse response, boolean bool) throws IOException{
		try{
			response.setContentType("text/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-cache"); //鍙栨秷娴忚鍣ㄧ紦�?��
			PrintWriter out = response.getWriter();
			out.print(bool);
			out.flush();
			out.close();
			response.getWriter().close();
		}catch (Exception e) {
			// TODO: handle exception
		}		
	 } 
}
