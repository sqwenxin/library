package com.sq.book.management.bean;

import java.util.Date;

public class BookChartBean {
    /**
     * 图书ID
     */
    private Integer bookId;

    /**
     * 图书分类名称
     */
    private String bcName;

    private Integer bookNumber;

    /**
     * 图书编号，唯一
     */
    private String bookCode;

    /**
     * 图书名称
     */
    private String bookName;

    /**
     * 作者
     */
    private String bookAuthor;

    /**
     * 关联图书分类编号
     */
    private Integer bookBcId;

    /**
     * 图书描述
     */
    private String bookDescribe;

    /**
     * 图书图片
     */
    private String bookImage;

    /**
     * 图书管理状态:0:未借出，1：已借出
     */
    private Integer bookManageState;

    /**
     * 图书当前状态：0：正常，1:损坏，2：丢失
     */
    private Integer bookState;

    /**
     * 图书损坏描述
     */
    private String bookDamageDescribe;

    /**
     * 图书创建人
     */
    private Integer bookCreateBy;

    /**
     * 图书创建时间
     */
    private Date bookCreateDate;

    /**
     * 图书更新人
     */
    private Integer bookUpdateBy;

    /**
     * 图书更新时间
     */
    private Date bookUpdateDate;

    /**
     * 图书是否删除：0：未删除，1：已删除
     */
    private Integer bookDel;

    private String querySql;

    public BookChartBean(){

    }

    /**
     * 设置图书ID
     */
    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    /**
     * 获取图书ID
     */
    public Integer getBookId() {
        return this.bookId;
    }

    /**
     * 设置图书编号，唯一
     */
    public void setBookCode(String bookCode) {
        this.bookCode = bookCode;
    }

    /**
     * 获取图书编号，唯一
     */
    public String getBookCode() {
        return this.bookCode;
    }

    /**
     * 设置图书名称
     */
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    /**
     * 获取图书名称
     */
    public String getBookName() {
        return this.bookName;
    }

    /**
     * 设置作者
     */
    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    /**
     * 获取作者
     */
    public String getBookAuthor() {
        return this.bookAuthor;
    }

    /**
     * 设置关联图书分类编号
     */
    public void setBookBcId(Integer bookBcId) {
        this.bookBcId = bookBcId;
    }

    /**
     * 获取关联图书分类编号
     */
    public Integer getBookBcId() {
        return this.bookBcId;
    }

    /**
     * 设置图书描述
     */
    public void setBookDescribe(String bookDescribe) {
        this.bookDescribe = bookDescribe;
    }

    /**
     * 获取图书描述
     */
    public String getBookDescribe() {
        return this.bookDescribe;
    }

    /**
     * 设置图书图片
     */
    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    /**
     * 获取图书图片
     */
    public String getBookImage() {
        return this.bookImage;
    }

    /**
     * 设置图书管理状态:0:未借出，1：已借出
     */
    public void setBookManageState(Integer bookManageState) {
        this.bookManageState = bookManageState;
    }

    /**
     * 获取图书管理状态:0:未借出，1：已借出
     */
    public Integer getBookManageState() {
        return this.bookManageState;
    }

    /**
     * 设置图书当前状态：0：正常，1:损坏，2：丢失
     */
    public void setBookState(Integer bookState) {
        this.bookState = bookState;
    }

    /**
     * 获取图书当前状态：0：正常，1:损坏，2：丢失
     */
    public Integer getBookState() {
        return this.bookState;
    }

    /**
     * 设置图书损坏描述
     */
    public void setBookDamageDescribe(String bookDamageDescribe) {
        this.bookDamageDescribe = bookDamageDescribe;
    }

    /**
     * 获取图书损坏描述
     */
    public String getBookDamageDescribe() {
        return this.bookDamageDescribe;
    }

    /**
     * 设置图书创建人
     */
    public void setBookCreateBy(Integer bookCreateBy) {
        this.bookCreateBy = bookCreateBy;
    }

    /**
     * 获取图书创建人
     */
    public Integer getBookCreateBy() {
        return this.bookCreateBy;
    }

    /**
     * 设置图书创建时间
     */
    public void setBookCreateDate(Date bookCreateDate) {
        this.bookCreateDate = bookCreateDate;
    }

    /**
     * 获取图书创建时间
     */
    public Date getBookCreateDate() {
        return this.bookCreateDate;
    }

    /**
     * 设置图书更新人
     */
    public void setBookUpdateBy(Integer bookUpdateBy) {
        this.bookUpdateBy = bookUpdateBy;
    }

    /**
     * 获取图书更新人
     */
    public Integer getBookUpdateBy() {
        return this.bookUpdateBy;
    }

    /**
     * 设置图书更新时间
     */
    public void setBookUpdateDate(Date bookUpdateDate) {
        this.bookUpdateDate = bookUpdateDate;
    }

    /**
     * 获取图书更新时间
     */
    public Date getBookUpdateDate() {
        return this.bookUpdateDate;
    }

    /**
     * 设置图书是否删除：0：未删除，1：已删除
     */
    public void setBookDel(Integer bookDel) {
        this.bookDel = bookDel;
    }

    /**
     * 获取图书是否删除：0：未删除，1：已删除
     */
    public Integer getBookDel() {
        return this.bookDel;
    }


    public String getQuerySql() {
        return querySql;
    }

    public void setQuerySql(String querySql) {
        this.querySql = querySql;
    }

    public String getBcName() {
        return bcName;
    }

    public void setBcName(String bcName) {
        this.bcName = bcName;
    }

    public Integer getBookNumber() {
        return bookNumber;
    }

    public void setBookNumber(Integer bookNumber) {
        this.bookNumber = bookNumber;
    }
}
