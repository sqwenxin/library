package com.sq.book.management.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.management.biz.IRoleModelBiz;
import com.sq.book.management.entity.RoleModelEntity;
	
/**
 * 管理控制层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/manager/roleModel")

public class RoleModelAction extends BaseAction {
	
	/**
	 * 注入业务层
	 */	
	@Autowired
	private IRoleModelBiz roleModelBiz;
	
	/**
	 * 主页
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletResponse response, HttpServletRequest request) {
		return "/roleModel/index";
	}
	
	/**
	 * 表单
	 * @return
	 */
	@RequestMapping("/form")
	public String form(RoleModelEntity roleModel, HttpServletResponse response, HttpServletRequest request) {
		if (roleModel.getRmId() != null && roleModel.getRmId() > 0) {
			roleModel = (RoleModelEntity) roleModelBiz.getEntity(roleModel.getRmId());
		}
		request.setAttribute("roleModel", roleModel);
		return "/roleModel/form";
	}
	
	/**
	 * 查询列表
	 * @param roleModel 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/list")
	public void list(RoleModelEntity roleModel, HttpServletResponse response, HttpServletRequest request) {
		PageUtil.startPage();
		List roleModelList = roleModelBiz.query(roleModel);
		this.outJson(response, new EasyUIBean(roleModelList, (int)PageUtil.endPage(roleModelList).getTotal()), "yyyy-MM-dd");
	}
	
	/**
	 * 根据实体查询
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/query")
	public void queryAll(RoleModelEntity roleModel, HttpServletResponse response, HttpServletRequest request) {
		List roleModelList = roleModelBiz.query(roleModel);
		this.outJson(response, new EasyUIBean(roleModelList, roleModelList.size()), "yyyy-MM-dd");
	}
	
	/**
	 * 获取实体
	 * @param roleModel 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/get")
	public void get(RoleModelEntity roleModel, HttpServletResponse response, HttpServletRequest request){
		if(roleModel.getRmId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(roleModel.getRmId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		RoleModelEntity _roleModel = (RoleModelEntity) roleModelBiz.getEntity(roleModel.getRmId());
		this.outJson(response, new ResultBean(true, _roleModel));
	}
	
	/**
	 * 保存实体
	 * @param roleModel 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/save")
	public void save(RoleModelEntity roleModel, HttpServletResponse response, HttpServletRequest request) {
		roleModelBiz.saveEntity(roleModel);
		this.outJson(response, new ResultBean(true, roleModel));
	}
	
	/** 
	 * 更新信息
	 * @param roleModel 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/update")
	public void update(RoleModelEntity roleModel,Integer[] modelIds,Integer roleId, HttpServletResponse response, HttpServletRequest request) {
		roleModel.setRmRoleId(roleId);
		roleModelBiz.deleteEntity(roleModel);
		for(Integer modeld : modelIds){
			RoleModelEntity _roleModel = new RoleModelEntity();
			_roleModel.setRmModelId(modeld);
			_roleModel.setRmRoleId(roleId);
			roleModelBiz.saveEntity(_roleModel);
		}
		this.outJson(response, new ResultBean(true, "分配成功"));
	}

	/**
	 * 删除
	 * @param response
	 * @param request
	 */
	@RequestMapping("/delete")
	public void delete(RoleModelEntity roleModel, HttpServletResponse response, HttpServletRequest request) {
		if(roleModel.getRmId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(roleModel.getRmId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		roleModelBiz.deleteEntity(roleModel.getRmId());		
		this.outJson(response, new ResultBean(true));
	}
	
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @param response
	 * @param request
	 */
	@RequestMapping("/deleteBatch")
	public void deleteBatch(String ids, HttpServletResponse response, HttpServletRequest request) {
		if (StringUtils.isEmpty(ids)) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		String[] idsArray = ids.split(",");
		roleModelBiz.deleteEntity(idsArray);
		this.outJson(response, new ResultBean(true));
	}
	
}