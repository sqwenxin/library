package com.sq.book.management.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.base.util.StringUtil;
import com.sq.book.management.biz.IUserBiz;
import com.sq.book.management.entity.UserEntity;
	
/**
 * 管理控制层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/manager/user")

public class UserAction extends BaseAction {
	
	/**
	 * 注入业务层
	 */	
	@Autowired
	private IUserBiz userBiz;
	
	/**
	 * 主页
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletResponse response, HttpServletRequest request) {
		return "/management/user/index";
	}
	
	/**
	 * 表单
	 * @return
	 */
	@RequestMapping("/form")
	public String form(UserEntity user, HttpServletResponse response, HttpServletRequest request) {
		if (user.getUserId() != null && user.getUserId() > 0) {
			user = (UserEntity) userBiz.getEntity(user.getUserId());
		}
		request.setAttribute("user", user);
		return "/user/form";
	}
	
	/**
	 * 查询列表
	 * @param user 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/list")
	public void list(UserEntity user, HttpServletResponse response, HttpServletRequest request) {
		PageUtil.startPage();
		List userList = userBiz.query(user);
		this.outJson(response, new EasyUIBean(userList, (int)PageUtil.endPage(userList).getTotal()), "yyyy-MM-dd");
	}
	
	/**
	 * 根据实体查询
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/query")
	public void queryAll(UserEntity user, HttpServletResponse response, HttpServletRequest request) {
		List userList = userBiz.query(user);
		this.outJson(response, new EasyUIBean(userList, userList.size()), "yyyy-MM-dd");
	}
	
	/**
	 * 获取实体
	 * @param user 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/get")
	public void get(UserEntity user, HttpServletResponse response, HttpServletRequest request){
		if(user.getUserId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(user.getUserId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		UserEntity _user = (UserEntity) userBiz.getEntity(user.getUserId());
		this.outJson(response, new ResultBean(true, _user));
	}
	
	/**
	 * 保存实体
	 * @param user 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/save")
	public void save(UserEntity user, HttpServletResponse response, HttpServletRequest request) {
		//密码MD5加密
		user.setUserPassword(StringUtil.MD5(user.getUserPassword()));
		userBiz.saveEntity(user);
		this.outJson(response, new ResultBean(true, user));
	}
	
	/** 
	 * 更新信息
	 * @param user 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/update")
	public void update(UserEntity user, HttpServletResponse response, HttpServletRequest request) {
		if(user.getUserId() == null && user.getIdArray() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(user.getUserPassword() != null){
			user.setUserPassword(StringUtil.MD5(user.getUserPassword()));
		}
		userBiz.updateEntity(user);
		this.outJson(response, new ResultBean(true, user));
	}

	/**
	 * 删除
	 * @param response
	 * @param request
	 */
	@RequestMapping("/delete")
	public void delete(UserEntity user, HttpServletResponse response, HttpServletRequest request) {
		if(user.getUserId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(user.getUserId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		userBiz.deleteEntity(user.getUserId());		
		this.outJson(response, new ResultBean(true));
	}
	
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @param response
	 * @param request
	 */
	@RequestMapping("/deleteBatch")
	public void deleteBatch(String ids, HttpServletResponse response, HttpServletRequest request) {
		if (StringUtils.isEmpty(ids)) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		String[] idsArray = ids.split(",");
		userBiz.deleteEntity(idsArray);
		this.outJson(response, new ResultBean(true));
	}
	
}