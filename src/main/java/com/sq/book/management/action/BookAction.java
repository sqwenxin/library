package com.sq.book.management.action;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.base.util.UpLoadUtil;
import com.sq.book.management.biz.IBookBiz;
import com.sq.book.management.entity.BookEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 管理控制层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/manager/book")

public class BookAction extends BaseAction {
	
	/**
	 * 注入业务层
	 */	
	@Autowired
	private IBookBiz bookBiz;
	
	/**
	 * 主页
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletResponse response, HttpServletRequest request) {
		return "/management/book/index";
	}
	
	/**
	 * 表单
	 * @return
	 */
	@RequestMapping("/form")
	public String form(BookEntity book, HttpServletResponse response, HttpServletRequest request) {
		if (book.getBookId() != null && book.getBookId() > 0) {
			book = (BookEntity) bookBiz.getEntity(book.getBookId());
		}
		request.setAttribute("book", book);
		return "/management/book/form";
	}
	
	/**
	 * 查询列表
	 * @param book 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/list")
	public void list(BookEntity book,String bookCategoryIds,HttpServletResponse response, HttpServletRequest request) {
		if(StringUtils.isNotBlank(bookCategoryIds)){
			book.setQuerySql("and book_bc_id in ( " + bookCategoryIds + ")");
		}
		PageUtil.startPage();
		List bookList = bookBiz.query(book);
		this.outJson(response, new EasyUIBean(bookList, (int)PageUtil.endPage(bookList).getTotal()), "yyyy-MM-dd");
	}
	
	/**
	 * 根据实体查询
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/query")
	public void queryAll(BookEntity book, HttpServletResponse response, HttpServletRequest request) {
		List bookList = bookBiz.query(book);
		this.outJson(response, new EasyUIBean(bookList, bookList.size()), "yyyy-MM-dd");
	}
	
	/**
	 * 获取实体
	 * @param book 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/get")
	public void get(BookEntity book, HttpServletResponse response, HttpServletRequest request){
		if(book.getBookId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(book.getBookId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		BookEntity _book = (BookEntity) bookBiz.getEntity(book.getBookId());
		this.outJson(response, new ResultBean(true, _book));
	}
	
	/**
	 * 保存实体
	 * @param book 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/save")
	public void save(BookEntity book, MultipartFile bookImagePath,  HttpServletResponse response, HttpServletRequest request) {
        String path = request.getContextPath();
        String basePath = request.getSession().getServletContext().getRealPath("/") + path + "/";
        path = UpLoadUtil.fileUpload(bookImagePath, basePath);// 上传文件
        book.setBookImage(path);
        book.setBookCode(this.bookCode());
	    bookBiz.saveEntity(book);
		this.outJson(response, new ResultBean(true, book));
	}
	
	/** 
	 * 更新信息
	 * @param book 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/update")
	public void update(BookEntity book,MultipartFile bookImagePath, HttpServletResponse response, HttpServletRequest request) {
		if(book.getBookId() == null && book.getIdArray() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(bookImagePath != null){
            String path = request.getContextPath();
            String basePath = request.getSession().getServletContext().getRealPath("/") + path + "/";
            path = UpLoadUtil.fileUpload(bookImagePath, basePath);// 上传文件
            book.setBookImage(path);
        }
		bookBiz.updateEntity(book);
		this.outJson(response, new ResultBean(true, book));
	}

	/**
	 * 删除
	 * @param response
	 * @param request
	 */
	@RequestMapping("/delete")
	public void delete(BookEntity book, HttpServletResponse response, HttpServletRequest request) {
		if(book.getBookId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(book.getBookId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		bookBiz.deleteEntity(book.getBookId());		
		this.outJson(response, new ResultBean(true,"删除成功"));
	}
	
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @param response
	 * @param request
	 */
	@RequestMapping("/deleteBatch")
	public void deleteBatch(String ids, HttpServletResponse response, HttpServletRequest request) {
		if (StringUtils.isEmpty(ids)) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		String[] idsArray = ids.split(",");
		bookBiz.deleteEntity(idsArray);
		this.outJson(response, new ResultBean(true));
	}

    /**
     * 根据日期生成编号
     * @return
     */
    private synchronized String bookCode(){
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return "SJ"+format.format(new Date());
    }
}