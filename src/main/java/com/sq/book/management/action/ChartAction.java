package com.sq.book.management.action;

import com.sq.book.base.action.BaseAction;
import com.sq.book.management.biz.IChartBiz;
import com.sq.book.management.entity.BookEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 报表
 * 管理控制层
 * @author 孙乾
 * @version
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/manager/chart")
public class ChartAction extends BaseAction {

    /**
     * 注入业务层
     */
    @Autowired
    private IChartBiz chartBiz;

    /**
     * 报表页面
     * @param response
     * @param request
     * @return
     */
    @RequestMapping("/index")
    public String index(HttpServletResponse response, HttpServletRequest request) {
        return "/management/chart/index";
    }

    /**
     * 查询图书分类及数量
     * @param book
     * @param bookCategoryIds
     * @param response
     * @param request
     */
    @RequestMapping("/queryByCategory")
    public void queryByCategory(BookEntity book, String bookCategoryIds, HttpServletResponse response, HttpServletRequest request) {
        List list = chartBiz.queryByCategory();
        this.outJson(response, list);
    }

    /**
     * 查询最近一年借阅情况
     * @param book
     * @param bookCategoryIds
     * @param response
     * @param request
     */
    @RequestMapping("/queryByMonth")
    public void queryByMonth(BookEntity book, String bookCategoryIds, HttpServletResponse response, HttpServletRequest request) {
        List list = chartBiz.createSQLQuery("select count(*) as size,year(bb_create_date) as year, month(bb_create_date) as month from book_borrow where bb_del=0 GROUP BY EXTRACT(YEAR_MONTH from bb_create_date) ORDER BY bb_create_date desc LIMIT 0,12");
        this.outJson(response, list);
    }

    /**
     * 借阅数量最多前10本书
     * @param book
     * @param bookCategoryIds
     * @param response
     * @param request
     */
    @RequestMapping("/queryByOrder")
    public void queryByOrder(BookEntity book, String bookCategoryIds, HttpServletResponse response, HttpServletRequest request) {
        List list = chartBiz.createSQLQuery("SELECT count(*) as size , bb_book_name as bookName from book_borrow where bb_del=0 group by bb_book_name order by size desc limit 0,10");
        this.outJson(response, list);
    }

    /**
     * 书籍状态分析
     * @param book
     * @param bookCategoryIds
     * @param response
     * @param request
     */
    @RequestMapping("/queryByManagerState")
    public void queryByManagerState(BookEntity book, String bookCategoryIds, HttpServletResponse response, HttpServletRequest request) {
        List<Integer> state = new ArrayList<Integer>();
        state.add(0);
        state.add(2);
        state.add(3);
        List<Integer> managerState = new ArrayList<Integer>();
        managerState.add(0);
        managerState.add(1);

        List borrowInfo = new ArrayList<>();
        List curInfo = new ArrayList<>();
        List bookState =  new ArrayList();
        bookState.add("正常");
        bookState.add("损坏");
        bookState.add("丢失");
        for(Integer _managerState : managerState){
            for(Integer _state : state){
                List<HashMap> map =  chartBiz.createSQLQuery("select count(*) as size from book where book_state=" + _state + " and book_manage_state=" + _managerState + " and book_del=0");
                if(_managerState == 0){
                    curInfo.add(map.get(0).get("size"));
                }else{
                    borrowInfo.add(map.get(0).get("size"));
                }

            }
        }

        Map out = new HashMap();
        out.put("bookState",bookState);
        out.put("borrowInfo",borrowInfo);
        out.put("curInfo",curInfo);
        this.outJson(response, out);
    }
}
