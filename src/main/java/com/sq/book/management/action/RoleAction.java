package com.sq.book.management.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.management.biz.IRoleBiz;
import com.sq.book.management.entity.RoleEntity;
	
/**
 * 管理控制层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/manager/role")

public class RoleAction extends BaseAction {
	
	/**
	 * 注入业务层
	 */	
	@Autowired
	private IRoleBiz roleBiz;
	
	/**
	 * 主页
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletResponse response, HttpServletRequest request) {
		return "/management/role/index";
	}
	
	/**
	 * 表单
	 * @return
	 */
	@RequestMapping("/form")
	public String form(RoleEntity role, HttpServletResponse response, HttpServletRequest request) {
		if (role.getRoleId() != null && role.getRoleId() > 0) {
			role = (RoleEntity) roleBiz.getEntity(role.getRoleId());
		}
		request.setAttribute("role", role);
		return "/role/form";
	}
	
	/**
	 * 查询列表
	 * @param role 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/list")
	public void list(RoleEntity role, HttpServletResponse response, HttpServletRequest request) {
		PageUtil.startPage();
		List roleList = roleBiz.query(role);
		this.outJson(response, new EasyUIBean(roleList, (int)PageUtil.endPage(roleList).getTotal()), "yyyy-MM-dd");
	}
	
	/**
	 * 根据实体查询
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/query")
	public void queryAll(RoleEntity role, HttpServletResponse response, HttpServletRequest request) {
		List roleList = roleBiz.query(role);
		this.outJson(response, new EasyUIBean(roleList, roleList.size()), "yyyy-MM-dd");
	}
	
	/**
	 * easy UI下拉框需要的数据
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/queryAll")
	public void queryBycommbox(RoleEntity role, HttpServletResponse response, HttpServletRequest request) {
		List roleList = roleBiz.query(role);
		this.outJson(response, roleList);
	}
	
	
	/**
	 * 获取实体
	 * @param role 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/get")
	public void get(RoleEntity role, HttpServletResponse response, HttpServletRequest request){
		if(role.getRoleId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(role.getRoleId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		RoleEntity _role = (RoleEntity) roleBiz.getEntity(role.getRoleId());
		this.outJson(response, new ResultBean(true, _role));
	}
	
	/**
	 * 保存实体
	 * @param role 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/save")
	public void save(RoleEntity role, HttpServletResponse response, HttpServletRequest request) {
		roleBiz.saveEntity(role);
		this.outJson(response, new ResultBean(true, "添加成功"));
	}
	
	/** 
	 * 更新信息
	 * @param role 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/update")
	public void update(RoleEntity role, HttpServletResponse response, HttpServletRequest request) {
		if(role.getRoleId() == null && role.getIdArray() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		roleBiz.updateEntity(role);
		this.outJson(response, new ResultBean(true, "更新成功"));
	}

	/**
	 * 删除
	 * @param response
	 * @param request
	 */
	@RequestMapping("/delete")
	public void delete(RoleEntity role, HttpServletResponse response, HttpServletRequest request) {
		if(role.getRoleId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(role.getRoleId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		roleBiz.deleteEntity(role.getRoleId());		
		this.outJson(response, new ResultBean(true));
	}
	
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @param response
	 * @param request
	 */
	@RequestMapping("/deleteBatch")
	public void deleteBatch(String ids, HttpServletResponse response, HttpServletRequest request) {
		if (StringUtils.isEmpty(ids)) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		String[] idsArray = ids.split(",");
		roleBiz.deleteEntity(idsArray);
		this.outJson(response, new ResultBean(true));
	}
	
}