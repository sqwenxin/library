package com.sq.book.management.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.management.biz.IModelBiz;
import com.sq.book.management.entity.ModelEntity;
	
/**
 * 管理控制层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/manager/model")

public class ModelAction extends BaseAction {
	
	/**
	 * 注入业务层
	 */	
	@Autowired
	private IModelBiz modelBiz;
	
	/**
	 * 主页
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletResponse response, HttpServletRequest request) {
		return "/model/index";
	}
	
	/**
	 * 表单
	 * @return
	 */
	@RequestMapping("/form")
	public String form(ModelEntity model, HttpServletResponse response, HttpServletRequest request) {
		if (model.getModelId() != null && model.getModelId() > 0) {
			model = (ModelEntity) modelBiz.getEntity(model.getModelId());
		}
		request.setAttribute("model", model);
		return "/model/form";
	}
	
	/**
	 * 查询列表
	 * @param model 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/list")
	public void list(ModelEntity model, HttpServletResponse response, HttpServletRequest request) {
		PageUtil.startPage();
		List modelList = modelBiz.query(model);
		this.outJson(response, new EasyUIBean(modelList, (int)PageUtil.endPage(modelList).getTotal()), "yyyy-MM-dd");
	}
	
	/**
	 * 根据实体查询
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/query")
	public void queryAll(ModelEntity model, HttpServletResponse response, HttpServletRequest request) {
		List modelList = modelBiz.query(model);
		this.outJson(response, new EasyUIBean(modelList, modelList.size()), "yyyy-MM-dd");
	}
	
	/**
	 * 获取实体
	 * @param model 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/get")
	public void get(ModelEntity model, HttpServletResponse response, HttpServletRequest request){
		if(model.getModelId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(model.getModelId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		ModelEntity _model = (ModelEntity) modelBiz.getEntity(model.getModelId());
		this.outJson(response, new ResultBean(true, _model));
	}
	
	/**
	 * 保存实体
	 * @param model 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/save")
	public void save(ModelEntity model, HttpServletResponse response, HttpServletRequest request) {
		modelBiz.saveEntity(model);
		this.outJson(response, new ResultBean(true, "添加成功"));
	}
	
	/** 
	 * 更新信息
	 * @param model 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/update")
	public void update(ModelEntity model, HttpServletResponse response, HttpServletRequest request) {
		if(model.getModelId() == null && model.getIdArray() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		modelBiz.updateEntity(model);
		this.outJson(response, new ResultBean(true, "更新成功"));
	}

	/**
	 * 删除
	 * @param response
	 * @param request
	 */
	@RequestMapping("/delete")
	public void delete(ModelEntity model, HttpServletResponse response, HttpServletRequest request) {
		if(model.getModelId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(model.getModelId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		modelBiz.deleteEntity(model.getModelId());		
		this.outJson(response, new ResultBean(true));
	}
	
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @param response
	 * @param request
	 */
	@RequestMapping("/deleteBatch")
	public void deleteBatch(String ids, HttpServletResponse response, HttpServletRequest request) {
		if (StringUtils.isEmpty(ids)) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		String[] idsArray = ids.split(",");
		modelBiz.deleteEntity(idsArray);
		this.outJson(response, new ResultBean(true,"删除成功"));
	}
	
}