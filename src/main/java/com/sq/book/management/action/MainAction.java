package com.sq.book.management.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.constant.SessionConstant;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.BaseUtil;
import com.sq.book.base.util.StringUtil;
import com.sq.book.management.biz.IUserBiz;
import com.sq.book.management.entity.UserEntity;

/**
 * 
 * 管理类，
 * 主要用于管理员登录，以及页面跳转
 * @author sq
 *
 */
@Controller
@RequestMapping("/manager")
public class MainAction extends BaseAction{

	@Autowired
	private IUserBiz userBiz;
	
	/**
	 * 检测登录页面
	 * @param userCode
	 * @param password
	 * @param response
	 * @param request
	 */
	@PostMapping("/checkLogin")
	public void checkLogin(String userCode, String password, HttpServletResponse response, HttpServletRequest request){
		UserEntity user = new UserEntity();
		user.setUserCode(userCode);
		
		//打印访问的路径信息
		System.out.println(request.getPathInfo());
		
		//根据账号从数据库中查找用户
		UserEntity _userEntity = (UserEntity) userBiz.getEntity(user);
		
		//从数据库查无此用户
		if (_userEntity == null) {
			outJson(response, new ResultBean(false, "用户不存在"));
			return;
		}
		
		//MD5加密前端提交密码
		String password_web = StringUtil.MD5(password);
		
		//密码不匹配
		if (!_userEntity.getUserPassword().equals(password_web)) {
			outJson(response, new ResultBean(false, "密码错误"));
			return;
		}
		
		//设置密码
		user.setUserPassword(password_web);
		
		//向shiro认证用户，提交过去的密码是前端的密码
		SecurityUtils.getSubject().login(new UsernamePasswordToken(_userEntity.getUserCode(), password));
		
		//向servlet session对象加入user对象
		request.getSession().setAttribute(SessionConstant.USER, _userEntity);
		outJson(response, new ResultBean(true, "登录成功"));

	}

	/**
	 * 退出	重定向到登录页面
	 * @return
	 */
	@RequestMapping("/logout")
	public String exit() {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:/login.action";
	}
	
	/**
	 * 检测登录后登录页面
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping("/login")
	public String login(HttpServletResponse response, HttpServletRequest request){
		//向servlet session对象加入项目名
		request.getSession().setAttribute(SessionConstant.BASE, request.getContextPath());
		return "/management/login/login";
	}
	
	/**
	 * 检测登录后登录到后台管理主页面
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletResponse response, HttpServletRequest request){
		UserEntity user = (UserEntity)request.getSession().getAttribute(SessionConstant.USER);
		if(user == null){
			outJson(response, new ResultBean(false, "请先登录"));
			return "";
		}
		return "/management/index";
	}
}
