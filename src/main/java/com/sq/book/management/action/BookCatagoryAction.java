package com.sq.book.management.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.management.biz.IBookCatagoryBiz;
import com.sq.book.management.entity.BookCatagoryEntity;
	
/**
 * 管理控制层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/manager/bookCatagory")

public class BookCatagoryAction extends BaseAction {
	
	/**
	 * 注入业务层
	 */	
	@Autowired
	private IBookCatagoryBiz bookCatagoryBiz;
	
	/**
	 * 主页
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletResponse response, HttpServletRequest request) {
		return "/bookCatagory/index";
	}
	
	/**
	 * 表单
	 * @return
	 */
	@RequestMapping("/form")
	public String form(BookCatagoryEntity bookCatagory, HttpServletResponse response, HttpServletRequest request) {
		if (bookCatagory.getBcId() != null && bookCatagory.getBcId() > 0) {
			bookCatagory = (BookCatagoryEntity) bookCatagoryBiz.getEntity(bookCatagory.getBcId());
		}
		request.setAttribute("bookCatagory", bookCatagory);
		return "/bookCatagory/form";
	}
	
	/**
	 * 查询列表
	 * @param bookCatagory 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/list")
	public void list(BookCatagoryEntity bookCatagory, HttpServletResponse response, HttpServletRequest request) {
		PageUtil.startPage();
		List bookCatagoryList = bookCatagoryBiz.query(bookCatagory);
		this.outJson(response, new EasyUIBean(bookCatagoryList, (int)PageUtil.endPage(bookCatagoryList).getTotal()), "yyyy-MM-dd");
	}
	
	/**
	 * 根据实体查询
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/query")
	public void queryAll(BookCatagoryEntity bookCatagory, HttpServletResponse response, HttpServletRequest request) {
		List bookCatagoryList = bookCatagoryBiz.query(bookCatagory);
		this.outJson(response,bookCatagoryList);
	}
	
	/**
	 * 获取实体
	 * @param bookCatagory 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/get")
	public void get(BookCatagoryEntity bookCatagory, HttpServletResponse response, HttpServletRequest request){
		if(bookCatagory.getBcId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(bookCatagory.getBcId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		BookCatagoryEntity _bookCatagory = (BookCatagoryEntity) bookCatagoryBiz.getEntity(bookCatagory.getBcId());
		this.outJson(response, new ResultBean(true, _bookCatagory));
	}
	
	/**
	 * 保存实体
	 * @param bookCatagory 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/save")
	public void save(BookCatagoryEntity bookCatagory, HttpServletResponse response, HttpServletRequest request) {
		bookCatagoryBiz.saveEntity(bookCatagory);
		this.outJson(response, new ResultBean(true, bookCatagory));
	}
	
	/** 
	 * 更新信息
	 * @param bookCatagory 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/update")
	public void update(BookCatagoryEntity bookCatagory, HttpServletResponse response, HttpServletRequest request) {
		if(bookCatagory.getBcId() == null && bookCatagory.getIdArray() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		bookCatagoryBiz.updateEntity(bookCatagory);
		this.outJson(response, new ResultBean(true, bookCatagory));
	}

	/**
	 * 删除
	 * @param response
	 * @param request
	 */
	@RequestMapping("/delete")
	public void delete(BookCatagoryEntity bookCatagory, HttpServletResponse response, HttpServletRequest request) {
		if(bookCatagory.getBcId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(bookCatagory.getBcId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		bookCatagoryBiz.deleteEntity(bookCatagory.getBcId());		
		this.outJson(response, new ResultBean(true));
	}
	
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @param response
	 * @param request
	 */
	@RequestMapping("/deleteBatch")
	public void deleteBatch(String ids, HttpServletResponse response, HttpServletRequest request) {
		if (StringUtils.isEmpty(ids)) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		String[] idsArray = ids.split(",");
		bookCatagoryBiz.deleteEntity(idsArray);
		this.outJson(response, new ResultBean(true));
	}
	
}