package com.sq.book.management.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.management.biz.IPurchaseBookBiz;
import com.sq.book.management.entity.PurchaseBookEntity;
	
/**
 * 管理控制层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2017-12-20 14:36:10<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/manager/purchaseBook")

public class PurchaseBookAction extends BaseAction {
	
	/**
	 * 注入业务层
	 */	
	@Autowired
	private IPurchaseBookBiz purchaseBookBiz;
	
	/**
	 * 主页
	 * @return
	 */
	@RequestMapping("/index")
	@RequiresAuthentication
	public String index(HttpServletResponse response, HttpServletRequest request) {
		return "/management/purchaseBook/index";
	}
	
	/**
	 * 表单
	 * @return
	 */
	@RequestMapping("/form")
	public String form(PurchaseBookEntity purchaseBook, HttpServletResponse response, HttpServletRequest request) {
		if (purchaseBook.getPbId() != null && purchaseBook.getPbId() > 0) {
			purchaseBook = (PurchaseBookEntity) purchaseBookBiz.getEntity(purchaseBook.getPbId());
		}
		request.setAttribute("purchaseBook", purchaseBook);
		return "/purchaseBook/form";
	}
	
	/**
	 * 查询列表
	 * @param purchaseBook 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/list")
	public void list(PurchaseBookEntity purchaseBook, HttpServletResponse response, HttpServletRequest request) {
		PageUtil.startPage();
		List purchaseBookList = purchaseBookBiz.query(purchaseBook);
		this.outJson(response, new EasyUIBean(purchaseBookList, (int)PageUtil.endPage(purchaseBookList).getTotal()), "yyyy-MM-dd");
	}
	
	/**
	 * 根据实体查询
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/query")
	public void queryAll(PurchaseBookEntity purchaseBook, HttpServletResponse response, HttpServletRequest request) {
		List purchaseBookList = purchaseBookBiz.query(purchaseBook);
		this.outJson(response, new EasyUIBean(purchaseBookList, purchaseBookList.size()), "yyyy-MM-dd");
	}
	
	/**
	 * 获取实体
	 * @param purchaseBook 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/get")
	public void get(PurchaseBookEntity purchaseBook, HttpServletResponse response, HttpServletRequest request){
		if(purchaseBook.getPbId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(purchaseBook.getPbId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		PurchaseBookEntity _purchaseBook = (PurchaseBookEntity) purchaseBookBiz.getEntity(purchaseBook.getPbId());
		this.outJson(response, new ResultBean(true, _purchaseBook));
	}
	
	/**
	 * 保存实体
	 * @param purchaseBook 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/save")
	public void save(PurchaseBookEntity purchaseBook, HttpServletResponse response, HttpServletRequest request) {
		purchaseBookBiz.saveEntity(purchaseBook);
		this.outJson(response, new ResultBean(true, purchaseBook));
	}
	
	/** 
	 * 更新信息
	 * @param purchaseBook 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/update")
	public void update(PurchaseBookEntity purchaseBook, HttpServletResponse response, HttpServletRequest request) {
		if(purchaseBook.getPbId() == null && purchaseBook.getIdArray() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		purchaseBookBiz.updateEntity(purchaseBook);
		this.outJson(response, new ResultBean(true, purchaseBook));
	}

	/**
	 * 删除
	 * @param response
	 * @param request
	 */
	@RequestMapping("/delete")
	public void delete(PurchaseBookEntity purchaseBook, HttpServletResponse response, HttpServletRequest request) {
		if(purchaseBook.getPbId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(purchaseBook.getPbId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		purchaseBookBiz.deleteEntity(purchaseBook.getPbId());		
		this.outJson(response, new ResultBean(true));
	}
	
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @param response
	 * @param request
	 */
	@RequestMapping("/deleteBatch")
	public void deleteBatch(String ids, HttpServletResponse response, HttpServletRequest request) {
		if (StringUtils.isEmpty(ids)) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		String[] idsArray = ids.split(",");
		purchaseBookBiz.deleteEntity(idsArray);
		this.outJson(response, new ResultBean(true));
	}
	
}