package com.sq.book.management.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.PageUtil;
import com.sq.book.management.biz.IBookBorrowBiz;
import com.sq.book.management.entity.BookBorrowEntity;
	
/**
 * 管理控制层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
@Controller
@RequestMapping("/bookBorrow")

public class BookBorrowAction extends BaseAction {
	
	/**
	 * 注入业务层
	 */	
	@Autowired
	private IBookBorrowBiz bookBorrowBiz;
	
	/**
	 * 主页
	 * @return
	 */
	@RequestMapping("/index")
	public String index(HttpServletResponse response, HttpServletRequest request) {
		return "/bookBorrow/index";
	}
	
	/**
	 * 表单
	 * @return
	 */
	@RequestMapping("/form")
	public String form(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request) {
		if (bookBorrow.getBbId() != null && bookBorrow.getBbId() > 0) {
			bookBorrow = (BookBorrowEntity) bookBorrowBiz.getEntity(bookBorrow.getBbId());
		}
		request.setAttribute("bookBorrow", bookBorrow);
		return "/bookBorrow/form";
	}
	
	/**
	 * 查询列表
	 * @param bookBorrow 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/list")
	public void list(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request) {
		PageUtil.startPage();
		List bookBorrowList = bookBorrowBiz.query(bookBorrow);
		this.outJson(response, new EasyUIBean(bookBorrowList, (int)PageUtil.endPage(bookBorrowList).getTotal()), "yyyy-MM-dd");
	}
	
	/**
	 * 根据实体查询
	 * 
	 * @param response
	 * @param request
	 */
	@RequestMapping("/query")
	public void queryAll(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request) {
		List bookBorrowList = bookBorrowBiz.query(bookBorrow);
		this.outJson(response, new EasyUIBean(bookBorrowList, bookBorrowList.size()), "yyyy-MM-dd");
	}
	
	/**
	 * 获取实体
	 * @param bookBorrow 实体
	 * @param response
	 * @param request
	 */
	@RequestMapping("/get")
	public void get(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request){
		if(bookBorrow.getBbId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(bookBorrow.getBbId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		BookBorrowEntity _bookBorrow = (BookBorrowEntity) bookBorrowBiz.getEntity(bookBorrow.getBbId());
		this.outJson(response, new ResultBean(true, _bookBorrow));
	}
	
	/**
	 * 保存实体
	 * @param bookBorrow 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/save")
	public void save(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request) {
		bookBorrowBiz.saveEntity(bookBorrow);
		this.outJson(response, new ResultBean(true, bookBorrow));
	}
	
	/** 
	 * 更新信息
	 * @param bookBorrow 实体
	 * @param response
	 * @param request
	 */
	@PostMapping("/update")
	public void update(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request) {
		if(bookBorrow.getBbId() == null && bookBorrow.getIdArray() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		bookBorrowBiz.updateEntity(bookBorrow);
		this.outJson(response, new ResultBean(true, bookBorrow));
	}

	/**
	 * 删除
	 * @param response
	 * @param request
	 */
	@RequestMapping("/delete")
	public void delete(BookBorrowEntity bookBorrow, HttpServletResponse response, HttpServletRequest request) {
		if(bookBorrow.getBbId() == null) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		if(bookBorrow.getBbId() <= 0) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		bookBorrowBiz.deleteEntity(bookBorrow.getBbId());		
		this.outJson(response, new ResultBean(true));
	}
	
	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @param response
	 * @param request
	 */
	@RequestMapping("/deleteBatch")
	public void deleteBatch(String ids, HttpServletResponse response, HttpServletRequest request) {
		if (StringUtils.isEmpty(ids)) {
			this.outJson(response, new ResultBean(false));
			return;
		}
		String[] idsArray = ids.split(",");
		bookBorrowBiz.deleteEntity(idsArray);
		this.outJson(response, new ResultBean(true));
	}
	
}