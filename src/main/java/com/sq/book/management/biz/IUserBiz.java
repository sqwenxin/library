package com.sq.book.management.biz;

import com.sq.book.base.biz.IBaseBiz;

/**
 * 业务
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:53<br/>
 * 历史修订：<br/>
 */
public interface IUserBiz extends IBaseBiz {

}