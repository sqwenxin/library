package com.sq.book.management.biz;

import com.sq.book.base.biz.IBaseBiz;
import com.sq.book.base.entity.ResultBean;


/**
 * 业务
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2017-12-20 14:36:10<br/>
 * 历史修订：<br/>
 */
public interface IWorkFlowBiz  extends IBaseBiz{

	ResultBean start(String cls, String map, String bussinessKey, String state);

}
