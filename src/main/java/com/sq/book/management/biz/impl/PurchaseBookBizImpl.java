package com.sq.book.management.biz.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sq.book.base.biz.impl.BaseBizImpl;
import com.sq.book.base.dao.IBaseDao;
import com.sq.book.base.entity.BaseEntity;
import com.sq.book.management.biz.IPurchaseBookBiz;
import com.sq.book.management.dao.IPurchaseBookDao;

/**
 * 管理持久化层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2017-12-20 14:36:10<br/>
 * 历史修订：<br/>
 */
 @Service("purchaseBookBizImpl")
public class PurchaseBookBizImpl extends BaseBizImpl implements IPurchaseBookBiz {

	@Autowired
	private IPurchaseBookDao purchaseBookDao;
	
	@Override
	protected IBaseDao getDao() {
		return purchaseBookDao;
	}


	
}