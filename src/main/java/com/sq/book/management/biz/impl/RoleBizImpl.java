package com.sq.book.management.biz.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sq.book.management.biz.IRoleBiz;
import com.sq.book.management.dao.IRoleDao;
import com.sq.book.base.biz.impl.BaseBizImpl;
import com.sq.book.base.dao.IBaseDao;

/**
 * 管理持久化层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
 @Service("roleBizImpl")
public class RoleBizImpl extends BaseBizImpl implements IRoleBiz {

	@Autowired
	private IRoleDao roleDao;
	
	@Override
	protected IBaseDao getDao() {
		return roleDao;
	}
	
}