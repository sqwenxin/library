package com.sq.book.management.biz.impl;

import com.alibaba.fastjson.JSONObject;
import com.sq.book.activity.ann.WorkFlow;
import com.sq.book.base.biz.impl.BaseBizImpl;
import com.sq.book.base.constant.SessionConstant;
import com.sq.book.base.dao.IBaseDao;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.base.util.SpringUtil;
import com.sq.book.management.biz.IWorkFlowBiz;
import com.sq.book.management.dao.IPurchaseBookDao;
import com.sq.book.management.entity.UserEntity;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("workFlowBizImpl")
public class WorkFlowBizImpl  extends BaseBizImpl implements IWorkFlowBiz {
		
	@Autowired
	private RuntimeService runtimeService;

	
	@Autowired
	private IPurchaseBookDao purchaseBookDao;
	
	@Override
	protected IBaseDao getDao() {
		return purchaseBookDao;
	}
	
	/**
	 * 启动流程
	 */
	@Override
	public ResultBean start(String cls,String map, String bussinessKey,String state){
		try {
			UserEntity user = (UserEntity)SpringUtil.getRequest().getSession().getAttribute(SessionConstant.USER);
			Class clazz = Class.forName(cls);
			WorkFlow workFolw = (WorkFlow) clazz.getAnnotation(WorkFlow.class);
			Map<String,Object> mapObj = JSONObject.parseObject(map,Map.class);
			mapObj.put("startUserName",user.getUserName());
			mapObj.put("TaskName","启动");


			ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(workFolw.procDefKey(), bussinessKey, mapObj);	
			String sql = "update " + workFolw.table() 
				+  " set " + workFolw.procInstColumn() + "=" + processInstance.getId() 
				+ "," + workFolw.statusColumn() + "=" + state
				+ " where " + workFolw.tableId() + "=" + bussinessKey;
			purchaseBookDao.excuteSql(sql);
			return new ResultBean(true,"启动成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResultBean(false,"启动失败");
		}		
	}


}
