package com.sq.book.management.biz.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sq.book.management.biz.IBookBiz;
import com.sq.book.management.dao.IBookDao;
import com.sq.book.base.biz.impl.BaseBizImpl;
import com.sq.book.base.dao.IBaseDao;
import com.sq.book.base.entity.BaseEntity;

/**
 * 管理持久化层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
 @Service("bookBizImpl")
public class BookBizImpl extends BaseBizImpl implements IBookBiz {

	@Autowired
	private IBookDao bookDao;
	
	@Override
	protected IBaseDao getDao() {
		return bookDao;
	}

}