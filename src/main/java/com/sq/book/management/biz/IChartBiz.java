package com.sq.book.management.biz;

import com.sq.book.base.biz.IBaseBiz;

import java.util.List;

/**
 * 报表接口
 */
public interface IChartBiz extends IBaseBiz {
    List queryByCategory();
}
