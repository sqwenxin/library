package com.sq.book.management.entity;

import java.util.Date;

import com.sq.book.activity.ann.WorkFlow;
import org.springframework.format.annotation.DateTimeFormat;
import com.sq.book.base.entity.BaseEntity;

 /**
 * 实体
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
 @WorkFlow(procDefKey = "borrowBook",
		 procInstColumn = "bb_process_instace_id",
		 statusColumn = "bb_process_state",
		 table = "book_borrow",
		 tableId = "bb_id")
public class BookBorrowEntity extends BaseEntity {

	 /**
	  * 关联图书信息
	  */
	private BookEntity book;

	/**
	 * 图书借阅ID
	 */
	private Integer bbId;
	
	/**
	 * 用户ID
	 */
	private Integer bbUserId;
	
	/**
	 * 图书ID
	 */
	private Integer bbBookId;
	
	/**
	 * 图书名称
	 */
	private String bbBookName;
	
	/**
	 * 状态：0：开始，1：确认收到，2:使用中，3：已归还，4：确认归还
	 */
	private Integer bbProcessState;
	
	/**
	 * 流程实例Id
	 */
	private String bbProcessInstaceId;
	
	/**
	 * 创建人
	 */
	private Integer bbCreateBy;
	
	/**
	 * 创建时间
	 */
	private Date bbCreateDate;
	
	/**
	 * 更新人
	 */
	private Integer bbUpdateBy;
	
	/**
	 * 更新时间
	 */
	private Date bbUpdateDate;
	
	/**
	 * 是否删除:0：未删除，1：已删除
	 */
	private Integer bbDel;
	
	public BookBorrowEntity(){}
	
	/**
	 * 设置图书借阅ID
	 */
	public void setBbId(Integer bbId) {
		this.bbId = bbId;
	}

	/**
	 * 获取图书借阅ID
	 */
	public Integer getBbId() {
		return this.bbId;
	}
	
	/**
	 * 设置用户ID
	 */
	public void setBbUserId(Integer bbUserId) {
		this.bbUserId = bbUserId;
	}

	/**
	 * 获取用户ID
	 */
	public Integer getBbUserId() {
		return this.bbUserId;
	}
	
	/**
	 * 设置图书ID
	 */
	public void setBbBookId(Integer bbBookId) {
		this.bbBookId = bbBookId;
	}

	/**
	 * 获取图书ID
	 */
	public Integer getBbBookId() {
		return this.bbBookId;
	}
	
	/**
	 * 设置图书名称
	 */
	public void setBbBookName(String bbBookName) {
		this.bbBookName = bbBookName;
	}

	/**
	 * 获取图书名称
	 */
	public String getBbBookName() {
		return this.bbBookName;
	}
	
	/**
	 * 设置状态：0：开始，1：确认收到，2:使用中，3：已归还，4：确认归还
	 */
	public void setBbProcessState(Integer bbProcessState) {
		this.bbProcessState = bbProcessState;
	}

	/**
	 * 获取状态：0：开始，1：确认收到，2:使用中，3：已归还，4：确认归还
	 */
	public Integer getBbProcessState() {
		return this.bbProcessState;
	}
	
	/**
	 * 设置流程实例Id
	 */
	public void setBbProcessInstaceId(String bbProcessInstaceId) {
		this.bbProcessInstaceId = bbProcessInstaceId;
	}

	/**
	 * 获取流程实例Id
	 */
	public String getBbProcessInstaceId() {
		return this.bbProcessInstaceId;
	}
	
	/**
	 * 设置创建人
	 */
	public void setBbCreateBy(Integer bbCreateBy) {
		this.bbCreateBy = bbCreateBy;
	}

	/**
	 * 获取创建人
	 */
	public Integer getBbCreateBy() {
		return this.bbCreateBy;
	}
	
	/**
	 * 设置创建时间
	 */
	public void setBbCreateDate(Date bbCreateDate) {
		this.bbCreateDate = bbCreateDate;
	}

	/**
	 * 获取创建时间
	 */
	public Date getBbCreateDate() {
		return this.bbCreateDate;
	}
	
	/**
	 * 设置更新人
	 */
	public void setBbUpdateBy(Integer bbUpdateBy) {
		this.bbUpdateBy = bbUpdateBy;
	}

	/**
	 * 获取更新人
	 */
	public Integer getBbUpdateBy() {
		return this.bbUpdateBy;
	}
	
	/**
	 * 设置更新时间
	 */
	public void setBbUpdateDate(Date bbUpdateDate) {
		this.bbUpdateDate = bbUpdateDate;
	}

	/**
	 * 获取更新时间
	 */
	public Date getBbUpdateDate() {
		return this.bbUpdateDate;
	}
	
	/**
	 * 设置是否删除:0：未删除，1：已删除
	 */
	public void setBbDel(Integer bbDel) {
		this.bbDel = bbDel;
	}

	/**
	 * 获取是否删除:0：未删除，1：已删除
	 */
	public Integer getBbDel() {
		return this.bbDel;
	}

	 public BookEntity getBook() {
		 return book;
	 }

	 public void setBook(BookEntity book) {
		 this.book = book;
	 }
 }