package com.sq.book.management.entity;

import java.util.Date;

import com.sq.book.activity.ann.WorkFlow;
import com.sq.book.base.entity.BaseEntity;

 /**
 * 实体
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2017-12-20 15:50:55<br/>
 * 历史修订：<br/>
 */


@WorkFlow(procDefKey = "purchaseBook", 
		  procInstColumn = "pb_process_instance_id", 
		  statusColumn = "pb_process_state", 
		  table = "purchase_book", 
		  tableId = "pb_id")
public class PurchaseBookEntity extends BaseEntity {
	
	/**
	 * 采购记录Id
	 */
	private Integer pbId;
	
	/**
	 * 采购名称
	 */
	private String pbName;
	
	/**
	 * 采购内容
	 */
	private String pbContent;
	
	/**
	 * 采购价格
	 */
	private String pbPrice;
	
	/**
	 * 采购的流程实例id 
	 */
	private String pbProcessInstanceId;
	
	/**
	 * 流程状态字段
	 */
	private Integer pbProcessState;
	
	/**
	 * 创建人
	 */
	private Integer pbCreateBy;
	
	/**
	 * 创建时间
	 */
	private Date pbCreateDate;
	
	/**
	 * 更新人
	 */
	private Integer pbUpdateBy;
	
	/**
	 * 更新时间
	 */
	private Date pbUpdateDate;
	
	/**
	 * 是否删除：0 未删除，1：已删除
	 */
	private Integer pbDel;
	
	public PurchaseBookEntity(){}
	
	/**
	 * 设置采购记录Id
	 */
	public void setPbId(Integer pbId) {
		this.pbId = pbId;
	}

	/**
	 * 获取采购记录Id
	 */
	public Integer getPbId() {
		return this.pbId;
	}
	
	/**
	 * 设置采购名称
	 */
	public void setPbName(String pbName) {
		this.pbName = pbName;
	}

	/**
	 * 获取采购名称
	 */
	public String getPbName() {
		return this.pbName;
	}
	
	/**
	 * 设置采购内容
	 */
	public void setPbContent(String pbContent) {
		this.pbContent = pbContent;
	}

	/**
	 * 获取采购内容
	 */
	public String getPbContent() {
		return this.pbContent;
	}
	
	/**
	 * 设置采购价格
	 */
	public void setPbPrice(String pbPrice) {
		this.pbPrice = pbPrice;
	}

	/**
	 * 获取采购价格
	 */
	public String getPbPrice() {
		return this.pbPrice;
	}
	
	/**
	 * 设置采购的流程实例id 
	 */
	public void setPbProcessInstanceId(String pbProcessInstanceId) {
		this.pbProcessInstanceId = pbProcessInstanceId;
	}

	/**
	 * 获取采购的流程实例id 
	 */
	public String getPbProcessInstanceId() {
		return this.pbProcessInstanceId;
	}
	
	/**
	 * 设置流程状态字段
	 */
	public void setPbProcessState(Integer pbProcessState) {
		this.pbProcessState = pbProcessState;
	}

	/**
	 * 获取流程状态字段
	 */
	public Integer getPbProcessState() {
		return this.pbProcessState;
	}
	
	/**
	 * 设置创建人
	 */
	public void setPbCreateBy(Integer pbCreateBy) {
		this.pbCreateBy = pbCreateBy;
	}

	/**
	 * 获取创建人
	 */
	public Integer getPbCreateBy() {
		return this.pbCreateBy;
	}
	
	/**
	 * 设置创建时间
	 */
	public void setPbCreateDate(Date pbCreateDate) {
		this.pbCreateDate = pbCreateDate;
	}

	/**
	 * 获取创建时间
	 */
	public Date getPbCreateDate() {
		return this.pbCreateDate;
	}
	
	/**
	 * 设置更新人
	 */
	public void setPbUpdateBy(Integer pbUpdateBy) {
		this.pbUpdateBy = pbUpdateBy;
	}

	/**
	 * 获取更新人
	 */
	public Integer getPbUpdateBy() {
		return this.pbUpdateBy;
	}
	
	/**
	 * 设置更新时间
	 */
	public void setPbUpdateDate(Date pbUpdateDate) {
		this.pbUpdateDate = pbUpdateDate;
	}

	/**
	 * 获取更新时间
	 */
	public Date getPbUpdateDate() {
		return this.pbUpdateDate;
	}
	
	/**
	 * 设置是否删除：0 未删除，1：已删除
	 */
	public void setPbDel(Integer pbDel) {
		this.pbDel = pbDel;
	}

	/**
	 * 获取是否删除：0 未删除，1：已删除
	 */
	public Integer getPbDel() {
		return this.pbDel;
	}
	
}