package com.sq.book.management.entity;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import com.sq.book.base.entity.BaseEntity;

 /**
 * 实体
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
public class BookCatagoryEntity extends BaseEntity {

	private List children;
	
	/**
	 * 
	 */
	private Integer bcId;
	
	/**
	 * 分类名称
	 */
	private String bcName;
	
	/**
	 * 分类描述
	 */
	private String bcDescribe;
	
	/**
	 * 上级分类ID
	 */
	private Integer bcParentId;
	
	/**
	 * 创建人
	 */
	private Integer bcCreateBy;
	
	/**
	 * 创建时间
	 */
	private Date bcCreateDate;
	
	/**
	 * 更新时间
	 */
	private Integer bcUpdateBy;
	
	/**
	 * 更新时间
	 */
	private Date bcUpdateDate;
	
	/**
	 * 是否删除：0：未删除，1：已删除
	 */
	private Integer bcDel;
	
	public BookCatagoryEntity(){}
	
	/**
	 * 设置
	 */
	public void setBcId(Integer bcId) {
		this.bcId = bcId;
	}

	/**
	 * 获取
	 */
	public Integer getBcId() {
		return this.bcId;
	}
	
	/**
	 * 设置分类名称
	 */
	public void setBcName(String bcName) {
		this.bcName = bcName;
	}

	/**
	 * 获取分类名称
	 */
	public String getBcName() {
		return this.bcName;
	}
	
	/**
	 * 设置分类描述
	 */
	public void setBcDescribe(String bcDescribe) {
		this.bcDescribe = bcDescribe;
	}

	/**
	 * 获取分类描述
	 */
	public String getBcDescribe() {
		return this.bcDescribe;
	}
	
	/**
	 * 设置上级分类ID
	 */
	public void setBcParentId(Integer bcParentId) {
		this.bcParentId = bcParentId;
	}

	/**
	 * 获取上级分类ID
	 */
	public Integer getBcParentId() {
		return this.bcParentId;
	}
	
	/**
	 * 设置创建人
	 */
	public void setBcCreateBy(Integer bcCreateBy) {
		this.bcCreateBy = bcCreateBy;
	}

	/**
	 * 获取创建人
	 */
	public Integer getBcCreateBy() {
		return this.bcCreateBy;
	}
	
	/**
	 * 设置创建时间
	 */
	public void setBcCreateDate(Date bcCreateDate) {
		this.bcCreateDate = bcCreateDate;
	}

	/**
	 * 获取创建时间
	 */
	public Date getBcCreateDate() {
		return this.bcCreateDate;
	}
	
	/**
	 * 设置更新时间
	 */
	public void setBcUpdateBy(Integer bcUpdateBy) {
		this.bcUpdateBy = bcUpdateBy;
	}

	/**
	 * 获取更新时间
	 */
	public Integer getBcUpdateBy() {
		return this.bcUpdateBy;
	}
	
	/**
	 * 设置更新时间
	 */
	public void setBcUpdateDate(Date bcUpdateDate) {
		this.bcUpdateDate = bcUpdateDate;
	}

	/**
	 * 获取更新时间
	 */
	public Date getBcUpdateDate() {
		return this.bcUpdateDate;
	}
	
	/**
	 * 设置是否删除：0：未删除，1：已删除
	 */
	public void setBcDel(Integer bcDel) {
		this.bcDel = bcDel;
	}

	/**
	 * 获取是否删除：0：未删除，1：已删除
	 */
	public Integer getBcDel() {
		return this.bcDel;
	}

	 public List getChildren() {
		 return children;
	 }

	 public void setChildren(List children) {
		 this.children = children;
	 }
 }