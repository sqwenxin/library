package com.sq.book.management.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.sq.book.base.entity.BaseEntity;

 /**
 * 实体
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:53<br/>
 * 历史修订：<br/>
 */
public class UserEntity extends BaseEntity {

	private RoleEntity role;
	
	/**
	 * 记录Id
	 */
	private Integer userId;
	
	/**
	 * 用户编号
	 */
	private String userCode;
	
	/**
	 * 用户密码
	 */
	private String userPassword;
	
	/**
	 * 用户名
	 */
	private String userName;
	
	/**
	 * 用户状态：0：新建，1：审核中，2：审核通过
	 */
	private Integer userState;
	
	/**
	 * 关联角色ID
	 */
	private Integer userRoleId;
	
	/**
	 * 创建人
	 */
	private Integer userCreateBy;
	
	/**
	 * 创建时间
	 */
	private Date userCreateDate;
	
	/**
	 * 更新人
	 */
	private Integer userUpdateBy;
	
	/**
	 * 更新时间
	 */
	private Date userUpdateDate;
	
	/**
	 * 是否删除：0：未删除，1：已删除
	 */
	private Integer userDel;
	
	public UserEntity(){}
	
	/**
	 * 设置记录Id
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * 获取记录Id
	 */
	public Integer getUserId() {
		return this.userId;
	}
	
	/**
	 * 设置用户编号
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	/**
	 * 获取用户编号
	 */
	public String getUserCode() {
		return this.userCode;
	}
	
	/**
	 * 设置用户密码
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * 获取用户密码
	 */
	public String getUserPassword() {
		return this.userPassword;
	}
	
	/**
	 * 设置用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 获取用户名
	 */
	public String getUserName() {
		return this.userName;
	}
	
	/**
	 * 设置用户状态：0：新建，1：审核中，2：审核通过
	 */
	public void setUserState(Integer userState) {
		this.userState = userState;
	}

	/**
	 * 获取用户状态：0：新建，1：审核中，2：审核通过
	 */
	public Integer getUserState() {
		return this.userState;
	}
	
	/**
	 * 设置关联角色ID
	 */
	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	/**
	 * 获取关联角色ID
	 */
	public Integer getUserRoleId() {
		return this.userRoleId;
	}
	
	/**
	 * 设置创建人
	 */
	public void setUserCreateBy(Integer userCreateBy) {
		this.userCreateBy = userCreateBy;
	}

	/**
	 * 获取创建人
	 */
	public Integer getUserCreateBy() {
		return this.userCreateBy;
	}
	
	/**
	 * 设置创建时间
	 */
	public void setUserCreateDate(Date userCreateDate) {
		this.userCreateDate = userCreateDate;
	}

	/**
	 * 获取创建时间
	 */
	public Date getUserCreateDate() {
		return this.userCreateDate;
	}
	
	/**
	 * 设置更新人
	 */
	public void setUserUpdateBy(Integer userUpdateBy) {
		this.userUpdateBy = userUpdateBy;
	}

	/**
	 * 获取更新人
	 */
	public Integer getUserUpdateBy() {
		return this.userUpdateBy;
	}
	
	/**
	 * 设置更新时间
	 */
	public void setUserUpdateDate(Date userUpdateDate) {
		this.userUpdateDate = userUpdateDate;
	}

	/**
	 * 获取更新时间
	 */
	public Date getUserUpdateDate() {
		return this.userUpdateDate;
	}
	
	/**
	 * 设置是否删除：0：未删除，1：已删除
	 */
	public void setUserDel(Integer userDel) {
		this.userDel = userDel;
	}

	/**
	 * 获取是否删除：0：未删除，1：已删除
	 */
	public Integer getUserDel() {
		return this.userDel;
	}

	 public RoleEntity getRole() {
		 return role;
	 }

	 public void setRole(RoleEntity role) {
		 this.role = role;
	 }
 }