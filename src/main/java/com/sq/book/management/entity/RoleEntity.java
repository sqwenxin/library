package com.sq.book.management.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.sq.book.base.entity.BaseEntity;

 /**
 * 实体
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
public class RoleEntity extends BaseEntity {
	
	/**
	 * 角色Id
	 */
	private Integer roleId;
	
	/**
	 * 角色名称
	 */
	private String roleName;
	
	/**
	 * 角色描述
	 */
	private String roleDescribe;
	
	/**
	 * 创建人
	 */
	private Integer roleCreateBy;
	
	/**
	 * 创建时间
	 */
	private Date roleCreateDate;
	
	/**
	 * 更新人
	 */
	private Integer roleUpdateBy;
	
	/**
	 * 创建时间
	 */
	private Date roleUpdateDate;
	
	/**
	 * 是否删除：0:未删除，1：已删除
	 */
	private Integer roleDel;
	
	public RoleEntity(){}
	
	/**
	 * 设置角色Id
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * 获取角色Id
	 */
	public Integer getRoleId() {
		return this.roleId;
	}
	
	/**
	 * 设置角色名称
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * 获取角色名称
	 */
	public String getRoleName() {
		return this.roleName;
	}
	
	/**
	 * 设置角色描述
	 */
	public void setRoleDescribe(String roleDescribe) {
		this.roleDescribe = roleDescribe;
	}

	/**
	 * 获取角色描述
	 */
	public String getRoleDescribe() {
		return this.roleDescribe;
	}
	
	/**
	 * 设置创建人
	 */
	public void setRoleCreateBy(Integer roleCreateBy) {
		this.roleCreateBy = roleCreateBy;
	}

	/**
	 * 获取创建人
	 */
	public Integer getRoleCreateBy() {
		return this.roleCreateBy;
	}
	
	/**
	 * 设置创建时间
	 */
	public void setRoleCreateDate(Date roleCreateDate) {
		this.roleCreateDate = roleCreateDate;
	}

	/**
	 * 获取创建时间
	 */
	public Date getRoleCreateDate() {
		return this.roleCreateDate;
	}
	
	/**
	 * 设置更新人
	 */
	public void setRoleUpdateBy(Integer roleUpdateBy) {
		this.roleUpdateBy = roleUpdateBy;
	}

	/**
	 * 获取更新人
	 */
	public Integer getRoleUpdateBy() {
		return this.roleUpdateBy;
	}
	
	/**
	 * 设置创建时间
	 */
	public void setRoleUpdateDate(Date roleUpdateDate) {
		this.roleUpdateDate = roleUpdateDate;
	}

	/**
	 * 获取创建时间
	 */
	public Date getRoleUpdateDate() {
		return this.roleUpdateDate;
	}
	
	/**
	 * 设置是否删除：0:未删除，1：已删除
	 */
	public void setRoleDel(Integer roleDel) {
		this.roleDel = roleDel;
	}

	/**
	 * 获取是否删除：0:未删除，1：已删除
	 */
	public Integer getRoleDel() {
		return this.roleDel;
	}
	
}