package com.sq.book.management.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.sq.book.base.entity.BaseEntity;

 /**
 * 实体
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
public class RoleModelEntity extends BaseEntity {


	 public ModelEntity model;
	
	/**
	 * 
	 */
	private Integer rmId;
	
	/**
	 * 角色ID
	 */
	private Integer rmRoleId;
	
	/**
	 * 模块Id
	 */
	private Integer rmModelId;

	 public RoleModelEntity(){}

	 public RoleModelEntity(Integer rmRoleId){
		 this.rmRoleId = rmRoleId;
	 }
	
	/**
	 * 设置
	 */
	public void setRmId(Integer rmId) {
		this.rmId = rmId;
	}

	/**
	 * 获取
	 */
	public Integer getRmId() {
		return this.rmId;
	}
	
	/**
	 * 设置角色ID
	 */
	public void setRmRoleId(Integer rmRoleId) {
		this.rmRoleId = rmRoleId;
	}

	/**
	 * 获取角色ID
	 */
	public Integer getRmRoleId() {
		return this.rmRoleId;
	}
	
	/**
	 * 设置模块Id
	 */
	public void setRmModelId(Integer rmModelId) {
		this.rmModelId = rmModelId;
	}

	/**
	 * 获取模块Id
	 */
	public Integer getRmModelId() {
		return this.rmModelId;
	}


	 public ModelEntity getModel() {
		 return model;
	 }

	 public void setModel(ModelEntity model) {
		 this.model = model;
	 }
	
}