package com.sq.book.management.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.sq.book.base.entity.BaseEntity;

 /**
 * 实体
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
public class ModelEntity extends BaseEntity {
	
	/**
	 * 
	 */
	private Integer modelId;
	
	/**
	 * 
	 */
	private String modelCode;
	
	/**
	 * 父级Id
	 */
	private Integer modelParentCode;
	
	private String _parentId = null;
	
	/**
	 * 模块名称
	 */
	private String modelName;
	
	/**
	 * 模块描述
	 */
	private String modelDescribe;
	
	/**
	 * 模块链接
	 */
	private String modelLink;
	
	/**
	 * 创建人
	 */
	private Integer modelCreateBy;
	
	/**
	 * 创建时间
	 */
	private Date modelCreateDate;
	
	/**
	 * 更新人
	 */
	private Integer modelUpdateBy;
	
	/**
	 * 创建时间
	 */
	private Date modelUpdateDate;
	
	/**
	 * 是否删除:0：未删除，1：已删除
	 */
	private Integer modelDel;
	
	public ModelEntity(){}
	
	/**
	 * 设置
	 */
	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}

	/**
	 * 获取
	 */
	public Integer getModelId() {
		return this.modelId;
	}
	
	/**
	 * 设置
	 */
	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	/**
	 * 获取
	 */
	public String getModelCode() {
		return this.modelCode;
	}
	
	/**
	 * 设置父级Id
	 */
	public void setModelParentCode(Integer modelParentCode) {
		this.modelParentCode = modelParentCode;
	}

	/**
	 * 获取父级Id
	 */
	public Integer getModelParentCode() {
		return this.modelParentCode;
	}
	
	/**
	 * 设置模块名称
	 */
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	/**
	 * 获取模块名称
	 */
	public String getModelName() {
		return this.modelName;
	}
	
	/**
	 * 设置模块描述
	 */
	public void setModelDescribe(String modelDescribe) {
		this.modelDescribe = modelDescribe;
	}

	/**
	 * 获取模块描述
	 */
	public String getModelDescribe() {
		return this.modelDescribe;
	}
	
	/**
	 * 设置模块链接
	 */
	public void setModelLink(String modelLink) {
		this.modelLink = modelLink;
	}

	/**
	 * 获取模块链接
	 */
	public String getModelLink() {
		return this.modelLink;
	}
	
	/**
	 * 设置创建人
	 */
	public void setModelCreateBy(Integer modelCreateBy) {
		this.modelCreateBy = modelCreateBy;
	}

	/**
	 * 获取创建人
	 */
	public Integer getModelCreateBy() {
		return this.modelCreateBy;
	}
	
	/**
	 * 设置创建时间
	 */
	public void setModelCreateDate(Date modelCreateDate) {
		this.modelCreateDate = modelCreateDate;
	}

	/**
	 * 获取创建时间
	 */
	public Date getModelCreateDate() {
		return this.modelCreateDate;
	}
	
	/**
	 * 设置更新人
	 */
	public void setModelUpdateBy(Integer modelUpdateBy) {
		this.modelUpdateBy = modelUpdateBy;
	}

	/**
	 * 获取更新人
	 */
	public Integer getModelUpdateBy() {
		return this.modelUpdateBy;
	}
	
	/**
	 * 设置创建时间
	 */
	public void setModelUpdateDate(Date modelUpdateDate) {
		this.modelUpdateDate = modelUpdateDate;
	}

	/**
	 * 获取创建时间
	 */
	public Date getModelUpdateDate() {
		return this.modelUpdateDate;
	}
	
	/**
	 * 设置是否删除:0：未删除，1：已删除
	 */
	public void setModelDel(Integer modelDel) {
		this.modelDel = modelDel;
	}

	/**
	 * 获取是否删除:0：未删除，1：已删除
	 */
	public Integer getModelDel() {
		return this.modelDel;
	}

	public String get_parentId() {
		return _parentId;
	}

	public void set_parentId(String _parentId) {
		this._parentId = _parentId;
	}
	
}