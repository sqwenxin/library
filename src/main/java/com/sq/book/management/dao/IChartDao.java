package com.sq.book.management.dao;

import com.sq.book.base.dao.IBaseDao;

import java.util.List;

public interface IChartDao extends IBaseDao {
    List queryByCategory();
}
