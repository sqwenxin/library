package com.sq.book.management.dao;

import com.sq.book.base.dao.IBaseDao;

/**
 * 持久层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2017-12-20 15:50:55<br/>
 * 历史修订：<br/>
 */
public interface IPurchaseBookDao extends IBaseDao {
}