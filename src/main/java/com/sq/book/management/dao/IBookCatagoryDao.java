package com.sq.book.management.dao;

import com.sq.book.base.dao.IBaseDao;

/**
 * 持久层
 * @author 孙乾
 * @version 
 * 版本号：1.0.0<br/>
 * 创建日期：2018-1-24 22:29:54<br/>
 * 历史修订：<br/>
 */
public interface IBookCatagoryDao extends IBaseDao {
}