package com.sq.book.activity.listen;

import com.sq.book.activity.ann.WorkFlow;
import com.sq.book.base.util.SpringUtil;
import com.sq.book.management.biz.IPurchaseBookBiz;
import com.sq.book.management.entity.PurchaseBookEntity;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * 书籍采购监听流程
 */
public class PurchaseBookListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {
        //事件名称
        String eventName = delegateExecution.getEventName();
        String activityName = delegateExecution.getCurrentActivityName();
        String currenActivityName = delegateExecution.getCurrentActivityName();
        String confirm = (String)delegateExecution.getVariable("confirm");
        String directorResult = (String)delegateExecution.getVariable("directorResult");
        String financeResult = (String)delegateExecution.getVariable("financeResult");

        String bussInessKey = delegateExecution.getProcessBusinessKey();
        String processInstanceId = delegateExecution.getProcessInstanceId();
        Integer state = 0;
        Class clazz = Class.forName("com.sq.book.management.entity.PurchaseBookEntity");
        WorkFlow workFolw = (WorkFlow) clazz.getAnnotation(WorkFlow.class);

        //更具beanName 获取业务层
        IPurchaseBookBiz purchaseBookBiz = (IPurchaseBookBiz) SpringUtil.getBean("purchaseBookBizImpl");
        List<Map<String,Integer>> list = purchaseBookBiz.createSQLQuery("select " + workFolw.statusColumn() + " from " +
                workFolw.table() + " where " + workFolw.tableId() + "=" + bussInessKey);
        state = list.get(0).get(workFolw.statusColumn());

        if("确认提交".equals(activityName)){
            if(confirm.equals("yes")){
                state ++;
            }else{
                state = 5;//状态为5，表示放弃本次任务
            }
        }
        if("主任审核".equals(activityName)){
            if(directorResult.equals("yes")){
                state ++;
            }else{
                state = 1;
            }
        }
        if("财务审核".equals(activityName)){
            if(financeResult.equals("yes")){
                state ++;
            }else{
                state = 1;//状态为1表示，退回到确认提交菜单
            }
        }
        String sql = "update " + workFolw.table()
                +  " set " + workFolw.procInstColumn() + "=" + processInstanceId
                + "," + workFolw.statusColumn() + "=" + state
                + " where " + workFolw.tableId() + "=" + bussInessKey;
        purchaseBookBiz.excuteSql(sql);
    }
}
