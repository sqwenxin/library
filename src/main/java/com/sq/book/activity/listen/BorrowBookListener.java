package com.sq.book.activity.listen;

import com.sq.book.activity.ann.WorkFlow;
import com.sq.book.base.util.SpringUtil;
import com.sq.book.management.biz.IBookBiz;
import com.sq.book.management.biz.IBookBorrowBiz;
import com.sq.book.management.biz.IPurchaseBookBiz;
import com.sq.book.management.entity.BookBorrowEntity;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

import java.util.List;
import java.util.Map;

public class BorrowBookListener implements ExecutionListener {

    /**
     * state 0 ：尚未启动流程
     * 1 ： 管理员确认中
     * 2 ： 书籍发放
     * 3 ： 确认收到书籍
     * 4 ： 书籍归还中
     * 5 ： 书籍完好确认中
     * 6 ： 丢失 管理员确认赔偿中
     * 7 : 丢失 是否同意赔偿
     * 8 ： 丢失 是否已赔偿
     * 9 ： 损坏管理员确认赔偿中
     * 10 ： 损坏 是否同意赔偿
     * 11 ： 损坏 是否已赔偿
     * 12 ： 结束
     * @param delegateExecution
     * @throws Exception
     */
    @Override
    public void notify(DelegateExecution delegateExecution) throws Exception {
        //事件名称
        String eventName = delegateExecution.getEventName();
        String activityName = delegateExecution.getCurrentActivityName();
        String currenActivityName = delegateExecution.getCurrentActivityName();
        String confirm = (String)delegateExecution.getVariable("confirm");
        String directorResult = (String)delegateExecution.getVariable("directorResult");
        String financeResult = (String)delegateExecution.getVariable("financeResult");
        String activityId = delegateExecution.getCurrentActivityId();

        //管理员审核参数
        String managerAdvice = (String)delegateExecution.getVariable("managerAdvice");

        //书籍发放参数
        String isGrant = (String)delegateExecution.getVariable("isGrant");

        //是否书籍收到书籍
        String isReceive = (String)delegateExecution.getVariable("isReceive");

        //书籍归还时状态
        Integer bookStateType = null;
        if(delegateExecution.getVariable("bookStateType") != null){
            bookStateType =  Integer.parseInt((String) delegateExecution.getVariable("bookStateType"));
        }


        //丢失时是否同意赔偿
        String missIsAgree = (String)delegateExecution.getVariable("missIsAgree");

        //丢失流程是否结束
        String missIsComplete = (String)delegateExecution.getVariable("missIsComplete");

        //书籍是否完好
        String isOK = (String)delegateExecution.getVariable("isOK");

        //损坏时是否同意赔偿
        String damageIsAgree = (String)delegateExecution.getVariable("damageIsAgree");

        //损坏流程是否结束
        String damageIsComplete = (String)delegateExecution.getVariable("damageIsComplete");


        String bussInessKey = delegateExecution.getProcessBusinessKey();
        String processInstanceId = delegateExecution.getProcessInstanceId();
        Integer state = 0;
        Class clazz = Class.forName("com.sq.book.management.entity.BookBorrowEntity");
        WorkFlow workFolw = (WorkFlow) clazz.getAnnotation(WorkFlow.class);

        //更具beanName 获取业务层
        IPurchaseBookBiz purchaseBookBiz = (IPurchaseBookBiz) SpringUtil.getBean("purchaseBookBizImpl");
        //书籍业务层
        IBookBiz bookBiz = (IBookBiz) SpringUtil.getBean("bookBizImpl");
        //借阅业务层
        IBookBorrowBiz bookBorrowBiz = (IBookBorrowBiz) SpringUtil.getBean("bookBorrowBizImpl");

        BookBorrowEntity borrow = (BookBorrowEntity)bookBorrowBiz.getEntity(Integer.parseInt(bussInessKey));

        List<Map<String,Integer>> list = purchaseBookBiz.createSQLQuery("select " + workFolw.statusColumn() + " from " +
                workFolw.table() + " where " + workFolw.tableId() + "=" + bussInessKey);
        state = list.get(0).get(workFolw.statusColumn());


        //管理员审核
        if("managerAudit".equals(activityId)){
            if(managerAdvice.equals("true")){
                state = 2;
            }else{
                state = 12;
            }
        }

        //发放书籍
        if("grant".equals(activityId)){
            if(isGrant.equals("true")){
                state = 3;
            }else{
                state = 12;
            }
        }

        //发放书籍
        if("receiveBook".equals(activityId)){
            if(isReceive.equals("true")){
                state = 4;
                borrow.getBook().setBookManageState(1);
                bookBiz.updateEntity(borrow.getBook());
            }else{
                state = 2;
            }
        }

        //归还书籍
        if("bookReturn".equals(activityId)){
            if(bookStateType == 0){
                state = 5;
            }else if(bookStateType == 1){
                state = 9;
                borrow.getBook().setBookState(2);
                bookBiz.updateEntity(borrow.getBook());
            }else{
                borrow.getBook().setBookState(1);
                bookBiz.updateEntity(borrow.getBook());
                state = 6;
            }
        }

        //丢失确认赔偿金额
        if("missComfirm".equals(activityId)){
            state = 7;
        }

        //丢失是否同意赔偿
        if("missAgreePay".equals(activityId)){
            if(missIsAgree.equals("true")){
                state = 8;
            }else{
                state = 6;
            }
        }

        //丢失流程是否结束
        if("missComlete".equals(activityId)){
            if(missIsComplete.equals("true")){
                borrow.getBook().setBookManageState(0);
                bookBiz.updateEntity(borrow.getBook());
                state = 12;
            }else{
                state = 7;
            }
        }

        //书籍完好
        if("bookWhole".equals(activityId)){
            if(isOK.equals("true")){
                borrow.getBook().setBookManageState(0);
                bookBiz.updateEntity(borrow.getBook());
                state = 12;
            }else{
                state = 4;
            }
        }

        //书籍损坏确认
        if("damageConfirm".equals(activityId)){
            state = 10;
        }

        //书籍损坏是否同意赔偿
        if("damageAgreePay".equals(activityId)){
            if(damageIsAgree.equals("true")){
                state = 11;
            }else{
                state = 9;
            }
        }

        //损坏流程是否结束
        if("damageComplete".equals(activityId)){
            if(damageIsComplete.equals("true")){
                borrow.getBook().setBookManageState(0);
                bookBiz.updateEntity(borrow.getBook());
                state = 12;
            }else{
                state = 10;
            }
        }

        String sql = "update " + workFolw.table()
                +  " set " + workFolw.procInstColumn() + "=" + processInstanceId
                + "," + workFolw.statusColumn() + "=" + state
                + " where " + workFolw.tableId() + "=" + bussInessKey;
        purchaseBookBiz.excuteSql(sql);
    }
}
