package com.sq.book.activity.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sq.book.base.constant.SessionConstant;
import com.sq.book.base.util.PageUtil;
import com.sq.book.base.util.StringUtil;
import com.sq.book.management.entity.UserEntity;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.*;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
import com.sq.book.base.action.BaseAction;
import com.sq.book.base.entity.EasyUIBean;
import com.sq.book.base.entity.ResultBean;
import com.sq.book.commonUtils.EasyUIPageUtil;
import com.sq.book.management.biz.IWorkFlowBiz;

@Controller
@RequestMapping("/workFlow")
public class WorkFlowAction extends BaseAction {
	
	@Autowired
	private RuntimeService runtimeService;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private HistoryService historyService;
	
	@Autowired
	private IWorkFlowBiz workFlowBiz;
	
	/**
	 * 任务列表
	 * @return
	 */
	@RequestMapping("/taskPage")
	public String taskPage(){
		return "/process/taskList";
	}

	/**
	 * 已完成任务列表
	 * @return
	 */
	@RequestMapping("/alreadySolvePage")
	public String alreadySolvePage(){
		return "/process/alreadySolve";
	}

	/**
	 * 跳转到相应的表单
	 * @param formKey
	 * @param procInstId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/form")
	public String form(String formKey,String procInstId,String taskId,HttpServletRequest request, HttpServletResponse response){
		if(StringUtils.isBlank(procInstId)){
			return "";
		}
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
		request.setAttribute("processInstance",processInstance);
		request.setAttribute("taskId",taskId);
		return "/" + formKey;
	}

	/**
	 * 
	 * @param procDefId1 流程标识
	 * @param bussinessKey 业务Id
	 * @param map 参数值
	 * @param request
	 * @param response
	 */
	@RequestMapping("/start")
	public void start(@RequestParam String cls,String map,@RequestParam String bussinessKey,String state,HttpServletRequest request, HttpServletResponse response){
		try {
			if(StringUtils.isBlank(state)){
				state = 0 + "";
			}			
			this.outJson(response, workFlowBiz.start(cls, map, bussinessKey, state));
		} catch (Exception e) {
			e.printStackTrace();
			this.outJson(response, new ResultBean(false, "发生错误启动失败"));
		}		
	}
	
	/**
	 * 查询任务列表
	 * @param request
	 * @param response
	 */
	@RequestMapping("/taskList")
	public void taskList(HttpServletRequest request, HttpServletResponse response){
		try {
			List<Task> taskList = taskService.createTaskQuery().list();
			this.outJson(response, new ResultBean(true,taskList,"查询成功"));
		} catch (Exception e) {
			this.outJson(response, new ResultBean(true,"查询失败"));
			// TODO: handle exception
		}		
	}
	
	/**
	 * 待办事项
	 * @param request
	 * @param response
	 */
	@RequestMapping("/todoList")
	public void todoList(EasyUIPageUtil easyUIPage,HttpServletRequest request, HttpServletResponse response){
		try {
			UserEntity user = (UserEntity)request.getSession().getAttribute(SessionConstant.USER);
			//根据用户获取任务
			List<Task> taskList = taskService.createTaskQuery().active().
									includeProcessVariables().orderByTaskCreateTime().
									taskAssignee(user.getUserId() + "").desc().list();
									//listPage((easyUIPage.getPage() - 1) * easyUIPage.getRows(), easyUIPage.getPage() * easyUIPage.getRows());
			//根据角色获取任务
			List<Task> _taskList = taskService.createTaskQuery().active().
					includeProcessVariables().orderByTaskCreateTime().
					taskCandidateGroup(user.getUserRoleId() + "").desc().list();
					//listPage((easyUIPage.getPage() - 1) * easyUIPage.getRows(), easyUIPage.getPage() * easyUIPage.getRows());
			if(taskList.size() == 0){
				taskList = new ArrayList<>();
			}
			taskList.addAll(_taskList);

			List mapList = new ArrayList<>();
			int start = (easyUIPage.getPage() - 1) * easyUIPage.getRows();
			for(int i = start ; i < taskList.size() && i < start + easyUIPage.getRows();i++){
				Task _task  = taskList.get(i);
				Map map = new HashMap<>();
				map.put("taskId", _task.getId());
				map.put("taskName", _task.getName());
				map.put("taskDefinitionKey", _task.getTaskDefinitionKey());
				map.put("createTime", _task.getCreateTime());
				map.put("description", _task.getDescription());
				map.put("processVariables", _task.getProcessVariables());
				map.put("taskLocalVariables", _task.getTaskLocalVariables());
				map.put("taskFormKey", _task.getFormKey());
				map.put("pocessInstanceId", _task.getProcessInstanceId());
				mapList.add(map);
			}
			this.outJson(response, new EasyUIBean(mapList, (int)taskList.size()), "yyyy-MM-dd");
		} catch (Exception e) {
			this.outJson(response, new ResultBean(true,"查询失败"));
			e.printStackTrace();
			// TODO: handle exception
		}		
	}

	/**
	 * 已处理事件
	 * @param request
	 * @param response
	 */
	@RequestMapping("/alreadySolve")
	public void alreadySolve(EasyUIPageUtil easyUIPage,HttpServletRequest request, HttpServletResponse response){
		try {
			UserEntity user = (UserEntity)request.getSession().getAttribute(SessionConstant.USER);
			List<HistoricTaskInstance> taskList = historyService.createHistoricTaskInstanceQuery().includeProcessVariables().
													taskAssignee(user.getUserId() + "").list();
			List<HistoricTaskInstance> _taskList = historyService.createHistoricTaskInstanceQuery().includeProcessVariables().
													taskCandidateGroup(user.getUserRoleId() + "").list();

			if(taskList.size() == 0){
				taskList = new ArrayList<>();
			}
			taskList.addAll(_taskList);

			List mapList = new ArrayList<>();
			int start = (easyUIPage.getPage() - 1) * easyUIPage.getRows();
			for(int i = start ; i < taskList.size() && i < start + easyUIPage.getRows();i++){
				HistoricTaskInstance _task  = taskList.get(i);
				Map map = new HashMap<>();
				map.put("taskId", _task.getId());
				map.put("taskName", _task.getName());
				map.put("taskDefinitionKey", _task.getTaskDefinitionKey());
				map.put("createTime", _task.getCreateTime());
				map.put("endTime", _task.getEndTime());
				map.put("description", _task.getDescription());
				map.put("processVariables", _task.getProcessVariables());
				map.put("taskLocalVariables", _task.getTaskLocalVariables());
				map.put("taskFormKey", _task.getFormKey());
				map.put("pocessInstanceId", _task.getProcessInstanceId());
				mapList.add(map);
			}
			this.outJson(response, new EasyUIBean(mapList, (int)taskList.size()), "yyyy-MM-dd");
		} catch (Exception e) {
			this.outJson(response, new ResultBean(true,"查询失败"));
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	/**
	 * 完成任务
	 * @param taskId
	 * @param map
	 * @param request
	 * @param response
	 */
	@RequestMapping("/completeTask")
	public void completeTask(String taskId,String procInstId,String comment, String map,HttpServletRequest request, HttpServletResponse response){
		try {
			Map<String,Object> mapObj = JSONObject.parseObject(map,Map.class);
			UserEntity user = (UserEntity)request.getSession().getAttribute(SessionConstant.USER);
			mapObj.put("userId",user.getUserId());
			mapObj.put("userName",user.getUserName());
			if(StringUtils.isBlank(comment)){
				comment = "";
			}
			taskService.setVariablesLocal(taskId,mapObj);
			taskService.addComment(taskId, procInstId, comment);
			taskService.complete(taskId, mapObj);
			this.outJson(response, new ResultBean(true, "任务完成"));
		} catch (Exception e) {
			e.printStackTrace();
			this.outJson(response, new ResultBean(false, "发生错误"));
		}		
	}
	
	/**
	 * 查看当前运行的节点
	 * @param procInstId
	 * @param response
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/viewer")
	public String viewer(String procInstId, HttpServletResponse response, HttpServletRequest request) {		
		try {
			String procDefId = "";
			ProcessInstance processInstance = (ProcessInstance) this.runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
			if(processInstance == null){
				HistoricProcessInstance HisInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(procInstId).singleResult();
				procDefId = HisInstance.getProcessDefinitionId();
			}else{
				procDefId = processInstance.getProcessDefinitionId();
			}
			HistoricProcessInstance hpi = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(procInstId).singleResult();
			return "redirect:/process-editor/diagram-viewer/?processDefinitionId=" + hpi.getProcessDefinitionId()
			+ "&processInstanceId=" + procInstId;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			this.outJson(response, new ResultBean(false, "发生错误"));
			return "";
		}		
	}
	
	/**
	 * 查询流程历史信息
	 * @param procInstId
	 * @param response
	 * @param request
	 */
	@RequestMapping("/history")
	public void history(String procInstId, HttpServletResponse response, HttpServletRequest request){
		List<HistoricActivityInstance> list =  ((HistoricActivityInstanceQuery) ((HistoricActivityInstanceQuery) historyService.createHistoricActivityInstanceQuery().processInstanceId(procInstId)
					.orderByHistoricActivityInstanceStartTime().asc()).orderByHistoricActivityInstanceEndTime()
					.asc()).list();
		List mapList = new ArrayList<>();
		List<HistoricVariableInstance> var = historyService.createHistoricVariableInstanceQuery()
				.processInstanceId(procInstId).list();
		Map _map = new HashMap();

		for (int i = 0; i < var.size(); i++) {
			HistoricVariableInstance v = var.get(i);
			_map.put(v.getVariableName(), v.getValue());
		}
		for(HistoricActivityInstance histIns : list){

			Map  map = new HashMap();
			if(StringUtils.isBlank(histIns.getActivityName())){
				continue;
			}
			if (StringUtils.isNotBlank(histIns.getActivityName()) && StringUtils.isBlank(histIns.getTaskId())){
				map.put("startTime", histIns.getStartTime());
				map.put("endTime", histIns.getEndTime());
				map.put("taskName", histIns.getActivityName());
				map.put("variables",_map);
				mapList.add(map);
				continue;
			}
			HistoricTaskInstance hisTask = historyService.createHistoricTaskInstanceQuery().taskId(histIns.getTaskId()).singleResult();

			map.put("taskId", histIns.getTaskId());
			map.put("taskName", histIns.getActivityName());
			map.put("startTime", histIns.getStartTime());
			map.put("endTime", histIns.getEndTime());
			map.put("processDefinitionId", histIns.getProcessDefinitionId());
			map.put("processInstanceId", histIns.getProcessInstanceId());
			map.put("assignee", histIns.getAssignee());
			map.put("comment", taskService.getTaskComments(histIns.getTaskId()));
			histIns.getExecutionId();
			//查询每个任务节点的参数
			List<HistoricVariableInstance> taskVariables = historyService.createHistoricVariableInstanceQuery().taskId(histIns.getTaskId()).list();
			Map taskVarMap = new HashMap();
			for (int i = 0; i < taskVariables.size(); i++) {
				HistoricVariableInstance v = taskVariables.get(i);
				taskVarMap.put(v.getVariableName(), v.getValue());
			}
			map.put("variables",taskVarMap);

			mapList.add(map);
		}
		this.outJson(response, new EasyUIBean(mapList, (int) mapList.size()), "yyyy-MM-dd HH:mm:ss");
	}
}
