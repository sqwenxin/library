package com.sq.book.activity.action;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sq.book.commonBean.ReturnMessageBean;
import com.sq.book.commonUtils.EasyUIJson;
import com.sq.book.commonUtils.EasyUIPageUtil;
import com.sq.book.commonUtils.ResultUtils;

@Controller
@RequestMapping("/process")
public class ProcessAction {

	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private RuntimeService runtimeService;

	/**
	 * �����б�ҳ��
	 * 
	 * @return
	 */
	@RequestMapping("/processPage")
	public String processPage() {
		return "process/process";
	}

	/**
	 * �鿴�����б�
	 * 
	 * @param easyUIPage
	 * @param request
	 * @param response
	 */
	@RequestMapping("/processList")
	public void query(EasyUIPageUtil easyUIPage, HttpServletRequest request, HttpServletResponse response) {
		ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
		int count = (int) processDefinitionQuery.count();
		List<ProcessDefinition> list = processDefinitionQuery.listPage(
				(easyUIPage.getPage() - 1) * easyUIPage.getRows(), easyUIPage.getPage() * easyUIPage.getRows());
		List _list = new ArrayList();
		for (ProcessDefinition processDefinition : list) {
			//查询流程部署信息
			Deployment deployment =  repositoryService.createDeploymentQuery().deploymentId(processDefinition.getDeploymentId()).singleResult();
			Map _map = new LinkedHashMap();
			_map.put("id", processDefinition.getId());
			_map.put("key", processDefinition.getKey());
			_map.put("name", processDefinition.getResourceName().split("\\.")[0]);
			_map.put("deploymentId", processDefinition.getDeploymentId());
			_map.put("version", Integer.valueOf(processDefinition.getVersion()));
			_map.put("xml", processDefinition.getResourceName());
			_map.put("png", processDefinition.getDiagramResourceName());
			_map.put("date", deployment.getDeploymentTime());			
			_list.add(_map);
		}
		ResultUtils.outJson(response, new EasyUIJson(_list, count));
	}

	/**
	 * 流程激活与挂起
	 * 
	 * @param state
	 * @param procDefId
	 * @param response
	 */
	@RequestMapping({ "update/{state}" })
	@ResponseBody
	public void updateState(@PathVariable("state") String state, String procDefId, HttpServletResponse response) {
		if (state.equals("active")) {
			try {
				this.repositoryService.activateProcessDefinitionById(procDefId, true, null);
			} catch (ActivitiException e) {
				e.printStackTrace();
				ResultUtils.outJson(response, new ReturnMessageBean(false ,"激活失败"));
				return;
			}
		}
		if (state.equals("suspend")) {// 挂起
			try {
				this.repositoryService.suspendProcessDefinitionById(procDefId, true, null);
			} catch (ActivitiException e) {
				e.printStackTrace();
				ResultUtils.outJson(response, new ReturnMessageBean(false ,"挂起失败"));
				return;
			}
		}
		ResultUtils.outJson(response, new ReturnMessageBean(true ,"操作成功"));
	}

	/**
	 * 删除流程
	 * 
	 * @param deploymentId
	 * @param response
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public void delete(String deploymentId, HttpServletResponse response) {
		try {
			this.repositoryService.deleteDeployment(deploymentId, true);
			ResultUtils.outJson(response, new ReturnMessageBean(true ,"删除成功"));
		} catch (Exception e) {
			e.printStackTrace();
			ResultUtils.outJson(response, new ReturnMessageBean(false ,"删除失败"));
			// TODO: handle exception
		}
	}

	/**
	 * 转化为模型
	 * 
	 * @param procDefId
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws XMLStreamException
	 */
	@RequestMapping("/toModel")
	public void convertToModel(String procDefId ,HttpServletResponse response) throws UnsupportedEncodingException, XMLStreamException {
		try {
			ResultUtils.outJson(response, new ReturnMessageBean(true, "转化成功", convertToModel(procDefId, null, null)));
		} catch (Exception e) {
			e.printStackTrace();
			ResultUtils.outJson(response, new ReturnMessageBean(true, "转化失败"));
			// TODO: handle exception
		}		
	}

	/**
	 * 流程转化为模型
	 * 
	 * @param procDefId
	 * @param key
	 * @param name
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws XMLStreamException
	 */
	public Model convertToModel(String procDefId, String key, String name)throws UnsupportedEncodingException, XMLStreamException {
		ProcessDefinition processDefinition = (ProcessDefinition) this.repositoryService.createProcessDefinitionQuery().processDefinitionId(procDefId).singleResult();
		InputStream bpmnStream = this.repositoryService.getResourceAsStream(processDefinition.getDeploymentId(),
		processDefinition.getResourceName());
		XMLInputFactory xif = XMLInputFactory.newInstance();
		InputStreamReader in = new InputStreamReader(bpmnStream, "UTF-8");
		XMLStreamReader xtr = xif.createXMLStreamReader(in);
		BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xtr);
		BpmnJsonConverter converter = new BpmnJsonConverter();
		ObjectNode modelNode = converter.convertToJson(bpmnModel);
		Model modelData = this.repositoryService.newModel();
		if ((!(StringUtils.isBlank(key))) && (!(StringUtils.isBlank(name)))) {
			modelData.setKey(key);
			modelData.setName(name);
		} else {
			modelData.setKey(processDefinition.getKey());
			modelData.setName(processDefinition.getName());
			modelData.setCategory(processDefinition.getCategory());
			modelData.setDeploymentId(processDefinition.getDeploymentId());
			modelData.setVersion(Integer.valueOf(Integer.parseInt(String
					.valueOf(this.repositoryService.createModelQuery().modelKey(modelData.getKey()).count() + 1L))));
		}
		ObjectNode modelObjectNode = new ObjectMapper().createObjectNode();
		modelObjectNode.put("name", processDefinition.getName());
		modelObjectNode.put("revision", modelData.getVersion());
		modelObjectNode.put("description", processDefinition.getDescription());
		modelData.setMetaInfo(modelObjectNode.toString());
		this.repositoryService.saveModel(modelData);
		this.repositoryService.addModelEditorSource(modelData.getId(), modelNode.toString().getBytes("utf-8"));
		return modelData;
	}

	/**
	 * 查看流程资源
	 * 
	 * @param procDefId
	 * @param proInsId
	 * @param resType
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping({ "resource/read" })
	public void resourceRead(String procDefId, String proInsId, String resType, HttpServletResponse response)
			throws Exception {
		InputStream resourceAsStream = this.resourceRead(procDefId, proInsId, resType);
		//�ļ������Ʋ����
		byte[] b = new byte[1024];
		int len = -1;
		while ((len = resourceAsStream.read(b, 0, 1024)) != -1){
			response.getOutputStream().write(b, 0, len);
		}			
	}
	
	/**
	 * �������Id��ȡͼƬ������xml�����
	 * @param procDefId
	 * @param proInsId
	 * @param resType Ϊ xml��image
	 * @return 
	 * @throws Exception
	 */
	public InputStream resourceRead(String procDefId, String proInsId, String resType) throws Exception {
		if (StringUtils.isBlank(procDefId)) {
			ProcessInstance processInstance = (ProcessInstance) this.runtimeService.createProcessInstanceQuery().processInstanceId(proInsId).singleResult();
			procDefId = processInstance.getProcessDefinitionId();
		}
		ProcessDefinition processDefinition = (ProcessDefinition) this.repositoryService.createProcessDefinitionQuery().processDefinitionId(procDefId).singleResult();
		String resourceName = "";
		if (resType.equals("image"))
			resourceName = processDefinition.getDiagramResourceName();
		else if (resType.equals("xml")) {
			resourceName = processDefinition.getResourceName();
		}
		InputStream resourceAsStream = this.repositoryService.getResourceAsStream(processDefinition.getDeploymentId(),resourceName);
		return resourceAsStream;
	}
}
