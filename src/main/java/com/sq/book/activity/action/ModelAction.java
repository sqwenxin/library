package com.sq.book.activity.action;


import java.io.ByteArrayInputStream;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ModelQuery;
import org.activiti.engine.repository.ProcessDefinition;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sq.book.commonUtils.EasyUIJson;
import com.sq.book.commonUtils.EasyUIPageUtil;
import com.sq.book.commonUtils.ResultUtils;

@Controller("ACModel")
@RequestMapping("/model")

public class ModelAction {
	@Autowired
	private ProcessEngine processEngine;
	@Autowired
	private RepositoryService repositoryService;
	
	/**
	 * 模型页面
	 * @return
	 */
	@RequestMapping("/modelPage")
	
	public String modelPage(){
		return "model/model";
	}
	
	/**
	 *	流程模型列表
	 */
	@RequestMapping("/modelList")
	public void queryModelList( EasyUIPageUtil easyUIPage,HttpServletRequest request, HttpServletResponse response){
		repositoryService.createNativeModelQuery();
		ModelQuery modelQuery =  repositoryService.createModelQuery();
		int count = (int) modelQuery.count();
		List list = new ArrayList();
		if(count != 0){
			list = modelQuery.listPage((easyUIPage.getPage() - 1) * easyUIPage.getRows(), easyUIPage.getPage() * easyUIPage.getRows());
		}				
		ResultUtils.outJson(response, new EasyUIJson(list, count));
	}
	
	/**
	 * 部署模型
	 * @param request
	 * @param response
	 */
	@RequestMapping("/deploymentModel")
	public void deploymentModel(@RequestParam("id")String id,HttpServletRequest request, HttpServletResponse response){
		String _id = "";
		try {
			Model modelData = this.repositoryService.getModel(id);
			BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
			JsonNode editorNode = new ObjectMapper().readTree(this.repositoryService.getModelEditorSource(modelData.getId()));
			BpmnModel bpmnModel = jsonConverter.convertToBpmnModel(editorNode);
			BpmnXMLConverter xmlConverter = new BpmnXMLConverter();
			byte[] bpmnBytes = xmlConverter.convertToXML(bpmnModel);

			String processName = modelData.getName();
			if (!(StringUtils.endsWith(processName, ".bpmn20.xml"))) {
				processName = processName + ".bpmn20.xml";
			}
			ByteArrayInputStream in = new ByteArrayInputStream(bpmnBytes);
			Deployment deployment = this.repositoryService.createDeployment().name(modelData.getName()).addInputStream(processName, in).deploy();

			List<ProcessDefinition> list = this.repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).list();
			for (ProcessDefinition processDefinition : list) {
				this.repositoryService.setProcessDefinitionCategory(processDefinition.getId(), modelData.getCategory());
				_id = processDefinition.getId();
			}
			ResultUtils.outJson(response, true);
		} catch (Exception e) {
			ResultUtils.outJson(response, "设计模型图不正确，检查模型正确性，模型ID=" + id);
			throw new ActivitiException("设计模型图不正确，检查模型正确性，模型ID=" + id, e);	
		}
	}
	
	/**
	 * 删除模型
	 */
	@RequestMapping("/deleteModel")
	public void deleteMdoel(@RequestParam("id")String id,HttpServletRequest request, HttpServletResponse response){
		try {
			repositoryService.deleteModel(id);
			ResultUtils.outJson(response, true);
		} catch (Exception e) {
			ResultUtils.outJson(response, "模型删除失败，模型ID=" + id);
			throw new ActivitiException("模型删除失败，模型ID=" + id, e);	
			// TODO: handle exception
		}
		
	}
}
