package com.sq.book.base.aop;


import com.sq.book.base.constant.SessionConstant;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import com.sq.book.management.entity.UserEntity;
import javax.servlet.http.HttpServletRequest;
import com.sq.book.base.util.SpringUtil;
import com.sq.book.base.entity.BaseEntity;

public class BaseAop {
	
	protected Logger logger = Logger.getLogger(getClass());
	
	/**
	 * 获取session中的用户实体
	 * <p>若没有用户，返回null</p>
	 */
	protected UserEntity getUserBySession() {
		HttpServletRequest request = SpringUtil.getRequest();
		if (request == null) {
			return new UserEntity();
		}
		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionConstant.USER);
		if (user == null) {
			return new UserEntity();
		}
		return user;
	}
	
	/**
	 * 获取方法上的实体数据
	 * @param joinPoint
	 * @return
	 */
	protected BaseEntity getRequestEntity(JoinPoint joinPoint) {
		Object[] params = joinPoint.getArgs();
		BaseEntity base = null;
		for (Object param : params) {
			if (param instanceof BaseEntity) {
				base = (BaseEntity) param;
			}
		}
		return base;
	}
	
}
