package com.sq.book.base.aop;


import com.sq.book.base.entity.BaseEntity;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 保存更新切面类
 * 保存更新时写入更新人更新时间
 * @author 孙乾
 *
 */
@Aspect
@Component
public class SaveOrUpdateAop extends BaseAop {

	@Pointcut("execution(* com.sq.book.**.biz..*Impl.saveEntity(..))")
	public void saveEntity() {}

	/**
	 * 
	 * @param joinPoint
	 * @throws Throwable
	 */
	@Before("saveEntity()")
	public void saveEntity(JoinPoint joinPoint) throws Throwable {
		Object[] params = joinPoint.getArgs();
		for (Object param : params) {
			if (param instanceof BaseEntity) {//保存实体方法
				BaseEntity base = (BaseEntity)param;
				base.setCreateBy(getUserBySession().getUserId());
				base.setCreateDate(new Date());
			} else if (param instanceof List) {//批量保存实体方法
				List<BaseEntity> list = (List<BaseEntity>)param;
				for (BaseEntity base : list) {
					base.setCreateBy(getUserBySession().getUserId());
					base.setCreateDate(new Date());
				}
			}
		}
	}
	
	@Pointcut("execution(* com.sq.book.**.biz..*Impl.updateEntity(..))")
	public void updateEntity() {}
	
	@Before("updateEntity()")
	public void updateEntity(JoinPoint joinPoint) throws Throwable {
		Object[] params = joinPoint.getArgs();
		for (Object param : params) {
			if (param instanceof BaseEntity) {
				BaseEntity base = (BaseEntity)param;
				base.setUpdateBy(getUserBySession().getUserId());
				base.setUpdateDate(new Date());
			}
		}
	}
}
