package com.sq.book.base.security;

import com.sq.book.management.biz.IRoleModelBiz;
import com.sq.book.management.biz.IUserBiz;
import com.sq.book.management.entity.RoleModelEntity;
import com.sq.book.management.entity.UserEntity;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import com.sq.book.base.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ShiroRealm extends AuthorizingRealm {

	@Autowired
	private IUserBiz userBiz;

	@Autowired
	private IRoleModelBiz roleModelBiz;
	
	
	/**
	 * 认证方法
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken upToken = (UsernamePasswordToken) token;
		
		Object principal = upToken.getUsername();//账号
		Object credentials = StringUtil.MD5(String.valueOf(upToken.getPassword()));//用户实际的密码（经过加密），将和前端提交密码匹配验证
		String realmName = getName();
		
		//ByteSource byteSource = ByteSource.Util.bytes("qxb"); //盐�?�加�?
		
		SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(principal, credentials, realmName);
		return info;
	}

	/**
	 * 授权方法
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		
		String userName = principal.toString();
		UserEntity user = new UserEntity();
        user.setUserCode(userName);
		UserEntity userEntity = (UserEntity)userBiz.query(user).get(0);

		List<RoleModelEntity> rmEntityList = roleModelBiz.query(new RoleModelEntity(userEntity.getUserRoleId()));

		for (RoleModelEntity rmEntity : rmEntityList) {
			info.addStringPermission(rmEntity.getModel().getModelCode());
		}

		return info;
	}

}
