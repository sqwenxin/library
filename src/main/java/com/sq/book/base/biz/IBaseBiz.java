package com.sq.book.base.biz;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sq.book.base.entity.BaseEntity;

public interface IBaseBiz {

	/**
	 * 根据ID查询实体信息
	 * @param id 实体ID
	 * @return 返回实体
	 */
	BaseEntity getEntity(int id);
	
	/**
	 * 根据实体获取实体
	 * @param baseEntity 实体
	 * @return 返回实体
	 */
	BaseEntity getEntity(BaseEntity baseEntity);
	
	/**
	 * 查询�?�?
	 * @return 实体集合
	 */
	List queryAll();
	
	/**
	 * 根据实体查询实体集合
	 * @param baseEntity 实体
	 * @return 实体集合
	 */
	List query(BaseEntity baseEntity);
	
	/**
	 * 保存实体
	 * @param baseEntity �?保存实体
	 * @return 实体主键�?
	 */
	int saveEntity(BaseEntity baseEntity);
	
	/**
	 * 批量保存
	 * @param list 实体集合
	 */
	void saveEntity(List list);
	
	/**
	 * 更新实体
	 * @param baseEntity 实体
	 */
	void updateEntity(BaseEntity baseEntity);
	

	
	/**
	 * 根据ids删除实体
	 * @param ids
	 */
	void deleteEntity(String[] ids);
	
	/**
	 * 根据id删除实体
	 * 底层未实现此Dao
	 * @param id
	 */
	void deleteEntity(int id);
	
	/**
	 * 根据实体删除实体
	 * @param entity
	 */
	void deleteEntity(BaseEntity entity);
	
	/**
	 * 导入执行SQL
	 * @param sql
	 * @return
	 */
	Object excuteSql(@Param("sql") String sql);
	
	/**
	 * 执行查询SQL并且返回结果�?
	 * @param sql
	 * @return
	 */
	List createSQLQuery(@Param("sql") String sql);
}
