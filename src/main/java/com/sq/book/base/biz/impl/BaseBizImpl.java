package com.sq.book.base.biz.impl;

import java.util.List;

import com.sq.book.base.biz.IBaseBiz;
import com.sq.book.base.dao.IBaseDao;
import com.sq.book.base.entity.BaseEntity;

public abstract class BaseBizImpl implements IBaseBiz{

	/**
	 * 该方法由子类去实现
	 * @return 子类的实例Dao
	 */
	protected abstract IBaseDao getDao();

	/**
	 * 根据ID查询实体信息
	 * @param id 实体ID
	 * @return 返回实体
	 */
	public BaseEntity getEntity(int id) {
		return getDao().getById(id);
	}

	/**
	 * 根据实体获取实体
	 * @param baseEntity 实体
	 * @return 返回实体
	 */
	public BaseEntity getEntity(BaseEntity baseEntity) {
		return getDao().getEntity(baseEntity);
	}

	/**
	 * 查询所有
	 * @return 实体集合
	 */
	public List queryAll() {
		return getDao().queryAll();
	}

	/**
	 * 根据实体查询实体集合
	 * @param baseEntity 实体
	 * @return 实体集合
	 */
	public List<BaseEntity> query(BaseEntity baseEntity) {
		return getDao().query(baseEntity);
	}

	/**
	 * 保存实体
	 * @param baseEntity 需保存实体
	 * @return 实体主键值
	 */
	public int saveEntity(BaseEntity baseEntity) {
		return getDao().saveEntity(baseEntity);
	}

	/**
	 * 批量保存
	 * @param list 实体集合
	 */
	public void saveEntity(List list) {
		if (list.size() > 0) {
			getDao().saveBatch(list);
		}
	}

	/**
	 * 更新实体
	 * @param baseEntity 实体
	 */
	public void updateEntity(BaseEntity baseEntity) {
		getDao().updateEntity(baseEntity);
	}
	


	/**
	 * 根据ids删除实体
	 * @param ids
	 */
	public void deleteEntity(String[] idArray) {
		if (idArray.length == 0) {
			return;
		}
		getDao().deleteByIds(idArray);
	}

	/**
	 * 根据id删除实体
	 * <b>底层并未实现deleteById的Dao<b>
	 * @param id
	 */
	public void deleteEntity(int id) {
		getDao().deleteById(id);
	}

	/**
	 * 根据实体删除实体
	 * @param entity
	 */
	public void deleteEntity(BaseEntity baseEntity) {
		getDao().deleteEntity(baseEntity);
	}


	/**
	 * 导入执行SQL
	 * @param sql
	 */
	public Object excuteSql(String sql) {
		return getDao().excuteSql(sql);
	}
	
	
	
	/**
	 * 执行查询SQL并且返回结果集
	 * @param sql
	 */
	public List createSQLQuery(String sql) {
		return getDao().createSQLQuery(sql);
	}
}
