package com.sq.book.base.constant;

public class SessionConstant {

	/**
	 * 用户key
	 */
	public final static String USER = "user";
	
	/**
	 * 公司key
	 */
	public final static String COMPANY = "company";
	
	/**
	 * 项目名称key
	 */
	public final static String BASE = "base";
	
	/**
	 * 项目名称key
	 */
	public final static String BASEPATH = "basePath";
	
	/**
	 * 数据权限相应的查询语�?
	 */
	public final static String DATASCORPSQL = "dataScorpSql";
	
	/**
	 * 批量导入数据的数据权�?
	 */
	public final static String DEPARTMENTDATASCORP = "departmentScorpIds";

	public final static String IMAGEPATH = "imagePath";
}
