package com.sq.book.base.util;

import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

public class PageUtil {

	public static void startPage() {
		int pageNum = RequestUtil.getParameter("page", 1);
		int pageSize = RequestUtil.getParameter("rows", 10);
		
		PageHelper.startPage(pageNum, pageSize);//pageNum:页码	pageSize：页尺寸
	}
	
	public static PageInfo endPage(List list) {
		PageInfo page = new PageInfo(list);
		return page;
	}
}
