package com.sq.book.base.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class SpringUtil {

	/**
	 * 获取当前客户端HttpServletRequest对象
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		try {
			HttpServletRequest request = 
					((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
			return request;
		} catch (Exception e) {
			//e.printStackTrace();
			return null;
		}
	}

	public SpringUtil() {
	}

	public static Object getBean(ServletContext sc, String beanName) {
		return WebApplicationContextUtils.getWebApplicationContext(sc).getBean(beanName);
	}

	public static Object getBean(String beanName) {
		return WebApplicationContextUtils.getWebApplicationContext(getRequest().getServletContext()).getBean(beanName);
	}

	public static Object getBean(Class cls) {
		return WebApplicationContextUtils.getWebApplicationContext(getRequest().getServletContext()).getBean(cls);
	}
}
