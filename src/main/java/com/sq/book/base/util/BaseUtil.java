package com.sq.book.base.util;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

public class BaseUtil {

	private static final String Attachments_Path = "D:\\Attachments";

	public static String getRealPath(String filePath) {
		// HttpServletRequest request = SpringUtil.getRequest();

		String path = Attachments_Path;// 该方法存在平台因素，存在有时后面有斜杠有时会没有
		String last = path.charAt(path.length() - 1) + ""; // 取最后一个字符串，判断是否存在斜�?
		String frist = filePath.charAt(0) + ""; // 取第�?个字符串，判断用户传递过来的参数是否有斜�?
		if (last.equals(File.separator)) {
			if (frist.equals("\\") || frist.equals("/")) {// 当前平台可以获取到最后的斜杠，那么就判断�?下用户传递的参数是否有斜杠，有就截取�?
				path = path + filePath.substring(1);
			} else {
				path = path + filePath; // 当前平台可以获取到最后的斜杠，用户传递的参数没有斜杠，就直接返回
			}
		} else {
			if (frist.equals("\\") || frist.equals("/")) {// 当前平台没有斜杠，用户传递的参数有斜杠，有就放回
				path = path + filePath;
			} else {
				path = path + File.separator + filePath; // 平台也米有斜杠，参数也没有斜杠，增加拼接放回
			}
		}
		return path;
	}

	public static String getRealPath() {
		HttpServletRequest request = SpringUtil.getRequest();
		String realPath = request.getSession().getServletContext().getRealPath("");
		return realPath;
	}

	/**
	 * 根据pattern格式获取日期字符串的Date类型数据
	 * 
	 * @param pattern
	 * @param dateStr
	 * @return
	 */
	public static Date parseDate(String pattern, String dateStr) {
		if (StringUtils.isBlank(pattern) || StringUtils.isBlank(dateStr)) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = format.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 获取字符串的Double类型数据
	 * 
	 * @param str
	 * @return
	 */
	public static Double parseDouble(String str) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		return Double.parseDouble(str);
	}

	/**
	 * 获取字符串的Integer类型数据
	 * 
	 * @param str
	 * @return
	 */
	public static Integer parseInt(String str) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		return Integer.parseInt(str);
	}

	/**
	 * 四舍五入，保留newScale位数浮点�?
	 * 
	 * @param decimal
	 * @param newScale
	 * @return
	 */
	public static Double parseRoundHalf(Double decimal, int newScale) {
		return new BigDecimal(decimal).setScale(newScale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * 获取session中的用户实体
	 * <p>
	 * 若没有用户，返回null
	 * </p>
	 */
//	public static UserEntity getUserBySession() {
//		HttpServletRequest request = SpringUtil.getRequest();
//		if (request == null) {
//			return new UserEntity();
//		}
//		UserEntity user = (UserEntity) request.getSession().getAttribute(SessionConstant.USER);
//		if (user == null) {
//			return new UserEntity();
//		}
//		return user;
//	}

	/**
	 * 通过时间秒毫秒数判断两个时间的间�?
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int differentDaysByMillisecond(Date date1, Date date2) {
		int days = (int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
		return days;
	}
	
	/**
	 * 设置session
	 * @param name
	 * @param value
	 */
	public static void setSession(String name, Object value) {
		HttpServletRequest request = SpringUtil.getRequest();
		if (request == null) {
			return;
		}
		
		HttpSession session = request.getSession();
		
		Object value_old = session.getAttribute(name);
		
		if (value_old != null) {
			return;
		}
		
		session.setAttribute(name, value);
	}
}
