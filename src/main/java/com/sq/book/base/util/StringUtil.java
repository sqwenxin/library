package com.sq.book.base.util;

import org.apache.shiro.crypto.hash.SimpleHash;

public class StringUtil {

	/**
	 * 字符串MD5加密
	 * @param str
	 * @return
	 */
	public static String MD5(String str) {
		SimpleHash result = new SimpleHash("MD5", str, null, 1);
		// System.out.println(result);
		return result.toString();
	}

	/**
	 * 字符串首字母大写
	 * @param str
	 * @return
	 */
	public static String upperCase(String str) {
		char[] ch = str.toCharArray();

		if (ch[0] >= 'a' && ch[0] <= 'z') {
			ch[0] -= 32;
		}
		return new String(ch);
	}
}
