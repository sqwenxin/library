package com.sq.book.base.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

/**
 * 上传工具工具�?
 * 
 * @author sq
 *
 */
public class UpLoadUtil {
	/**
	 * 上传文件
	 * 
	 * @param excelFileName
	 * @return
	 * @throws IOException
	 */
	public static String Fileupload(File excel, String filePath, String excelFileName) {
		try {
			String path = "";
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
					.getRequest();
			;
			String realPath = request.getSession().getServletContext().getRealPath("/");
			;
			File file = new File(realPath + filePath);
			// 判断文件夹是否存在，不存在就创建问价
			if (!file.exists() && !file.isDirectory()) {
				file.mkdir();
			}
			File savefile = new File(new File(realPath), excelFileName);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			FileUtils.copyFile(excel, savefile);
			// 删除临时文件
			excel.delete();
			path = realPath + filePath + "\\" + excelFileName;
			return path;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
	}

	/**
	 * 文件上传
	 * 
	 * @param excel
	 * @param filePath
	 * @param excelFileName
	 * @return
	 */
	public static String fileUpload(MultipartFile file, String filePath) {
		try {
			String fileName = file.getOriginalFilename();
			String newFileName = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + fileName;
			File dir = new File(filePath, newFileName);
			if (!dir.exists() && !dir.isDirectory()) {
				dir.mkdir();
			}
			File savefile = new File(new File(filePath), newFileName);
			if (!savefile.getParentFile().exists()) {
				savefile.getParentFile().mkdirs();
			}
			// 如果文件存在先删除原来的文件
			if (savefile.exists()) {
				savefile.delete();
			}
			FileUtils.copyInputStreamToFile(file.getInputStream(), savefile);
			return newFileName;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
	}
}
