package com.sq.book.base.util;

import org.springframework.util.StringUtils;

public class RequestUtil {
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public static Integer getParameter(String param) {
		return getParameter(param, 10);
	}

	public static Integer getParameter(String parm, Integer def) {
		String value = SpringUtil.getRequest().getParameter(parm);
		if (StringUtils.hasText(value)) {
			return Integer.parseInt(value);
		} else {
			return def;
		}
	}
}
