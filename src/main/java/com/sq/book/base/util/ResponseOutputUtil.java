package com.sq.book.base.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

public class ResponseOutputUtil {

	/**
	 * 输出json格式的object
	 * @param response HttpServletResponse对象，请求响应对�?
	 * @param object �?要json序列化的数据
	 */
	public static void outJson(HttpServletResponse response, Object object) {
		PrintWriter out = null;
		response.setContentType("application/json;charset=utf-8");
		try {
			out = response.getWriter();
			String jsonString = JSON.toJSONString(object);
			out.println(jsonString);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 以dateFormat格式格式化时间，输出json格式的object数据
	 * @param response HttpServletResponse对象，请求响应对�?
	 * @param object �?要json序列化的数据
	 * @param dateFormat 日期格式化字符串
	 */
	public static void outJson(HttpServletResponse response, Object object, String dateFormat) {
		PrintWriter out = null;
		response.setContentType("application/json;charset=utf-8");
		try {
			out = response.getWriter();
			String jsonString = JSON.toJSONStringWithDateFormat(object, dateFormat);
			out.println(jsonString);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 输出object字符�?
	 * @param response HttpServletResponse对象
	 * @param object 字符�?
	 */
	public static void outString(HttpServletResponse response, Object object) {
		PrintWriter out = null;
		response.setContentType("text/plain;charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");
		try {
			out = response.getWriter();
			out.println(object);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
