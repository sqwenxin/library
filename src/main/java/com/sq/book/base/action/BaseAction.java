package com.sq.book.base.action;

import com.alibaba.fastjson.JSON;
import com.sq.book.base.constant.SessionConstant;
import com.sq.book.base.util.SpringUtil;
import com.sq.book.management.entity.UserEntity;
import org.apache.log4j.Logger;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class BaseAction {
	
	protected Logger logger = Logger.getLogger(getClass());
	
	/**
	 * 淇敼spring mvc浼犺緭鏁扮粍�?垮害闄愬埗涓�?1024
	 * @param binder
	 */
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setAutoGrowCollectionLimit(1024);
    }

	/**
	 * 杈撳嚭json鏍煎紡鐨刼bject
	 * @param response HttpServletResponse瀵硅薄锛岃姹傚搷搴斿璞�
	 * @param object 闇�瑕乯son搴忓垪鍖栫殑鏁版�?
	 */
	protected void outJson(HttpServletResponse response, Object object) {
		PrintWriter out = null;
		response.setContentType("application/json;charset=utf-8");
		try {
			out = response.getWriter();
			String jsonString = JSON.toJSONString(object);
			out.println(jsonString);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 浠ateFormat鏍煎紡鏍煎紡鍖栨椂闂达紝杈撳嚭json鏍煎紡鐨刼bject鏁版�?
	 * @param response HttpServletResponse瀵硅薄锛岃姹傚搷搴斿璞�
	 * @param object 闇�瑕乯son搴忓垪鍖栫殑鏁版�?
	 * @param dateFormat 鏃ユ湡鏍煎紡鍖栧瓧绗︿覆
	 */
	protected void outJson(HttpServletResponse response, Object object, String dateFormat) {
		PrintWriter out = null;
		response.setContentType("application/json;charset=utf-8");
		try {
			out = response.getWriter();
			String jsonString = JSON.toJSONStringWithDateFormat(object, dateFormat);
			out.println(jsonString);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 杈撳嚭object瀛楃涓�?
	 * @param response HttpServletResponse瀵硅�?
	 * @param str 瀛楃涓�?
	 */
	protected void outString(HttpServletResponse response, Object str) {
		PrintWriter out = null;
		response.setContentType("text/plain;charset=utf-8");
		response.setHeader("Cache-Control", "no-cache");
		try {
			out = response.getWriter();
			out.println(str);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 鑾峰彇session涓殑鐢ㄦ埛瀹炰�?
	 * <p>鑻ユ病鏈夌敤鎴凤紝杩斿洖null</p>
	 */
	protected UserEntity getUserBySession() {
		UserEntity user = (UserEntity) SpringUtil.getRequest().getSession().getAttribute(SessionConstant.USER);
		if (user != null) {
			return user;
		} else {
			return null;
		}
	}
	
	/**
	 * 涓婁紶鏂囦欢瑙ｆ瀽涓烘暟鎹�
	 * @param file 涓婁紶鏂囦欢
	 * @param cols 鏂囦欢鍒楁暟
	 * @return
	 */
//	protected List<List<String>> file2List(MultipartFile file, Integer cols) {
//		String basePath = AttachFileUtil.getRealPath("/import");
//		String path = UpLoadUtil.fileUpload(file, basePath);
//		List<List<String>> list = Excel.commonImport(path, cols);
//		return list;
//	}
	
}
