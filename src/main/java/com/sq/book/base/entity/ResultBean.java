package com.sq.book.base.entity;

public class ResultBean {

	private Object data;
	
	private boolean result;
	
	private String msg;

	public ResultBean() {
		
	}

	/**
	 * @param result
	 */
	public ResultBean(boolean result) {
		super();
		this.result = result;
	}
	
	/**
	 * @param data
	 * @param result
	 */
	public ResultBean(boolean result, Object data) {
		super();
		this.data = data;
		this.result = result;
	}
	
	/**
	 * @param data
	 * @param result
	 * @param msg
	 */
	public ResultBean(boolean result, String msg) {
		this.result = result;
		this.msg = msg;
	}

	/**
	 * @param data
	 * @param result
	 * @param msg
	 */
	public ResultBean(boolean result, Object data, String msg) {
		this.data = data;
		this.result = result;
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
