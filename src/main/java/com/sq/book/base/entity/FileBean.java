package com.sq.book.base.entity;

import java.io.File;

/**
 * 文件包装�?
 * @author 邱小�?
 *
 */
public class FileBean {

	/**
	 * 文件是否已上�?
	 */
	private Boolean uploaded;
	
	/**
	 * 文件名称
	 */
	private String name;
	
	/**
	 * 文件路径
	 */
	private String path;
	
	/**
	 * 文件大小
	 */
	private long size;

	public FileBean() {
		super();
	}

	public FileBean(Boolean uploaded, String name, String path, long size) {
		super();
		this.uploaded = uploaded;
		this.name = name;
		this.path = path;
		this.size = size;
	}
	
	public FileBean(File file) {
		this.uploaded = true;
		this.name = file.getName();
		this.path = file.getPath();
		this.size = file.length();
	}

	public Boolean getUploaded() {
		return uploaded;
	}

	public void setUploaded(Boolean uploaded) {
		this.uploaded = uploaded;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}
}
