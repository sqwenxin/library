package com.sq.book.base.entity;

import java.util.List;

public class EasyUIBean {

	private int total;
	
	private List rows;

	public EasyUIBean() {
		
	}
	
	public EasyUIBean(List rows, int total) {
		super();
		this.rows = rows;
		this.total = total;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List getRows() {
		return rows;
	}

	public void setRows(List rows) {
		this.rows = rows;
	}
	
}
