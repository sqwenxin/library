CREATE TABLE `user` (
`user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '记录Id',
`user_code` varchar(50) NOT NULL COMMENT '用户编号',
`user_password` varchar(20) NOT NULL DEFAULT '000000' COMMENT '用户密码',
`user_name` varchar(50) NULL COMMENT '用户名',
`user_state` int(11) NULL DEFAULT 0 COMMENT '用户状态：0：新建，1：审核中，2：审核通过',
`user_role_id` int(11) NULL COMMENT '关联角色ID',
`user_create_by` int(11) NULL COMMENT '创建人',
`user_create_date` date NULL COMMENT '创建时间',
`user_update_by` int(11) NULL COMMENT '更新人',
`user_update_date` date NULL COMMENT '更新时间',
`user_del` int(11) NULL DEFAULT 0 COMMENT '是否删除：0：未删除，1：已删除',
PRIMARY KEY (`user_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='用户信息表';

CREATE TABLE `role` (
`role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色Id',
`role_name` varchar(100) NULL COMMENT '角色名称',
`role_describe` varchar(400) NULL COMMENT '角色描述',
`role_create_by` int(11) NULL COMMENT '创建人',
`role_create_date` date NULL COMMENT '创建时间',
`role_update_by` int(11) NULL COMMENT '更新人',
`role_update_date` date NULL COMMENT '创建时间',
`role_del` int(11) NULL DEFAULT 0 COMMENT '是否删除：0:未删除，1：已删除',
PRIMARY KEY (`role_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `book_catagory` (
`bc_id` int(11) NOT NULL AUTO_INCREMENT,
`bc_name` varchar(50) NULL COMMENT '分类名称',
`bc_describe` varchar(400) NULL COMMENT '分类描述',
`bc_parent_id` int(11) NULL COMMENT '上级分类ID',
`bc_create_by` int(11) NULL COMMENT '创建人',
`bc_create_date` date NULL COMMENT '创建时间',
`bc_update_by` int(11) NULL COMMENT '更新时间',
`bc_update_date` date NULL COMMENT '更新时间',
`bc_del` int(11) NULL DEFAULT 0 COMMENT '是否删除：0：未删除，1：已删除',
PRIMARY KEY (`bc_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE `purchase_book` (
`pb_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '采购数据Id',
`pb_name` varchar(50) NULL COMMENT '采购名称',
`pb_price` varchar(50) NULL COMMENT '采购价格',
`pb_content` varchar(500) NULL COMMENT '采购内容',
`pb_process_instance_id` varchar(20) NULL COMMENT '关联采购流程实例Id',
`pb_process_state` int(11) NULL COMMENT '流程状态字段',
`pb_create_by` int(11) NULL COMMENT '创建人',
`pb_create_date` date NULL COMMENT '创建时间',
`pb_update_by` int(11) NULL COMMENT '更新人',
`pb_update_date` date NULL COMMENT '更新时间',
`pb_del` int(11) NULL DEFAULT 0 COMMENT '是否删除：0：未删除，1：已删除',
PRIMARY KEY (`pb_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='采购流程';

CREATE TABLE `book` (
`book_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '图书ID',
`book_code` varchar(20) NOT NULL COMMENT '图书编号，唯一',
`book_name` varchar(200) NOT NULL COMMENT '图书名称',
`book_author` varchar(100) NULL COMMENT '作者',
`book_bc_id` int(11) NULL COMMENT '关联图书分类编号',
`book_describe` varchar(4000) NULL COMMENT '图书描述',
`book_image` varchar(500) NULL COMMENT '图书图片',
`book_manage_state` int(11) NULL DEFAULT 0 COMMENT '图书管理状态:0:未借出，1：已借出',
`book_state` int(11) NULL DEFAULT 0 COMMENT '图书当前状态：0：正常，1:损坏，2：丢失',
`book_damage_describe` varchar(4000) NULL COMMENT '图书损坏描述',
`book_create_by` int(11) NULL COMMENT '图书创建人',
`book_create_date` date NULL COMMENT '图书创建时间',
`book_update_by` int(11) NULL COMMENT '图书更新人',
`book_update_date` date NULL COMMENT '图书更新时间',
`book_del` int(11) NULL DEFAULT 0 COMMENT '图书是否删除：0：未删除，1：已删除',
PRIMARY KEY (`book_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='图书表';

CREATE TABLE `book_borrow` (
`bb_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '图书借阅ID',
`bb_user_id` int(11) NOT NULL COMMENT '用户ID',
`bb_book_id` int(11) NOT NULL COMMENT '图书ID',
`bb_book_name` varchar(50) NOT NULL COMMENT '图书名称',
`bb_process_state` int(11) NULL DEFAULT 0 COMMENT '状态：0：开始，1：确认收到，2:使用中，3：已归还，4：确认归还',
`bb_process_instace_id` varchar(50) NULL COMMENT '流程实例Id',
`bb_create_by` int(11) NULL COMMENT '创建人',
`bb_create_date` date NULL COMMENT '创建时间',
`bb_update_by` int(11) NULL COMMENT '更新人',
`bb_update_date` date NULL COMMENT '更新时间',
`bb_del` int(11) NULL DEFAULT 0 COMMENT '是否删除:0：未删除，1：已删除',
PRIMARY KEY (`bb_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='图书借阅表';

CREATE TABLE `model` (
`model_id` int(11) NOT NULL AUTO_INCREMENT,
`model_code` varchar(20) NULL,
`model_parent_code` int(11) NULL COMMENT '父级Id',
`model_name` varchar(100) NULL COMMENT '模块名称',
`model_describe` varchar(500) NULL COMMENT '模块描述',
`model_link` varchar(200) NULL COMMENT '模块链接',
`model_create_by` int(11) NULL COMMENT '创建人',
`model_create_date` date NULL COMMENT '创建时间',
`model_update_by` int(11) NULL COMMENT '更新人',
`model_update_date` date NULL COMMENT '创建时间',
`model_del` int(11) NULL DEFAULT 0 COMMENT '是否删除:0：未删除，1：已删除',
PRIMARY KEY (`model_id`, `model_code`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='模块表';

CREATE TABLE `role_model` (
`rm_id` int(11) NOT NULL AUTO_INCREMENT,
`rm_role_id` int(11) NOT NULL COMMENT '角色ID',
`rm_model_id` int(11) NOT NULL COMMENT '模块Id',
PRIMARY KEY (`rm_id`) 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='角色模块关联表';

